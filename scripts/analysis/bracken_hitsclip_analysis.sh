#!/bin/bash
# This script runs the Python analysis and requires that the reads have already been aligned

set -e

function get_hitsclip_project_dir {
	echo "$( cd "$(dirname $(dirname $(dirname "${BASH_SOURCE[0]}" )))" && pwd )"
}

if [ -z ${IGENOMES_DIR} ]; then
	echo "You must set the IGENOMES_DIR variable" >&2
	exit 1;
fi

PROJECT_DIR=$(get_hitsclip_project_dir)
PYTHON_SCRIPT=${PROJECT_DIR}/scripts/analysis/bracken_hitsclip.py
DATA_DIR=${PROJECT_DIR}/data

export PYTHONPATH=${PROJECT_DIR}

CONFIG_FILE=${1:-${PROJECT_DIR}/config/200_transfection_hg18_bed.cfg}
if [ ! -e ${CONFIG_FILE} ]; then
	echo "Usage $(basename $0): <config_file>" >&2;
	exit 1;
fi

python ${PYTHON_SCRIPT} --config ${CONFIG_FILE} --igenomes-dir=${IGENOMES_DIR} --data-dir ${DATA_DIR}
