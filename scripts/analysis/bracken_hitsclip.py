#!/usr/bin/env python

'''
Created on May 21, 2012

@author: dlawrence
'''

from argparse import ArgumentParser
import logging
import time

from sacgf import hitsclip, util
from sacgf.genomics.reference import Reference
from sacgf.hitsclip import hitsclip_config
from sacgf.hitsclip.peaks import expression
import sacgf.hitsclip.peaks as hcpeaks
from sacgf.hitsclip.peaks.find_peaks import find_peaks


def handle_args():
    parser = ArgumentParser(description='Perform Analysis on HITS-CLIP data for Bracken 2014 paper')
    parser.add_argument("--config", help='Project config file')
    parser.add_argument("--igenomes-dir", help="Directory of Illumina iGenomes (contains 'Homo_sapiens/UCSC/hg18')")
    parser.add_argument("--data-dir", help="Data directory")
    return parser.parse_args()


start = time.time()
args = handle_args()
config = hitsclip_config.HITSCLIPConfig(args.data_dir, args.igenomes_dir, args.config)
logger = util.setup_basic_rotating_logging(config.log_file, config.log_level)

logger.info("Starting '%s'", args.config)
reference = Reference(config=config)
chrom_dao = reference.get_chrom_dao()
samples = hitsclip.load_samples(config, chrom_dao)
microarray_expression = expression.load_expression(config.micro_array_expression_csv, samples.keys())

hitsclip.do_qc(config, reference, samples)

peaks_dfs = {}
for treatment_name in config.treatments:
    peaks = find_peaks(config.peak_min_treatment_depth, config.peak_min_ratio, config.peak_uninteresting_cutoff, samples["control"].array, samples[treatment_name].array)
    hcpeaks.write_peaks_bed_file(config.output, treatment_name, peaks)
    peaks_dfs[treatment_name] = hcpeaks.post_peak_processing(config, reference, microarray_expression, samples, treatment_name, peaks)
    
hcpeaks.sublists.do_gene_list_graphs(config, reference, samples)



end = time.time()
logging.info("Took %d seconds", (end - start))
