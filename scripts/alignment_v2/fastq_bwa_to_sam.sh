#!/bin/bash

. sacgf_init.sh
load_module bwa

FASTQ=${1}
CORES=${2}

ORGANISM=${ORGANISM:-${DEFAULT_ORGANISM}}
BUILD=${BUILD:-${DEFAULT_BUILD}}

echo "Using organism ${ORGANISM} build ${BUILD}"

if [ -z ${FASTQ} ]; then
        echo "Usage $(basename $0): Provide fastq as first argument, # cores as second. No fastq file provided as argument" >&2
        exit;
elif [ ! -e ${FASTQ} ]; then
        echo "$(basename $0): No fastq file ('${FASTQ}') found"
        exit
else
        echo "FASTQ = '${FASTQ}'";
fi

if [ -z ${CORES} ]; then
	echo "$(basename $0): No cores supplied - defaulting to using 4 cores."
	CORES=4
else
	echo "Using ${CORES} cores"
fi	

NAME=$(get_name ${FASTQ}).bwa.${BUILD}
BWA_INDEX=$(get_igenomes_tool_index BWA)

echo "BWA Index = ${BWA_INDEX}"

# align reads with bwa
bwa aln -t ${CORES} ${BWA_INDEX} ${FASTQ} -f ${NAME}.sai

# generate sam alignment file
bwa samse -n 3 ${BWA_INDEX} ${NAME}.sai ${FASTQ} -f ${NAME}.sam

# Cleanup intermediate file
rm ${NAME}.sai
