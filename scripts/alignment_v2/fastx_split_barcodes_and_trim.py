#!/usr/bin/env python

'''
SA Cancer Genomics Facility

2012-07-09: Initial version - Dave Lawrence
2012-10-26: Multiple barcode files

'''

import re
import sys
from Bio import SeqIO
from argparse import ArgumentParser
from sacgf import genomics, util

__version_info__ = (0, 1, 2)
__version__ = '.'.join(map(str, __version_info__))

class Barcode(object):
    def __init__(self, sequence, mismatches):
        self.sequence = sequence
        self.matches = util.generate_mis_matches(sequence, mismatches)
        self.regex_patterns = [re.compile(i, re.IGNORECASE) for i in self.matches]
        self.count = 0
        
class BarcodeMatcher(object):
    
    def __init__(self, file_name, mismatches):
        self.file_name = file_name
        self.mismatches = mismatches
        self.barcodes = {}
        self.total = 0
        self.unmatched = 0
        
        with open(file_name) as f:
            bc_sequences = util.file_to_hash(f, "\t")

        for (bc, sequence) in bc_sequences.iteritems():
            self.barcodes[bc] = Barcode(sequence, mismatches)

    def get_barcode_match(self, seq):
        self.total += 1
        for (bc, bc_obj) in self.barcodes.iteritems():
            for pattern in bc_obj.regex_patterns:
                match_obj = pattern.match(seq)
                if match_obj:
                    bc_obj.count += 1
                    return (bc, match_obj.end(0))
        self.unmatched += 1
        return (None, 0)

    def get_header(self):
        mm_string = ""
        if self.mismatches != 0:
            mm_string = " (%d mismatches)" % self.mismatches
        return "BarcodeMatcher '%s'%s:" % (self.file_name, mm_string)

    def print_debug_info(self):
        print self.get_header()
        for (bc, bc_obj) in self.barcodes.iteritems():
            print "%s: sequence=%s, matches=%s" % (bc, bc_obj.sequence, bc_obj.matches)

    def print_stats(self):
        print self.get_header()

        for (bc, bc_obj) in self.barcodes.iteritems():
            print_percent(bc, bc_obj.count, self.total)

        print_percent("unmatched", self.unmatched, self.total)


def print_percent(name, count, total):
    perc = 0
    if total != 0:
        perc = 100.0*count/total
    print "%s : %d (%d%%)" % (name, count, perc)


class RecordWriter(object):
    class BCOutput(object):
        def __init__(self, open_func, file_name, sequence_format):
            self.count = 0
            self.out = open_func(file_name, "wb")
            self.sequence_format = sequence_format
            
        def write(self, record):
            self.count += 1
            SeqIO.write(record, self.out, self.sequence_format)            

    def __init__(self, open_func, sequence_format, prefix, extension):
        self.open_func = open_func
        self.sequence_format = sequence_format
        self.prefix = prefix
        self.extension = extension
        self.output = {} 
    
    def create_output(self, bc):
        file_name = "".join([self.prefix, bc, self.extension])
        return RecordWriter.BCOutput(self.open_func, file_name, self.sequence_format)
    
    def write(self, bc, record):
        if bc is None:
            bc = "unmatched"
            
        if self.output.get(bc) is None:
            self.output[bc] = self.create_output(bc)
        self.output[bc].write(record)

    def print_stats(self):
        print "Records written:"
        total = sum([b.count for b in self.output.itervalues()])
        for (bc, bc_out) in self.output.iteritems():
            print "%s : %d (%d%%)" % (bc, bc_out.count, 100.0*bc_out.count/total)


def create_barcode_matchers(bc_match_list, verbose):
    barcode_matchers = []
    for (bc_file, mismatches) in bc_match_list:
        bc_matcher = BarcodeMatcher(bc_file, mismatches)
        barcode_matchers.append(bc_matcher)
        if verbose:
            bc_matcher.print_debug_info()
    
    return barcode_matchers

def main(input_file, bc_match_list, prefix, verbose):
    barcode_matchers = create_barcode_matchers(bc_match_list, verbose)

    open_func = util.get_open_func(input_file)
    sequence_format = genomics.sequence_file_format(input_file)
    extension = genomics.get_sequence_extension(input_file)
    record_iterator = SeqIO.parse(open_func(input_file, "rb"), sequence_format)
    record_writer = RecordWriter(open_func, sequence_format, prefix, extension)

    for record in record_iterator:
        seq = str(record.seq)
        matched_b_c = None
        for barcode_matcher in barcode_matchers:
            (bc, trim_size) = barcode_matcher.get_barcode_match(seq)
            if bc is not None:
                if trim_size > 0:
                    record = record[trim_size:]
                matched_b_c = bc
                # TODO: if passed option write to bc_matcher specific file...
                break

        record_writer.write(matched_b_c, record)

    for barcode_matcher in barcode_matchers:
        barcode_matcher.print_stats()

    record_writer.print_stats()
    # TODO: Total record_writer stats


def handle_args():
    # Args
    parser = ArgumentParser(description='Split Barcodes', version=__version__)
    parser.add_argument('--verbose',    action='store_true')
    parser.add_argument('--bcfile',     required=True, action='append')
    parser.add_argument('--mismatches', type=int, action='append')
    parser.add_argument('--prefix')
    parser.add_argument('input_file',   metavar='input_file', help='fasta or fastq file')

    return parser.parse_args()

if __name__ == '__main__':
    args = handle_args()

    if args.prefix:
        prefix = args.prefix
    else:
        prefix = genomics.get_sequence_name(args.input_file) + '.'

    num_b_c = len(args.bcfile)
    if args.mismatches is None:
        mismatches = [0] * num_b_c
    else:
        num_m_m = len(args.mismatches)
        if num_m_m > 0:
            if num_b_c != num_m_m:
                sys.stderr.write("'bcfile' (%d) and 'mismatches' (%d) must be specified the same number of times\n" % (num_b_c, num_m_m))
                sys.exit(1)
        mismatches = args.mismatches
        
    bc_match_list = zip(args.bcfile, mismatches)
    main(args.input_file, bc_match_list, prefix, args.verbose)
