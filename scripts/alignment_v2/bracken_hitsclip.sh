#!/bin/bash

set -e

FASTQ=$1

if [[ $# -ne 1 || $1 != *fastq* ]]; then
	echo "Usage: $(basename $0) fastq" >&2
	exit 1
fi

if [ -z ${IGENOMES_DIR} ]; then
	echo "You must set the IGENOMES_DIR variable" >&2
	exit 1;
fi

export ORGANISM=Homo_sapiens
export BUILD=hg19

function get_hitsclip_project_dir {
	echo "$( cd "$(dirname $(dirname $(dirname "${BASH_SOURCE[0]}" )))" && pwd )"
}

PROJECT_DIR=$(get_hitsclip_project_dir)
SCRIPTS_DIR=${PROJECT_DIR}/scripts
CONFIG_DIR=${PROJECT_DIR}/config
SCRATCH_DIR=${PROJECT_DIR}/output/scratch
SPLIT_FASTQ_DIR=${SCRATCH_DIR}/barcode_split_fastq
BAMS_DIR=${PROJECT_DIR}/output/bams

BARCODES=${CONFIG_DIR}/barcodes.txt

export ADAPTER_FILE=${CONFIG_DIR}/adapters/GAII.txt
export PATH=${SCRIPTS_DIR}:${PATH}
export PYTHONPATH=${PROJECT_DIR}

. sacgf_init.sh

# Split barcodes

if [ ! -e ${SPLIT_FASTQ_DIR} ]; then
	echo "Splitting barcodes"
	mkdir -p ${SPLIT_FASTQ_DIR}
	fastx_split_barcodes_and_trim.py --bcfile ${BARCODES} --mismatches=0 --bcfile ${BARCODES} --mismatches=1 --prefix=${SPLIT_FASTQ_DIR}/ ${FASTQ}
fi

EXTENSION=$(get_extension ${SPLIT_FASTQ_DIR}/noTrf.fastq*)
# Combine noTrf & scr into "combined_controls" (yes this works for .gz files as well)
cat ${SPLIT_FASTQ_DIR}/noTrf.${EXTENSION} ${SPLIT_FASTQ_DIR}/scr.${EXTENSION} > ${SPLIT_FASTQ_DIR}/combined_controls.${EXTENSION}

cd output

# Map & Trim
for EXPERIMENT in 200a 200b combined_controls; do
	FASTQ=${SPLIT_FASTQ_DIR}/${EXPERIMENT}.fastq.gz
	fastq_small_rnaseq.sh ${FASTQ}
done

# Clean up
mkdir -p ${SCRATCH_DIR}
mkdir -p ${BAMS_DIR}

rm -f *.sam

# Move trimmed files in there if you want to look at them
if [ -e *.fastq* ]; then
	mv *.fastq* ${SCRATCH_DIR}
fi
if [ -e *.bam* ]; then
	mv *.bam* ${BAMS_DIR}
fi
if [ -e flagstats ]; then
	mv flagstats ${BAMS_DIR}
fi

cd ..
