#!/bin/bash

. sacgf_init.sh

if [ $# -lt 1 ]; then
        echo "Usage $(basename ${0}) one.bam (two.bam etc)" >&2
        exit 1;
fi

load_module samtools
for BAM in $@; do
	if [ $(get_extension ${BAM}) = 'bam' ]; then
		BAM_INDEX=${BAM}.bai
		if [ ! -e ${BAM_INDEX} ]; then
			echo "Generating ${BAM_INDEX}" >&2
			samtools index ${BAM}
		fi
		mkdir -p flagstats
		BAM_FLAGSTAT=flagstats/$(get_name ${BAM}).flagstat.txt
		if [ ! -e ${BAM_FLAGSTAT} ]; then
			echo "Generating ${BAM_FLAGSTAT}" >&2
			samtools flagstat ${BAM} > ${BAM_FLAGSTAT}
		fi
	fi
done


