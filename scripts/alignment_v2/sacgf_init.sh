#!/bin/bash

set -e

function require_variable {
	for __NAME in $@; do
		__VALUE=${!__NAME}
		if [ -z "${__VALUE}" ]; then
			echo "Variable '${__NAME}' not set">&2
			exit 1
		fi
	done
}

function require_file {
	if [[ $# -lt 1 ]]; then
		echo "require_file must be passed at least 1 file" 1>&2
		exit 1
	fi	
	local FILE
	for FILE in $@; do
		if [ ! -e ${FILE} ]; then
			echo "Could not access required file: '${FILE}'" 1>&2
			exit 1
		fi
	done
}

function load_module {
	local MODULE=$1
	if [ ! -z ${BASH_MODULE_INIT} ]; then
		if [ -z ${MODULE_VERSION_STACK} ]; then
			echo "Loading ${BASH_MODULE_INIT}" >&2
			. ${BASH_MODULE_INIT}
		fi
		echo "module load ${MODULE}" >&2
		module load ${MODULE}
	else
		echo "Didn't call module, I hope ${MODULE} is in your path!" 1>&2
	fi
}

function get_name {
        if [[ $# -ne 1 || -z $1 ]]; then
                echo "get_name must be passed exactly 1 argument" 1>&2
		exit 1
        fi
        local FILENAME=$(basename $1)
        local EXTENSION=${FILENAME##*.}
        local NAME="${FILENAME%.*}"
        if [ ${EXTENSION} == "gz" ]; then # get next one...
                NAME=$(get_name ${NAME})
        fi
        echo ${NAME}
}

function get_extension {
        if [[ $# -ne 1 || -z $1 ]]; then
                echo "get_extension must be passed exactly 1 argument" 1>&2
		exit 1
        fi
        local FILENAME=${1}
        local EXTENSION=${FILENAME##*.}
        local NAME="${FILENAME%.*}"
        if [ ${EXTENSION} == "gz" ]; then # get next one...
                EXTENSION=$(get_extension ${NAME}).${EXTENSION}
        fi
        echo ${EXTENSION}
}

function is_gzipped {
        if [[ $# -ne 1 || -z $1 ]]; then
                echo "is_gzipped must be passed exactly 1 argument" 1>&2
		exit 1
        fi
        local FILENAME=${1}
        local EXTENSION=${FILENAME##*.}
        local NAME="${FILENAME%.*}"
        if [ ${EXTENSION} == "gz" ]; then # get next one...
			return 0;
		else
			return 1;
        fi
}

function get_igenomes_tool_index {
	local TOOL=${1}
	require_variable TOOL
	local IGENOMES_TOOL_INDEX=$(get_igenomes_reference_dir)/Sequence/${TOOL}Index/genome.fa
	require_file ${IGENOMES_TOOL_INDEX}
	echo ${IGENOMES_TOOL_INDEX}
}	

function get_igenomes_reference_dir {
	require_variable IGENOMES_DIR
	require_variable ORGANISM
	require_variable BUILD
	local SOURCE=${SOURCE:-UCSC}
	echo ${IGENOMES_DIR}/${ORGANISM}/${SOURCE}/${BUILD}
}


function get_igenomes_reference_genome {
    local IGENOMES_REFERENCE_GENOME=$(get_igenomes_reference_dir)/Sequence/WholeGenomeFasta/genome.fa
	require_file ${IGENOMES_REFERENCE_GENOME}
	echo ${IGENOMES_REFERENCE_GENOME}
}

function get_igenomes_reference_gtf {
	local IGENOMES_REFERENCE_GTF=$(get_igenomes_reference_dir)/Annotation/Genes/genes.gtf
	require_file ${IGENOMES_REFERENCE_GTF}
    echo ${IGENOMES_REFERENCE_GTF}
}

require_variable IGENOMES_DIR
