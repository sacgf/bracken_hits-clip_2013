#!/bin/bash
# same as fastq_small_mirna_count but without the count (ie not keeping SAM)

. sacgf_init.sh

FASTQ=$1
BUILD=${BUILD:-${DEFAULT_BUILD}}

if [ -z ${FASTQ} ]; then
        echo "No fastq file provided as argument"
        exit 1;
elif [ ! -e ${FASTQ} ]; then
        echo "No fastq file ('${FASTQ}') found"
        exit 1;
else
        echo "FASTQ = '${FASTQ}'";
fi


NAME=$(get_name ${FASTQ})
EXTENSION=$(get_extension ${FASTQ})

TRIMMED_FILE=$(fastx_get_trimmed_name.sh $@)
echo "Checking for '${TRIMMED_FILE}'" 1>&2
if [ -e ${TRIMMED_FILE} ]; then
	echo "${TRIMMED_FILE} already exists." 1>&2
else
	fastx_trim_adapter.sh ${FASTQ}
	if [ ! -e ${TRIMMED_FILE} ]; then
		echo "Ran 'fastx_trim_adapter.sh ${FASTQ}' but can't find trimmed file: ${TRIMMED_FILE}" >&2
		exit 1
	else
		echo "Trimmed and produced file '${TRIMMED_FILE}'" >&2
	fi
fi

TRIMMED_NAME=$(get_name ${TRIMMED_FILE})

#make a fastqc report if one doesn't exist already

FASTQC_ZIP=${TRIMMED_NAME}_fastqc.zip
if [ ! -e ${FASTQC_ZIP} ]; then
	echo "FastQC on '${TRIMMED_FILE}'" >&2
        fastqc.sh ${TRIMMED_FILE}
fi

#align using bwa
ALIGNED_NAME=${TRIMMED_NAME}.bwa.${BUILD}
BAM=${ALIGNED_NAME}.bam
if [ ! -e ${BAM} ]; then
	echo "BWA aligning ${TRIMMED_FILE} to ${BAM}" >&2
	fastq_bwa_to_bam.sh ${TRIMMED_FILE}
fi


