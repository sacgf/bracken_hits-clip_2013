#!/bin/bash

. sacgf_init.sh

FASTQ=$1
BUILD=${BUILD:-${DEFAULT_BUILD}}
#Usage: script <fastq> <Number of cores>
fastq_bwa_to_sam.sh $@ 

NAME=$(get_name ${FASTQ}).bwa.${BUILD}
sam_to_bam.sh ${NAME}.sam
