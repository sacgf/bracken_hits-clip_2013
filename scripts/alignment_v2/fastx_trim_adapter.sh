#!/bin/bash

# Trims with adapter found in argument file, or if none:
# 		1. Looks for files which contain paths of the adapter to use:
#			${NAME}.adapter.txt
#			adapter/${NAME}.adapter.txt (ie this might contain "/data/sacgf/reference/adapters/HiSeq2000.txt"
#			
#		2. Falls back on ${DEFAULT_ADAPTER} variable

# 14-APR-2014 - dml - changed to do no quality trim by default, you can get previous behavior by
#                     setting environment variable QUALITY_CUTOFF=28

. sacgf_init.sh
load_module cutadapt

FASTQ=${1}
ADAPTER_FILE=${2:-${ADAPTER_FILE}}
OVERLAP=${3:-${OVERLAP:-${DEFAULT_ADAPTER_OVERLAP:-5}}}
MIN_LENGTH=${4:-${MIN_LENGTH:-${DEFAULT_ADAPTER_MIN_LENGTH:-18}}}
QUALITY_CUTOFF=${5:-${QUALITY_CUTOFF:-${DEFAULT_TRIM_QUALITY_CUTOFF:-}}} # Default is None
ERROR_RATE=${6:-${ERROR_RATE:-${DEFAULT_TRIM_ERROR_RATE:-0.2}}}

# Fail on errors
set -e

if [ -z ${FASTQ} ]; then
        echo "No fastq file provided as argument"
        exit 1;
elif [ ! -e ${FASTQ} ]; then
        echo "No fastq file ('${FASTQ}') found"
        exit 1;
else
        echo `basename ${0}` ${FASTQ}
fi

ADAPTER_FILE=$(fastx_get_adapter_file.sh $@)

if [[ -z ${ADAPTER_FILE} || ! -e ${ADAPTER_FILE} ]]; then
	echo "No adapter file file ('${ADAPTER_FILE}') found" >&2
	exit 1
else
	echo "Adapter file = '${ADAPTER_FILE}'" >&2
	ADAPTER=$(cat ${ADAPTER_FILE})
	echo "Adapter = ${ADAPTER}" >&2
fi

QUALITY_PARAMS=
if [ ! -z $QUALITY_CUTOFF ]; then
	QUALITY_PARAMS=--quality-cutoff=${QUALITY_CUTOFF}
fi

OUTPUT=$(fastx_get_trimmed_name.sh $@)
NAME=$(get_name $FASTQ)
CUTADAPT_LOG=${NAME}.cutadapt.log
CUTADAPT_COMMAND="cutadapt ${QUALITY_PARAMS} --adapter=${ADAPTER} --error-rate ${ERROR_RATE} --overlap=${OVERLAP} --minimum-length=${MIN_LENGTH} --output=${OUTPUT} ${FASTQ}"
echo "Running: '${CUTADAPT_COMMAND}'" >&2
${CUTADAPT_COMMAND} > ${CUTADAPT_LOG}

