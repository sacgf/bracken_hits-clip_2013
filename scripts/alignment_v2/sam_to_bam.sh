#!/bin/bash

. sacgf_init.sh
load_module samtools

SAM=$1
if [ -z ${SAM} ]; then
        echo "No argument provided"
        exit 1;
fi
NAME=$(get_name ${SAM})

samtools view -b -S ${SAM} > ${NAME}.unsorted.bam
samtools sort ${NAME}.unsorted.bam ${NAME}
rm ${NAME}.unsorted.bam

bam_index.sh ${NAME}.bam

