#!/bin/bash

. sacgf_init.sh

FASTQ=${1}

# Fail on errors
set -e

if [ -z ${FASTQ} ]; then
        echo "Usage $(basename $0): FASTA/FASTQ" >&2
        exit 1;
elif [ ! -e ${FASTQ} ]; then
        echo "No fastq file ('${FASTQ}') found" >&2
        exit 1;
fi

NAME=$(get_name ${FASTQ})
EXTENSION=$(get_extension ${FASTQ})

echo "\${ADAPTER_FILE}='${ADAPTER_FILE}'" >&2
ADAPTER_FILE=$(fastx_get_adapter_file.sh $@)
TRIM_EXTENSION="trimmed_$(get_name ${ADAPTER_FILE})"
TRIMMED_NAME=${NAME}.${TRIM_EXTENSION}.${EXTENSION}
echo ${TRIMMED_NAME}
