#!/usr/bin/env python

import os
import sys
from pyfasta import Fasta

organism='Homo_sapiens'
annotation_group = 'UCSC'
build = 'hg18'

igenomes_dir = os.environ.get("IGENOMES_DIR")
print igenomes_dir
if not igenomes_dir:
        sys.stderr.write("You must set the 'IGENOMES_DIR' environment variable\n") 
        sys.exit(1)

reference_genome = os.path.join(igenomes_dir, organism, annotation_group, build, 'Sequence', 'WholeGenomeFasta', 'genome.fa')

key_fn = lambda key : key.split()[0] # Use first value before whitespace as keys
Fasta(reference_genome, key_fn=key_fn)

