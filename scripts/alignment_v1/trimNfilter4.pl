#!/usr/bin/perl -w

use POSIX ;
use strict;
use diagnostics;
use English;
use Getopt::Long;
use Data::Dumper;
use IO::File ;




                                                                                                                                            
$Data::Dumper::Indent = 1;                                                                                                                  
$Data::Dumper::Useqq  = 1;                                                                                                                  
$Data::Dumper::Purity = 1;  

my ($usage) = <<'USAGE';                                                                                                                    

USAGE:

All scripts were written using perl version 5.10.0



This script reads in a set of miRNAs and a targetScan summary file. The genes having target sites for the listed miRNAs are recorded in hash
and the numbers of corrresponding target sites and types are also recorded.

Output consists of listing of all genes that have at least one target site for all of the miRNAs of interest
together with the target site types (conservation, seet type and count).

version 4 : amended to trim back trailing B quality values that the latest illumina pipeline versions generate as regions to be stripped.


	
#FILES
README - this information file

#------------------


 trimNfilter.pl 
  


Usage: $0 <options> fasta_file
    Options
	
    	-i	: raw input otherwise defaults to processed by maq sol2sanger  
        -k	: hits file known from previous processing step
        -o      : main output file of reads matching the hits file
        -f      : reads forked off from hits stream - those that were not in the hits file
	-p	: Phred score average for the pre-barcode read  eg 20 means 1 in 100 errors 30 means 1 in 1000, avge of less than 30 would be pretty poor
	-b      : barcode file - will strip bcode and anything to the right and will write to outfile.bCodeSeqN for each of the N barcodes and all to outfile.bCodeStripped :
		bCode|ATCACG|19|Brain
		bCode|CGATGT|19|Ovary
		bCode|TTAGGC|19|Kidney
		bCode|TGACCA|19|Lung
		bCode|ACAGTG|19|Liver
		bCode|GCCAAT|19|Spleen
		
	-c	: correct scores from solexa to proper/standard fastq
	-r	: filter out repeat sequences
	-t      : file of sequences to trim - will look for min of 5 nt of the trim sequences (overlapping the end of a read) - primers, linkers, vectors
		  trim reverse and reverse complements of the supplied trim sequences as per second field in the trim seq file :
		#trimLinker|GATCGGAAGAGCTCGTATGCCGTCTTCTGCTTG|RRC|5|IlluminaLinker     # RRC means allow reverse and reverse complement, try match down to 5 bases
		trimLinker|CAGACGACGAGCGGGATCGTATGCCGTCTTCTGCTTG|RRC|1|IlluminaLinker
		trimLinker|CAGACGACGAGCGGGATCGTATGCCGTCTTCTGCTTG|1|IlluminaLinker
		#trimPrimer|GCCTTGCCAGCCCGCTCAG|R|5|RochePrimerA
		#trimPrimer|GCCTCCCTCGCGCCATCAG|R|5|RochePrimerB
		#trimVector|GCCTGAAGTACC|
	-q	: trim reads back from end, removing any contiguous bases having read quality lower than the given character ascii code	
	-d	: file of sequences to dump, drop from OP if matching sequence found

        -h      : help and exit
        --help	: help and exit 
	
	eg ./trimNfilter.pl -i /usr/local/work/mmCleave/s_1_sequence_fastq.txt -b /usr/local/work/mmCleave/barcodes.file -c 1 -o testItOut.fastq
	./trimNfilter.pl -i /usr/local/work/mmCleave/s_1_sequence_fastq.txt -d dropTheseSeqs -b /usr/local/work/mmCleave/barcodes.file -c 1 -o testItOut.fastqRead
	./trimNfilter.pl -i /usr/local/work/mmCleave/s_1_sequence_fastq.txt -d dropTheseSeqs -b /usr/local/work/mmCleave/barcodes.file -p 20 -r 1 -c 1 -o testItOut.fastqRead
	./trimNfilter.pl -i /usr/local/work/CLIPseq1/A_sequence_fastq.txt -d dropTheseSeqs -b /usr/local/work/CLIPseq1/CLIPbarcodeFile -p 20 -r 1 -c 1 -o testItOut.fastqRead
	
	
	trimNfilter2.pl -i $F -n $F -b /data/adelaide/CLIP-Seq_091110/CLIPbarcodeFile -p 20 -r 1 -c 1 -t /data/adelaide/CLIP-Seq_091110/CLIPilluminaLinkerz -o testOut.file
USAGE

#suffixes to edit
# aa ab ac ad ae af ag ah ai aj ak al am an ao ap aq ar as at au av aw ax ay az ba bb bc bd be bf bg bh bi bj bk bl bm bn bo bp bq br bs bt bu bv bx by bz


#  perl /data/adelaide/scripts/trimNfilter3.pl -i 'keep/sp110pool-'$e -n -$e -b /data/adelaide/CLIPbarcodeFileBigPare -p 2 -r 1 -t /data/adelaide/CLIPilluminaLinkerz -o hg18CLIP-110filt.fastq



#if ( $#ARGV < 1 ) {
    if (0) {
    print $usage;                                                                                                                           
    exit;                                                                                                                                   
}
    
our $trimQualities 	= 0;  # -q  minimum quality acceptable
our $trimAveQual 	= 2;
our $trimWindow 	= 5;     # what length of window in nt for average qual calcs
our $trimSpikeQual 	= 1.5 ;  # if get one of these will reject entire read or if more than 50% of read length will trim back to it

our $convert_quals2fastq = 0;
our $phredQualityThreshold;
our $repeatFilter = 0;
my (
    $fastQsourceFile,
    $hitFile,
    $outFilteredFile,
    $forkedLeftFile,
    $dumpSeqFile,
    $trimFile,
    $barcodeFile,
    $fileNameSuffix,
    $help
);

$fileNameSuffix = '';
&GetOptions(	"i=s"=>\$fastQsourceFile,
                "k=s"=>\$hitFile,
                "o=s"=>\$outFilteredFile,
                "f=s"=>\$forkedLeftFile,
		"d=s"=>\$dumpSeqFile,
		"r=i"=>\$repeatFilter,
		"t=s"=>\$trimFile,
		"n=s"=>\$fileNameSuffix,
		"b=s"=>\$barcodeFile,
		"q=i"=>\$trimQualities,
		"c=i"=>\$convert_quals2fastq,
		"p=i"=>\$phredQualityThreshold,
                "h"=>\$help,
                "help"=>\$help);

	if( $help ) {
		
    		#&show_help($usage);
		print "\n$usage\n";
    		exit(1);
	}
	
sub show_help{
	my $usage = shift;
	print "\n$usage\n";
}

my (
	$trim_linkers,
	$newstring
   );
if (defined($trimFile)) {
	if (-e $trimFile) {$trim_linkers = 1}
	else{$trim_linkers = 0}
}
our @repeatSeqs;
$repeatSeqs[0] = 'AAAAAAAAAAAAAA';
$repeatSeqs[1] = 'TTTTTTTTTTTTT';
$repeatSeqs[2] = 'GGGGGGGGGGGGG';
$repeatSeqs[3] = 'CCCCCCCCCCCCC';

my $trim_B_quals = 1 ;  # trim back reads where illumina pipeline indicated trailing string of B qualities ie unacceptably low
our $trimTrailingBstrings = 0;
our $trimBqualsTooshort = 0;  # counter of truncated B quals reads that fell below sufficience remaining seq length (21) (including a 6 length barcode )
my $trimBquals2short = 0;
my $trailingLinkers = 1;
my $leadingLinkers = 0;

my $joinedUpBcodes ;
our @barcodeSeqs;
our @barcodeStarts ;
our @barcodeNames ;
our @barCodeFilePointers;
my $joinedUpLinkers;
our @linkerSeqs;
our @linkers;
our @Bstrings;
our @linkerMin;
our @linkerNames;
our @linkerMultipliers;   # ie reverse and/or reverse complement

open(OVEROUT,  ">${outFilteredFile}$fileNameSuffix") or die "couldn't open main output file outFilteredFile\n";
open(GENREJECT,">generalReadRejectFile$fileNameSuffix") or die "couldn't open main output file generalReadRejectFile\n";
open(LOWPHRED, ">lowQualityReads$fileNameSuffix") or die "couldn't open main output file lowQualityReads\n";
open(NOBCODE,  ">noBarcodeFound$fileNameSuffix") or die "couldn't open main output file noBarcodeFound\n";

my $trimLinker2short = 0;
our $shorties = 21;  # cannot reliably map anything less (as barcodes are removed later) than this and maybe a good case for ensuring a longer sequence

my $Bs = 'B';
my @Bestrings;
while (length ($Bs) < 37){   # build array of B strings from 1 to 36 in length  for back trimming Bquals
	unshift(@Bestrings, $Bs);
	$Bs .= 'B';
}
@Bstrings = reverse(@Bestrings);

if (-e $barcodeFile) {
	open (BCODE,"<$barcodeFile" ) or die "could not open barcode file : $barcodeFile for reading\n";
	while(<BCODE>){
		next if /^\s+/;
		my @fields = split /\|/ ;
		chomp @fields;
		if ($fields[0] ne 'bCode') {print "barcode file format not obeyed. It should follow: bCode|TTAGGC|19|Kidney\nPlease fix and rerun\n"; exit 0 }
		push(@barcodeSeqs,  $fields[1]) ;
		push(@barcodeStarts,$fields[2]) ;
		push(@barcodeNames, $fields[3]) ;
		print "in Trim, barcode info : $fields[1] $fields[2] $fields[3] \n";
		push(@barCodeFilePointers,new IO::File ">"."$fields[3].fastq$fileNameSuffix" );  # use IO::File to create new file handle
	}
	close BCODE ;
	$barcodeFile = 1;
	$joinedUpBcodes = join(" : ",@barcodeSeqs);
}
else{$barcodeFile = 0;  print "Hey no barcode file found - could be a problem\n"}
my %seq2Dump;
my $seq2dumpFlag = 0;

if (defined($forkedLeftFile) && -e $forkedLeftFile) {open (FLEFT,">$forkedLeftFile" ) or die "could not open fork out file : $forkedLeftFile for reading\n" }
else {$forkedLeftFile = 0}
my $name ;
my %hits ;
if (defined($hitFile) && -e $hitFile) {
	open (HITS,"<$hitFile" ) or die "could not open barcode file : $hitFile for reading\n";
	while(<HITS>){
		next if /^\s+/;
		my @fields = split /\t/ ;
		chomp @fields;
		if ($fields[0] ne '') {
			$name = '@'.$fields[0];
			$hits{$name} = 1;
		}
		#push(@barcodeNames,$fields[3]) ;
		#push(@barCodeFilePointers,new IO::File ">Bcode_"."$fields[3]_$fields[1].fastq" );  # use IO::File to create new file handle
	}
	close HITS ;
	$hitFile = 1;
	
	print "last readname was $name\n\n";
	
}
else{$hitFile = 0}

if ($trim_linkers){
	if (-e $trimFile) {
		open (LINKERS,"<$trimFile" ) or die "could not open linker file : $trimFile for reading\n";
		while(<LINKERS>){
			next if /^\s+/;
			my @fields = split /\|/ ;
			
			# trimLinker|GATCGGAAGAGCTCGTATGCCGTCTTCTGCTTG|R:RC|5|IlluminaLinker
			chomp @fields;
			if ($fields[0] !~ /trim/) {print "trim file format not obeyed. It should follow: trimLinker|GATCGGAAGAGCTCGTATGCCGTCTTCTGCTTG|RRC|5|IlluminaLinker\nPlease fix and rerun\n"; exit 0 }
			push(@linkerSeqs,  $fields[1]) ;
			push(@linkerMin,$fields[3]) ;
			push(@linkerMultipliers, $fields[2]) ;
			push(@linkerNames, $fields[4]) ;
			print "in Trim, linker info : $fields[1] $fields[2] $fields[3] $fields[4]\n";
			my $linkerMinimumFragment = $fields[3];
			
			my $linker1 = $fields[1] ;
			my $seq = $linker1;
			my $leng = length($seq)+1;
			my $revSeq= reverse $seq;
			for (my $i = $linkerMinimumFragment; $i < $leng; $i++){
				my $subSeq = substr($seq,0,$i);
				unshift(@linkers, $subSeq);
				print "sub seq is $subSeq " ;
				if ($fields[2] eq 'R') {
					my $supSeq = reverse substr($revSeq,0,$i);
					unshift(@linkers, $supSeq);
					print "sup seq is $supSeq " ;
				}
				print "\n";
			}
			if ($fields[2] =~ /^R:/){
				my $linker2 = reverse($fields[1]) ;
				my $seq = $linker2;
				my $leng = length($seq)+1;
				my $revSeq= reverse $seq;
				for (my $i = $linkerMinimumFragment; $i < $leng; $i++){   
					my $subSeq = substr($seq,0,$i);
					my $supSeq = reverse substr($revSeq,0,$i);
					unshift(@linkers, $subSeq);
					unshift(@linkers, $supSeq);
				}
			}  
			if ($fields[2] =~ /RC/) {
				my $linker3 = revcomLine($fields[1]);
				my $seq = $linker3;
				my $leng = length($seq)+1;
				my $revSeq= reverse $seq;
				for (my $i = $linkerMinimumFragment; $i < $leng; $i++){
					my $subSeq = substr($seq,0,$i);
					my $supSeq = reverse substr($revSeq,0,$i);
					unshift(@linkers, $subSeq);
					unshift(@linkers, $supSeq);
				}
			}  # revcomLine
			  
		}
		close LINKERS ;
		#$linkerFile = 1;
		$joinedUpLinkers = join(" : ",@linkers);
	}
	
}



#if (defined($dumpSeqFile))
if (defined($dumpSeqFile) && -e $dumpSeqFile) {
	open(SEQDUMP,"<$dumpSeqFile") or die "couldn't open main sequences to dump file $dumpSeqFile\n";
	while(<SEQDUMP>){
		next if /^\s+/;
		next if /^#/;
		chomp;
		my ($seq, $freq) = split ;
		chomp $seq;
		$seq2Dump{$seq} = 1;
	}
	$seq2dumpFlag = 1;
}
my $increment = 100000;
open (SOLEXA,"<$fastQsourceFile" ) or die "could not open solexa file : $fastQsourceFile for reading\n";

my $readName ;
my $seq ;
my $quality_scores ;
my $counter = 0;
my $repArraySize = $#repeatSeqs + 1 ;
our $phredAvge ;

my ($minASCII, $maxASCII) = (256, -20);
my @ASCIIvals ;
my $totCount = 0;

my $repeatCount = 0;

while(<SOLEXA>)
{

	if ( /^@/ ) {  # start of fastq record
		$counter++;
		$readName = $_ ;
		chomp $readName;
		$seq = <SOLEXA>;
		chomp $seq;
		<SOLEXA>;
		if (/^\+$/) {<SOLEXA>;}
		else {
			$quality_scores=<SOLEXA>; chomp $quality_scores;
			my @strs = split('',$quality_scores);
			chomp @strs;
			foreach(@strs) {
				my $val = ord($_);
				#push @ASCIIvals, $val;
				if ($val < $minASCII){$minASCII = $val}
				if ($val > $maxASCII){$maxASCII = $val}
			}
		}	
		if ($seq2dumpFlag){
			if (exists($seq2Dump{$seq})){
				# print "dropseq $counter   ";
				next ;
			} # if dump seq then look to next solexa fastq record
		}
		my $repeatFlag = 0;
		if ($repeatFilter){
			for (my $k = 0; $k < ($repArraySize); $k++){
				if($seq =~ /($repeatSeqs[$k])/) {print GENREJECT "$readName-REPEAT\n$seq\n+\n$quality_scores\n"; $repeatFlag = 1; $repeatCount++; last;}
			}
		}
		if ($repeatFlag){next}
		#if ($convert_quals2fastq){ ($quality_scores, $phredAvge ) = split (/\s+/,&convert2fastQ($quality_scores) )}   # set to run in trim barcodes
		if ($trim_linkers){
			my $tempPointer = trimLinkers($readName, $seq, $quality_scores);
			my @retData = @{$tempPointer};
			$readName = $retData[0];
			$seq = $retData[1];
			$quality_scores = $retData[2];
			if ($quality_scores eq 'too short') {
				#print " $readName too short post linker  " ;
				$trimLinker2short++;
				#print "$trimLinker2short  ";
				next ;
			}
		}
		
		if ($trim_B_quals){
			my $tempPointer = trimBquals($readName, $seq, $quality_scores);  # latest illumina pipelines generate string of trailing B's for low quality discardable bases
			my @retData = @{$tempPointer};
			$readName = $retData[0];
			$seq = $retData[1];
			$quality_scores = $retData[2];
			if ($quality_scores eq 'too short') {
				#print " $readName too short post linker  " ;
				$trimBqualsTooshort++;
				#print "$trimLinker2short  ";
				next ;
			}
		}
		
		if ($barcodeFile){trimBarcodes($readName, $seq, $quality_scores)}
		
		else{
			
			#  if ($trimQualities){}  # do this in the subroutines
			if ($convert_quals2fastq){
				my ($quality_scores, $phredAvge ) = split (/\s+/,&convert2fastQ($quality_scores) );
				
				if (defined($phredQualityThreshold) && $phredAvge < $phredQualityThreshold) {print LOWPHRED "$readName\n$seq\n+\n$quality_scores\n"; next;}
				
			}
			if ($hitFile && $forkedLeftFile ){   # print read to main out if not in hit list
				if (exists($hits{"$readName"})) {print FLEFT "$readName\n$seq\n+\n$quality_scores\n"}   # print read to fork out if in hit list
				else  { print OVEROUT "$readName\n$seq\n+\n$quality_scores\n"} 
			}
			elsif ($hitFile ){   # otherwise print read to main out if not in hit list
				if (exists($hits{$readName})) {print OVEROUT "$readName\n$seq\n+\n$quality_scores\n";  } # print "got one $readName\n"}
				else {} #print "not found $readName    " }
			}
			else {}
			
		}
		my $kount = $counter/$increment;   #  my $increment = 100000;
		if (int($kount) * $increment eq  $counter ) {print "$counter  \n";}	
	}

}
#print "OLD:\t$quality_string\n";
#print "NEW:\t$newstring\n";

print "post trim linker (<16) shorties dropped :$trimLinker2short  ";

print "\nfinal vals max $maxASCII  min $minASCII \n\n";
print "repeat rejects are $repeatCount of tot reads $counter\n";
print "trim B qual strings $trimTrailingBstrings and too short $trimBqualsTooshort or $trimBquals2short\n" ;

exit(0);

sub trimBarcodes{
	my $bcodeNotFound = 1;
	my $numBarCodes = $#barcodeSeqs + 1;
	my $readName = shift;
	my $seq = shift;
	my $quals = shift;
	
	#print "readName $readName seq $seq quals $quals\n";
	# barcode arrays are file spanning globals
	
	my $trailingBarcodesFlag = 0;
	
	for (my $i=0; $i < $numBarCodes; $i++ ){
		my $offsetInStr = 0;
		if ($barcodeStarts[$i] > 10){
			$offsetInStr = $barcodeStarts[$i] - 2 ;  # back up a bit from the predicted length of the sequence before barcode should be found
			$trailingBarcodesFlag = 1; 
		
			my $seqA = substr($seq, 0, $offsetInStr);
			my $seqB = substr($seq, $offsetInStr); # $barcodeStarts[$i]
			my $qualA = substr($quals, 0, $offsetInStr);
			my $qualB = substr($quals, $offsetInStr);
			my $seqOld = $seq;
			my $qualsOld = $quals;
			#print "bcodes  $barcodeNames[$i] $barcodeSeqs[$i] $offsetInStr $seq $seqA $seqB\n";
			if ($seqB =~ /($barcodeSeqs[$i]\w+$)/){   # was 
				
			#if ($-[0] >= 4 ){print "barcode $barcodeSeqs[$i] string position is $-[0] , that is over 3 nt downstream of the designated start location $barcodeStarts[$i]\nplease fix\n"; exit 0}
			#elsif ($-[0]  < $barcodeStarts[$i]){print "barcode $barcodeSeqs[$i] is $-[0] , that is upstream of the designated start location $barcodeStarts[$i]\nplease fix\n"; exit 0}
			#if (1) {
				$seqB =~ s/($barcodeSeqs[$i]\w+$)//;
				$qualB = substr($qualB,0,length($seqB)) ;
				$quals = $qualA.$qualB;
				$seq = $seqA.$seqB;
				if ($convert_quals2fastq){ ($quality_scores, $phredAvge ) = split (/\s+/,&convert2fastQ($quals) );
					if (defined($phredQualityThreshold) && $phredAvge < $phredQualityThreshold) {print LOWPHRED "$readName\n$seqOld\n+\n$qualsOld\n"; return 0;}
				}

				print {$barCodeFilePointers[$i]} "$readName\n$seq\n+\n$quals\n";
				$bcodeNotFound = 0;
				print OVEROUT "$readName\n$seq\n+\n$quals\n";
				#print "out: $seqOld\n     $seqA $seqB $barcodeSeqs[$i]\n     $seq\n $qualsOld\n $qualA $qualB\n $quals\n $seq\n";
				return 0;
			}
		}  # trailing barcodes end
		
		else {  # barcodes are at start of reads   # this process can be changed to allow flexible location of start ie offset of +- 1 or even of a barcode length - but may not be worth the effort 
			my $leng = length($barcodeSeqs[$i]);
			my $leng0 = $leng - 1;
			my $seqA = substr($seq, 0, $leng);
			
			if ($seqA =~ /^($barcodeSeqs[$i]$)/){
				# exact barcode match at start of read	
				my $seqB = substr($seq, $leng); # $barcodeStarts[$i]
				my $qualA = substr($quals, 0, $leng);
				my $qualB = substr($quals, $leng);
				my $seqOld = $seq;
				my $qualsOld = $quals;
				if ($convert_quals2fastq){
					($quality_scores, $phredAvge ) = split (/\s+/,&convert2fastQ($quals) );
					if (defined($phredQualityThreshold) && $phredAvge < $phredQualityThreshold) {print LOWPHRED "$readName\n$seqOld\n+\n$qualsOld\n"; return 0;}
				}

				print {$barCodeFilePointers[$i]} "$readName\n$seqB\n+\n$qualB\n";
				$bcodeNotFound = 0;
				print OVEROUT "$readName\n$seq\n+\n$quals\n";
				#print "out: $seqOld\n     $seqA $seqB $barcodeSeqs[$i]\n     $seq\n $qualsOld\n $qualA $qualB\n $quals\n $seq\n";
				return 0;
			}
			
		}
		
	}
	if ($bcodeNotFound) {
		print NOBCODE "$readName\n$seq\n+\n$quals\n";
		#print "no barcodes ( $joinedUpBcodes ) found in $seq after $barcodeStarts[0] bases\n";
	}	
}

sub convert2fastQ {
        my $qstring = shift;
        my @quals = split("",$qstring);
        my $qual="";
        my $scale_factor=31;  # diff between Qphred+64  and Sanger Qphred+33
	my $phred ;
	my $phredAverage = 0;
        foreach my $q (@quals) {
                my $newval  = ord($q) - $scale_factor;
		$phredAverage += $newval - $scale_factor; # equivalent to the original SolexaQual - 64
                my $newqual = chr($newval);
                $qual.=$newqual;
        }
	$phredAverage /= ($#quals + 1) ;
        return "$qual $phredAverage";
}

sub trimLinkersBack{
	my $bcodeNotFound = 1;
	my $numBarCodes = $#barcodeSeqs + 1;
	my $readName = shift;
	my $seq = shift;
	my $quals = shift;
	
	# barcode arrays are file spanning globals
	
	for (my $i=0; $i < $numBarCodes; $i++ ){
		my $offsetInStr = $barcodeStarts[$i] - 2 ;  # back up a bit from the predicted length of the sequence before barcode should be found
		my $seqA = substr($seq, 0, $offsetInStr);
		my $seqB = substr($seq, $offsetInStr); # $barcodeStarts[$i]
		my $qualA = substr($quals, 0, $offsetInStr);
		my $qualB = substr($quals, $offsetInStr);
		my $seqOld = $seq;
		my $qualsOld = $quals;
		#print "bcodes  $barcodeNames[$i] $barcodeSeqs[$i] $offsetInStr $seq $seqA $seqB\n";
		if ($seqB =~ /($barcodeSeqs[$i]\w+$)/){   # was 
			
			#if ($-[0] >= 4 ){print "barcode $barcodeSeqs[$i] string position is $-[0] , that is over 3 nt downstream of the designated start location $barcodeStarts[$i]\nplease fix\n"; exit 0}
			#elsif ($-[0]  < $barcodeStarts[$i]){print "barcode $barcodeSeqs[$i] is $-[0] , that is upstream of the designated start location $barcodeStarts[$i]\nplease fix\n"; exit 0}
			#if (1) {
				$seqB =~ s/($barcodeSeqs[$i]\w+$)//;
				$qualB = substr($qualB,0,length($seqB)) ;
				$quals = $qualA.$qualB;
				$seq = $seqA.$seqB;
				if ($convert_quals2fastq){ ($quality_scores, $phredAvge ) = split (/\s+/,&convert2fastQ($quals) );
					if (defined($phredQualityThreshold) && $phredAvge < $phredQualityThreshold) {print LOWPHRED "$readName\n$seqOld\n+\n$qualsOld\n"; return 0;}
				}

				print {$barCodeFilePointers[$i]} "$readName\n$seq\n+\n$quals\n";
				$bcodeNotFound = 0;
				print OVEROUT "$readName\n$seq\n+\n$quals\n";
				#print "out: $seqOld\n     $seqA $seqB $barcodeSeqs[$i]\n     $seq\n $qualsOld\n $qualA $qualB\n $quals\n $seq\n";
				return 0;
			}
		
	}
	if ($bcodeNotFound) {
		print NOBCODE "$readName\n$seq\n+\n$quals\n";
		#print "no barcodes ( $joinedUpBcodes ) found in $seq after $barcodeStarts[0] bases\n";
	}
	my @retData ;
	push @retData, $readName ;
	push @retData, $seq  ;
	push @retData, $quals  ;
	return  \@retData ;
}

sub trimLinkers
{
	my $linkerNotFound = 1;
	#my $numBarCodes = $#barcodeSeqs + 1;
	my $readName = shift;
	my $seq = shift;
	my $quals = shift;
	
	my $numLinkers = $#linkers  + 1;
	my $qualsLeft = '';
	my $seqLeft   = '';
	#print "num linkers is  $numLinkers \n" ;
	# barcode arrays are file spanning globals
	my @retData ;
	
	my $trim1 = 0;
	my $trim2 = 0;
	my $trimShort = 0 ;
	for (my $i=0; $i < $numLinkers ; $i++ ){
		if (length($seq) >= length($linkers[$i])){
			if ($seq =~ /$linkers[$i]$/) { # this looks at end of lines, but linker frags could be found further inboard, but what minimum should use to dump seq 6 chars is 1/(16^3)  ie 1/4096 by chance but string is 30nt long
				my $hit = rindex ($seq, $linkers[$i]) ;  # cannot do this with index as shorter truncated linkers must anchor to end of sequence, rindex should give last occurrence

				$seqLeft = substr($seq,0,$hit) ;
				$qualsLeft = substr($quals,0,$hit) ;
				if ($hit < $shorties){
					$qualsLeft = 'too short';  # tell the caller to dump the sequence as too short to be specific mapping
					$trimShort = 1 ;
					# print " skip short: $seqLeft  " ;
				}   
				$seq = $seqLeft;
				$quals = $qualsLeft  ;
				if (!$trim1) {$trim1 = 1;}
				if ( $trim1) {$trim1++ ;}

			}
		}
		#elsif ($trim1) {last} # let it run as will go through to shorter linker frags if let loop further
		else {next}
	}
	
	if ($trim1 == 1){  # run the linker removal again to see if fragments of linker still left in read
		for (my $i=0; $i < $numLinkers ; $i++ ){
		if (length($seq) >= length($linkers[$i])){
			if ($seq =~ /$linkers[$i]$/) { # this looks at end of lines, but linker frags could be found further inboard, but what minimum should use to dump seq 6 chars is 1/(16^3)  ie 1/4096 by chance but string is 30nt long
				my $hit = rindex ($seq, $linkers[$i]) ;  # cannot do this with index as shorter truncated linkers must anchor to end of sequence, rindex should give last occurrence

				$seqLeft = substr($seq,0,$hit) ;
				$qualsLeft = substr($quals,0,$hit) ;

				if ($hit < $shorties){
					$qualsLeft = 'too short';  # tell the caller to dump the sequence as too short to be specific mapping
					$trimShort = 1 ;
					# print " skip short: $seqLeft  " ;
				}   
				$seq = $seqLeft;
				$quals = $qualsLeft  ;
				$trim2 = 1;

			}
		}
		#elsif ($trim2) {last}  # let it run as will go through to shorter linker frags if let loop further
		else {next}
	}
	}
	
	if (0)  # hang onto this stuff
	{
				push @retData, $readName ;
				push @retData, $seqLeft  ;
				push @retData, $qualsLeft  ;
				return  \@retData ;
	}
						
				#cf regex
				#my $whole = "Can someone tell me how to find the last index of s substring in a string which is repeated several times in the string?";
				#my $piece = "string";
				#my $place;
				#if ($whole =~ m { .* \Q$piece\E }gsx) {
				#    $place = pos($whole) - length($piece);
				#    print "Last found it at position $place\n";
				#} else {
				#    print "Couldn't find it\n";
				#}
		
		
	#my $offsetInStr = $barcodeStarts[$i] - 2 ;  # back up a bit from the predicted length of the sequence before barcode should be found
	#my $seqA = substr($seq, 0, $offsetInStr);
	#my $seqB = substr($seq, $offsetInStr); # $barcodeStarts[$i]
	#my $qualA = substr($quals, 0, $offsetInStr);
	#my $qualB = substr($quals, $offsetInStr);
	#my $seqOld = $seq;
	#my $qualsOld = $quals;
	#@linkerSeqs ;
	#@linkerMin ;
	#@linkerMultipliers ;
	#@linkerNames ;
	

	push @retData, $readName ;
	push @retData, $seq  ;
	push @retData, $quals  ;
	return  \@retData ;
}



sub trimBquals
{
	#reused from trimLinkers so some variables left as before
	my $BstringNotFound = 1;
	#my $numBarCodes = $#barcodeSeqs + 1;
	my $readName = shift;
	my $seq = shift;
	my $quals = shift;
	
	
	my $numBstrings = $#Bstrings  + 1;
	#my $numLinkers = length($seq) + 1;  # actually is for generating an expanded set of strings of multiple Bs
	my $qualsLeft = '';
	my $seqLeft   = '';
	#print "num Bstrings is  $numBstrings \n" ;
	# barcode arrays are file spanning globals
	my @retData ;
	
	my $trim1 = 0;
	my $trim2 = 0;
	my $trimShort = 0 ;
	for (my $i=0; $i < $numBstrings ; $i++ ){
		if (length($quals) >= length($Bstrings[$i])){
			if ($quals =~ /$Bstrings[$i]$/) { # this looks at end of lines, but linker frags could be found further inboard, but what minimum should use to dump seq 6 chars is 1/(16^3)  ie 1/4096 by chance but string is 30nt long
				my $hit = rindex ($quals, $Bstrings[$i]) ;  # cannot do this with index as shorter truncated Bstrings must anchor to end of sequence, rindex should give last occurrence
				
				$seqLeft = substr($seq,0,$hit) ;
				$qualsLeft = substr($quals,0,$hit) ;
				if ($hit < $shorties){
					$qualsLeft = 'too short';  # tell the caller to dump the sequence as too short to be specific mapping
					$trimShort = 1 ;
					# print " skip short: $seqLeft  " ;
				}   
				$seq = $seqLeft;
				$quals = $qualsLeft  ;
				if (!$trim1) {$trim1 = 1;}
				if ( $trim1) {$trim1++ ; }

			}
		}
		#elsif ($trim1) {last} # let it run as will go through to shorter linker frags if let loop further
		else {next}
	}
	if ($trim1){$trimTrailingBstrings++;}
	if ($trimShort){$trimBqualsTooshort++, }
	push @retData, $readName ;
	push @retData, $seq  ;
	push @retData, $quals  ;
	return  \@retData ;
}


sub revcomLine {
    my $inSeq = shift;
    chomp $inSeq;
    $inSeq =~ tr  [ATCGatcg.] [TAGCtagc.]
      ; #  or tr/a-z/A-Z/;   # shift all to upper case   [ATCGatcg] [TAGCtagc] ;    [ACGTRYMKSWBDHVNacgtrymkswbdhvn]  [TGCAYRKMWSVHDBNtgcayrkmwsvhdbn];  #
    $inSeq = reverse $inSeq;
    return $inSeq;
}

# example lines from fastq file      65 nt reads   qual on +HWI lines   /usr/local/work/CRC-SNP/CRCNGen/s_4_sequence_fastq.txt

#   @HWI-EAS382:4:1:6:1173#0/1
#   AAGACGCGCGTCTANAGGGACACAGCNGAGCCAAACTGGAACGAGGTGAGGAACTGATTCCACAA
#   +HWI-EAS382:4:1:6:1173#0/1
#   aaRa_U`]^MWa_XDZOMXa__]_VZDR^Y[```]`\QP\aYVYVPQLYQL[a^ZY]^^\aa__Z
#   @HWI-EAS382:4:1:6:172#0/1
#   TTGCTGAAGATGTCACTTCTTTTGTTNCTTCTTTATAGTTCCCCACCATTGATTTTTTTTTAATG
#   +HWI-EAS382:4:1:6:172#0/1
#   `_V``aaa^``_^a_`a``aa`_\\XDZ__a^_^`^_`W_a``_a`___`V[^a_``a_a_aa_Y
#   @HWI-EAS382:4:1:6:1501#0/1
#   TTATTGGTCTTAATGGGGCTATTAGTNTTCATGGGACAGTCTTTGAAATTCTGGAGAGCTTCACT
#   +HWI-EAS382:4:1:6:1501#0/1
#   aaaa_S[X``_Z\_Z]WR]_``_`[NDT^^^YOMQT]\YW^`_^QYX]_^]ZLLWRVJY^____^
#@HWI-EAS382:4:1:6:533#0/1
#AGCCTCTGCCCTTCTTTTTCTTACTTNAGAACTTGTAATGGTGTTAGGGAACAGTTGTAGGGGCA
#+HWI-EAS382:4:1:6:533#0/1
#aaabaa_^baa^``\_a`aa^_aa^VDT`_a`]^`^aa]`_]a]_a]U]`_`^XTa`Ya`Z^\`a
#@HWI-EAS382:4:1:6:1434#0/1
#CATCACATCTCTTAGACAACTCAAATNATGCCCACACAACACACACACGCCTCTTCTACCTATCC
#+HWI-EAS382:4:1:6:1434#0/1
#^abbba^`]baaaa]a\aaa[b`a`^D\`]`aabaa`a`aaaaabba`Uaa__a`a[XYQ\``^V
#@HWI-EAS382:4:1:6:571#0/1
#TTCAAAGGTGCTGACCTTGACCTGTGNAGAGCCACCGTCTCCTAGGGAGACTGTGATGCTTACAC
#+HWI-EAS382:4:1:6:571#0/1
#abbbbbaa^aba_baa``_aaa`_UND[^a^aaaaaZ\a``a_aVJS]^^``^\\]^[]a_a__`

#but when run  maq sol2sanger s_4_sequence_fastq.txt s_4_sequence.fastq  get :   /usr/local/work/CRC-SNP/CRCNGen/s_4_sequence.fastq

#@HWI-EAS382:4:1:6:1173#0/1
#AAGACGCGCGTCTANAGGGACACAGCNGAGCCAAACTGGAACGAGGTGAGGAACTGATTCCACAA
#+
#BB3B@6A>?.8B@9&;0.9B@@>@7;&3?:<AAA>A=21=B:7:712-:2-<B?;:>??=BB@@;
#@HWI-EAS382:4:1:6:172#0/1
#TTGCTGAAGATGTCACTTCTTTTGTTNCTTCTTTATAGTTCCCCACCATTGATTTTTTTTTAATG
#+
#A@7AABBB?AA@?B@ABAABBA@==9&;@@B?@?A?@A8@BAA@BA@@@A7<?B@AAB@B@BB@:
#@HWI-EAS382:4:1:6:1501#0/1
#TTATTGGTCTTAATGGGGCTATTAGTNTTCATGGGACAGTCTTTGAAATTCTGGAGAGCTTCACT
#+
#BBBB@4<9AA@;=@;>83>@AA@A</&5???:0.25>=:8?A@?2:9>@?>;--837+:?@@@@?
#@HWI-EAS382:4:1:6:533#0/1
#AGCCTCTGCCCTTCTTTTTCTTACTTNAGAACTTGTAATGGTGTTAGGGAACAGTTGTAGGGGCA
#+
#BBBCBB@?CBB?AA=@BABB?@BB?7&5A@BA>?A?BB>A@>B>@B>6>A@A?95BA:BA;?=AB
#@HWI-EAS382:4:1:6:1434#0/1
#CATCACATCTCTTAGACAACTCAAATNATGCCCACACAACACACACACGCCTCTTCTACCTATCC
#+
#?BCCCB?A>CBBBB>B=BBB<CABA?&=A>ABBCBBABABBBBBCCBA6BB@@BAB<9:2=AA?7


