#!/usr/bin/perl -w

use POSIX ;
use strict;
use diagnostics;
use English;
use Getopt::Long;
use Data::Dumper;
#use IO::File ;




                                                                                                                                            
$Data::Dumper::Indent = 1;                                                                                                                  
$Data::Dumper::Useqq  = 1;                                                                                                                  
$Data::Dumper::Purity = 1;

my $species = 'hg18';  # 'mm9' ;
my $descript = 'CLIPsRNA' ;  # 'Cleave' ; 'CLIPmRNA'

my ($usage) = <<'USAGE';                                                                                                                    

USAGE:

All scripts were written using perl version 5.10.0

whatGenePositionMultiTissue.pl

This script reads in a refSeq or other gene description UCSC or other such file and populates a
hash structure with protein coding gene structural locations wrt chromosome positions.

Output consists of listing of all genes that have at least one target site for all of the miRNAs of interest
together with the target site types (conservation, set type and count).
	
#FILES
README - this information file

#------------------

on z800	./whatGenePositionNonUnique.pl -i ../mm9kgucsc.1 -b Liver.bed -r Liver.NonMito.Bowtiehits -o LiverfilteredHits
	
	./whatGenePosition.pl -i ~/myWork/miRNAtargetScanning/mouseMiRNA/mm9kgucsc.1 -b testOut.bed -r brainNoMitoNoMiss -o filteredLocationOut         was run in ~/myWork/CRCfindingSNPs
can cut and paste this foreach set under tcsh	
foreach
f ( Brain Liver Lung Ovary Spleen Kidney )
echo $f
./whatGenePosition.pl -i ../mm9kgucsc.1 -b $f'.bed' -r $f'.NonMito.Bowtiehits' -o $f'filteredHits'
./bed2browser.py $f'ByChr.bed' 145871067 4 > $f'ByChr.html'
end

  ./whatGenePosition.pl -i hgTableKnownGene -o filteredLocationOut
  
  ./whatGenePosition.pl -i hgTableKnownGene -b testOut.bed -r brainNoMitoNoMiss -o filteredLocationOut


 ./whatGenePosition.pl -i /Users/ihijszu/myWork/SLDEs/rnafolding/files/hgTableKnownGene -o filteredLocationOut
 
 or for mouse :  /Users/ihijszu/myWork/miRNAtargetScanning/mouseMiRNA/mm9kgucsc.1
  
eg  ./filterKnownSNPs.pl -g MF -k 3 -n 100 -c None -o scoresOut.tx -d backAllTNT3pUTR -t h -s scoringGOdistribs/GMrgScore3 
prints out an html linked version where the files are in a web server html dir subdir and are linked on each gene name.

If option to filter by barcode is supplied then any records without a barccode found will be placed in the noBarcodeFound file

Usage: $0 <options> fasta_file
    Options
	
    	-i	: input of gene source information file  
        -o      : main output file of reads matching the hits file
	-r	: read hits input file
	-b	: bed output file
        -h      : help and exit
        --help	: help and exit 
	

USAGE



#if ( $#ARGV < 1 ) {
    if (0) {
    print $usage;                                                                                                                           
    exit;                                                                                                                                   
}
our $trimQualities = 0;
our $convert_quals2fastq = 0;
our $phredQualityThreshold;
my (
    $geneSourceFile,
    $hitFile,
    $outFile,
#    $forkedLeftFile,
#    $dumpSeqFile,
#    $trimFile,
    $bedFile,
    $help
);

&GetOptions(	"i=s"=>\$geneSourceFile,
                "r=s"=>\$hitFile,
                "o=s"=>\$outFile,
#                "f=s"=>\$forkedLeftFile,
#		"d=s"=>\$dumpSeqFile,
#		"t=s"=>\$trimFile,
		"b=s"=>\$bedFile,
#		"q=s"=>\$trimQualities,
#		"c=i"=>\$convert_quals2fastq,
#		"p=i"=>\$phredQualityThreshold,
                "h"=>\$help,
                "help"=>\$help);

	if( $help ) {
    		&show_help();
    		exit(1);
	}    
my (
	$trim_linkers,
	$newstring
   );
#if (defined($trimFile)) {
#	if (-e $trimFile) {$trim_linkers = 1}
#	else{$trim_linkers = 0}
#}


#uc001bkm.1	chr1	+	25816545	25983527	25816875	25982867	12	25816545,25885517,25945745,25948332,25952561,25957669,25962951,25970734,25977182,25980017,25981662,25982740,	25817415,25885614,25945861,25948413,25952656,25957787,25963045,25970850,25977402,25980190,25981778,25983527,	Q9NR34	uc001bkm.1
#uc001bkn.1	chr1	+	25979791	25983527	25980127	25982867	3	25979791,25981662,25982740,	25980190,25981778,25983527,		uc001bkn.1
#uc001bko.1	chr1	+	25999253	26017300	25999308	26014796	13	25999253,26000120,26001093,26004219,26007657,26008103,26008760,26010531,26010768,26011764,26012958,26013154,26014625,	25999491,26000238,26001195,26004353,26007867,26008228,26008898,26010613,26010957,26011870,26013071,26013256,26017300,	NP_065184	uc001bko.1
#uc001bkp.1	chr1	+	25999253	26017300	25999308	26014796	12	25999253,26000120,26004219,26007657,26008103,26008760,26010531,26010768,26011764,26012958,26013154,26014625,	25999491,26000238,26004353,26007867,26008228,26008898,26010613,26010957,26011870,26013071,26013256,26017300,	NP_996809	uc001bkp.1
#uc001bkq.1	chr1	+	26019047	26032017	26022191	26031104	7	26019047,26022096,26022721,26025379,26025692,26028586,26030998,	26019107,26022206,26022826,26025489,26025904,26028908,26032017,	Q9NUI7	uc001bkq.1
#uc001bkr.1	chr1	+	26019047	26032017	26022191	26028862	7	26019047,26022096,26022721,26025379,26025692,26028695,26030998,	26019107,26022206,26022826,26025489,26025904,26028908,26032017,	Q63HP1	uc001bkr.1
#uc001bks.1	chr1	+	26019270	26032017	26022182	26031104	7	26019270,26022096,26022721,26025379,26025692,26028586,26030998,	26019492,26022206,26022826,26025489,26025904,26028908,26032017,	NP_062457	uc001bks.1
#uc001bkt.1	chr1	+	26019905	26032017	26022191	26031104	7	26019905,26022096,26022721,26025379,26025692,26028586,26030998,	26020081,26022206,26022826,26025489,26025904,26028908,26032017,	Q9NUI7	uc001bkt.1
#uc001bku.1	chr1	+	26021607	26032017	26022191	26028862	6	26021607,26022721,26025379,26025692,26028695,26030998,	26022206,26022826,26025489,26025904,26028908,26032017,	Q63HP1	uc001bku.1
#uc001bkv.1	chr1	+	26024709	26032017	26025744	26031104	3	26024709,26028586,26030998,	26025904,26028908,26032017,	Q7Z2S7	uc001bkv.1
#uc001bkw.1	chr1	-	26033083	26058435	26034070	26058435	3	26033083,26036490,26058357,	26034924,26036632,26058435,	Q9H7T9	uc001bkw.1
#uc001bkx.1	chr1	-	26060561	26070331	26061876	26062917	2	26060561,26069686,	26062939,26070331,	Q86WK9	uc001bkx.1
#uc001bky.1	chr1	-	26083258	26091685	26083771	26084392	2	26083258,26091591,	26084947,26091685,	NP_689710	uc001bky.1

my $info =<< 'INFO';
Description of the ucsc knownGene table and file format:
name 		uc007aet.1	varchar(255) 	values 		Name of gene
chrom 		chr1		varchar(255) 	values 		Reference sequence chromosome or scaffold
strand 		-		char(1) 	values 		+ or - for strand
txStart 	3195984		int(10) 	unsigned 	range 	Transcription start position
txEnd 		3205713		int(10) 	unsigned 	range 	Transcription end position
cdsStart 	3195984		int(10) 	unsigned 	range 	Coding region start
cdsEnd 		3195984		int(10) 	unsigned 	range 	Coding region end
exonCount 	2		int(10) 	unsigned 	range 	Number of exons
exonStarts 	3195984,3203519,		longblob 	Exon start positions
exonEnds 	3197398,3205713,		longblob 	Exon end positions
proteinID 	 		varchar(40) 	values 		UniProt display ID for Known Genes, UniProt accession or RefSeq protein ID for UCSC Genes
alignID 	uc007aet.1	varchar(255) 	values 		Unique identifier for each (known gene, alignment position) pair


we need to have a way to trawl chromosomes quickly, to find overlap of a read with gene regions of interest. hash key is good for getting chromosome data,
then an array pair with start and end of feature locations and an efficient search method would be best. Or is there a better indexing method to speed this up?

INFO





open(GENEIN,"<$geneSourceFile") or die "couldn't open known gene input file $geneSourceFile\n";
open(FOUT,">$outFile")    or die "couldn't open main output file $outFile\n";
open(HITSIN,"<$hitFile")  or die "couldn't open hit input file $hitFile\n";
open(BEDOUT,">$bedFile") or die "couldn't open bed format output file $bedFile \n";
my $bNme = $bedFile;
$bNme =~ s/.bed// ;
my $freqFile = $bNme.'ByFreqNonUnique.txt'; # html
my $bedFile2 = $bNme.'ByChrNonUnique.bed' ;
open(BEDOUT2,">$bedFile2") or die "couldn't open bed format output file $bedFile2 \n";
my $bedFile3 = $bNme.'MultiByChr.bed' ;
open(BEDOUT3,">$bedFile3") or die "couldn't open bed format output file $bedFile3 \n";
print BEDOUT 'track name="'.$species.$descript.$bNme.'" description="pred '.$species.' '.$bNme.'" visibility=2 url='.$species.$descript.$bNme.' color="190,80,80" useScore=1'."\n";
print BEDOUT2 'track name="'.$species.$descript.$bNme.'2" description="pred '.$species.' '.$bNme.'2" visibility=2 url='.$species.$descript.$bNme.'2 color="190,80,80" useScore=1'."\n";
print BEDOUT3 'track name="'.$species.$descript.$bNme.'3" description="pred '.$species.' '.$bNme.'3" visibility=2 url=h'.$species.$descript.$bNme.'3 color="190,80,80" useScore=1'."\n";
open(FREQOUT,">$freqFile") or die "couldn't open html format output file $freqFile \n";

#print BEDOUT "$chr\t$locn\t$locInc\t$howMany$readPat\t960\t$strand\n" ;
      

#my %features;
our %chrMapBeg;
our %chrMap ;
our %Kgenes ; my @KgNums ;
my $geneCounter = -1;
my $header = <GENEIN>; print "$header\n";
while(<GENEIN>) {
	next if /^\s+/;
	my @fields = split(/\t/,$_); chomp @fields;
	#print "res is $fields[0]  $fields[1] $fields[2] $fields[3] :$fields[4] :$fields[7] :$fields[8] :$fields[9] :$fields[10]\n";
	my $kgID = $fields[0];
	my $chr = $fields[1];
	my $strand = $fields[2];
	my $geneBeg = $fields[3];
	my $geneEnd = $fields[4];
	my $cdsBeg = $fields[5];
	my $cdsEnd = $fields[6];	
	my $numExons = $fields[7];
	my @exonsBeg = split (/,/,$fields[8]);
	my @exonsEnd = split (/,/,$fields[9]);
	my $protId = $fields[10];
	my $aliId = $fields[11];
	
	if (!defined($chrMapBeg{$chr}->{$geneBeg})) {$chrMapBeg{$chr}->{$geneBeg} = $geneEnd} # at gene begin in chr array, push the geneCount identifier and gene end pair
	else {$geneBeg++;$chrMapBeg{$chr}->{$geneBeg} = $geneEnd}
	
	
	if (!exists($Kgenes{$kgID})) {
		$geneCounter++;
		#$KgNums[$geneCounter] = $_;
		$KgNums[$geneCounter]{kgID} = $kgID;
		$KgNums[$geneCounter]{chr} = $chr;
		$KgNums[$geneCounter]{strand} = $strand;
		if ($strand eq '+') {$KgNums[$geneCounter]{sense} = 1}  # will not exist if anti-sense strand
		$KgNums[$geneCounter]{geneBeg} = $geneBeg;
		$KgNums[$geneCounter]{geneEnd} = $geneEnd;
		$KgNums[$geneCounter]{cdsBeg} = $cdsBeg;
		$KgNums[$geneCounter]{cdsEnd} = $cdsEnd;
		if ($cdsBeg eq $cdsEnd) {$KgNums[$geneCounter]{nonCoding} = 1}  # will not exist if anti-sense strand
		$KgNums[$geneCounter]{numExons} = $numExons;
		$KgNums[$geneCounter]{exonsBeg} = \@exonsBeg;
		$KgNums[$geneCounter]{exonsEnd} = \@exonsEnd;
		$KgNums[$geneCounter]{protId} = $protId;
		$KgNums[$geneCounter]{aliId} = $aliId;
		$KgNums[$geneCounter]{geneNames} = '';
		$Kgenes{$kgID} = $geneCounter;
		$chrMap{$chr}->{$geneBeg} = $geneCounter;
	}
	#if (exists($chrMap{$chr} )){}
	#else {
	#for (my $i = $geneBeg; $i < $geneEnd; $i++){

	#}

}

our %geneMap;
foreach my $chr (sort {$a cmp $b} keys %chrMap) {
	#print "\nchromosome is $chr:  " ;
	foreach (sort {$a <=> $b} keys %{$chrMapBeg{$chr}}) {
		push @{$geneMap{$chr}}, $_   ;
		#print "  $_";
	}
}

# test hit
while(0){
#chrX   and  17728189
my $chr = 'chrX' ;
my $locn = 72262650;
print "\nchromosome is $chr:  " ;
for (my $i = 0; $i < $#{$geneMap{$chr}}; $i++){
	
	next if ((${$geneMap{$chr}}[$i]) > $locn) ;   # gene Begin 
	next if (($chrMapBeg{$chr}->{${$geneMap{$chr}}[$i]}) < $locn) ;  # gene End
	my $exonStarts = join(' ', @{$KgNums[$chrMap{$chr}->{${$geneMap{$chr}}[$i]}]{exonsEnd} });
	#print "target :$locn  begin  ${$geneMap{$chr}}[$i]  and end $chrMapBeg{$chr}->{${$geneMap{$chr}}[$i]} and exons starts $exonStarts\n";
	
}

}

my %readHits;  # key is read sequence and val is number of hits for each sequence plus the three
#   other fields strand chr start location (don't bother with  mismatch count or quality line)
#  need to ensure that the hit for a read sequence is the same location or flag an error
my %chrHits;
my %readMultiHits;
my %readMultiHitsDegeneracy ; # keep record on multiplicity of mapping locations to genome for a multiple hit read
my %chrMultiHits;
my @excludeTheseSeqs ;
my @removeTheseSeqs ;
my $previousReadID ='';

while(<HITSIN>) {    # this does the read counting
	next if /^\s+/;
	my @fields = split(/\t/,$_); chomp @fields;
	# HWI-EAS157:1:1:366:1486#0/1	+	chrM	9876	ATGCCATCTACCCTCTTCAAC	aaaa``\`\aa\FN^UTa``^	1	12:T>C
	#print "res is $fields[0]  $fields[1] $fields[2] $fields[3] :$fields[4] :$fields[7] :$fields[8] :$fields[9] :$fields[10]\n";
	my $readID = $fields[0];
	my $chr = $fields[2];
	my $strand = $fields[1];
	my $hitBeg = $fields[3];
	my $readSeq = $fields[4];
	$readMultiHitsDegeneracy{$readSeq} += $fields[6];    # same read should map to same numnber of locations so have one degeneracy value.
	                      # if there are multiple reads then the sum of reads should be divided by the degeneracy  to get a weighted frequency figure
	
	if ($chr eq 'chrM') {print "\nALERT : same read sequence maps to chrM - will kill as suspect";
		
		push (@excludeTheseSeqs, $readSeq) ;
		$previousReadID = $readID;
		next ;
		
	}
	elsif (exists($readHits{$readSeq}) and ($readHits{$readSeq}->{chr} ne $chr or $readHits{$readSeq}->{hitBeg} ne $hitBeg)) {
		print "\nALERT : same read sequence maps to different unique location";
		
		push(@removeTheseSeqs,$readSeq);
		if (exists($readMultiHits{$readSeq}->{$chr}->{$strand}->{$hitBeg})){
			$readMultiHits{$readSeq}->{$chr}->{$strand}->{$hitBeg}++ ;
			#$readMultiHitsDegeneracy{$readSeq} = 1 ;
		}
		else {
			$readMultiHits{$readSeq}->{$chr}->{$strand}->{$hitBeg} = 1 ;	
		}
		
	}
	elsif (exists($readHits{$readSeq})){$readHits{$readSeq}->{howMany}++}
	else {
		$readHits{$readSeq}->{howMany}=1;
		$readHits{$readSeq}->{chr}=$chr;
		$readHits{$readSeq}->{strand}=$strand;
		$readHits{$readSeq}->{hitBeg}=$hitBeg;	
	}
	$previousReadID = $readID;
}

foreach my $readSeq(@excludeTheseSeqs) {delete($readMultiHits{$readSeq}); delete($readHits{$readSeq})}
foreach my $readSeq(@removeTheseSeqs) {delete($readHits{$readSeq}) }

#foreach my $readSeq(@excludeTheseSeqs) {delete($readMultiHits{$readSeq}); delete($readHits{$readSeq})}
#foreach my $readSeq(@removeTheseSeqs) {delete($readHits{$readSeq}) }
my $patCounter= 0;
foreach my $readPat( keys %readHits) {
	my $chr = $readHits{$readPat}->{chr};
	my $beg = $readHits{$readPat}->{hitBeg};
	my $leng = length($readPat) ;
	$chrHits{$chr}->{$beg}->{$readPat}->{howMany} = $readHits{$readPat}->{howMany} ;
	$chrHits{$chr}->{$beg}->{$readPat}->{strand}  = $readHits{$readPat}->{strand} ;
	$chrHits{$chr}->{$beg}->{$readPat}->{hitBeg}  = $readHits{$readPat}->{hitBeg} ;
	$chrHits{$chr}->{$beg}->{$readPat}->{leng} = $leng ;
	#if ($chr eq 'chr18' && $beg == 65408577) {$patCounter++;  print "found a 65408577 patt $readPat num $patCounter\n"}
}



foreach my $readPat( keys %readMultiHits) {
	my $leng = length($readPat) ;
foreach	my $chr    ( keys%{$readMultiHits{$readPat}}) {
foreach	my $strand ( keys%{$readMultiHits{$readPat}->{$chr}}) {
foreach	my $beg    ( keys%{$readMultiHits{$readPat}->{$chr}->{$strand}}) {	
	
	if (!defined($readPat) || !defined($chr) || !defined($strand) || !defined($beg) ) {next}
	if (defined($readMultiHits{$readPat}->{$chr}->{$strand}->{$beg})) {
		$chrMultiHits{$chr}->{$beg}->{$readPat}->{howMany} = $readMultiHits{$readPat}->{$chr}->{$strand}->{$beg} ;
		$chrMultiHits{$chr}->{$beg}->{$readPat}->{strand}  = $strand ;
		$chrMultiHits{$chr}->{$beg}->{$readPat}->{hitBeg}  = $beg ;
		$chrMultiHits{$chr}->{$beg}->{$readPat}->{leng} = $leng ;
	}
}
}
}
}

foreach my $chr( keys %chrHits) {   # process in order by Chr then read start coordinate then read length  - good for stepping through genes and calculating hits
	foreach my $beg( sort {$a <=> $b} keys %{$chrHits{$chr}}) {   #  {$chrHits{$chr}->{$a} <=> $chrHits{$chr}->{$b} }  would be for sorting by value not key
		foreach my $readPat( sort keys %{$chrHits{$chr}->{$beg}}) {
			my $howMany  = $chrHits{$chr}->{$beg}->{$readPat}->{howMany} ;
			my $strand   = $chrHits{$chr}->{$beg}->{$readPat}->{strand}  ;
			my $locn     = $chrHits{$chr}->{$beg}->{$readPat}->{hitBeg}  ;
			my $leng     = $chrHits{$chr}->{$beg}->{$readPat}->{leng} ;
			
			#print "$chr  $beg  $strand  $howMany $leng";
			my $locInc  = $locn + $leng;
			my $howIntense = $howMany * 70; if ($howIntense > 900) {$howIntense = 999}
			print BEDOUT2 "$chr\t$locn\t$locInc\t${readPat}_$howMany\t$howIntense\t$strand\n" ;
		}	
	}
}


foreach my $chr( keys %chrMultiHits) {   # process in order by Chr then read start coordinate then read length  - good for stepping through genes and calculating hits
	foreach my $beg( sort {$a <=> $b} keys %{$chrMultiHits{$chr}}) {   #  {$chrHits{$chr}->{$a} <=> $chrHits{$chr}->{$b} }  would be for sorting by value not key
		foreach my $readPat( sort keys %{$chrMultiHits{$chr}->{$beg}}) {
			my $howMany  = $chrMultiHits{$chr}->{$beg}->{$readPat}->{howMany} ;
			my $strand   = $chrMultiHits{$chr}->{$beg}->{$readPat}->{strand}  ;
			my $locn     = $chrMultiHits{$chr}->{$beg}->{$readPat}->{hitBeg}  ;
			my $leng  = $chrMultiHits{$chr}->{$beg}->{$readPat}->{leng}; 
			
			#print "$chr  $beg  $strand  $howMany $leng";
			my $locInc  = $locn + $leng;
			my $degeneracyCount ='';
			my $howIntense = $howMany * 70; if ($howIntense > 900) {$howIntense = 999}
			if (exists($readMultiHitsDegeneracy{$readPat}) && defined ($readMultiHitsDegeneracy{$readPat})) { $degeneracyCount = $readMultiHitsDegeneracy{$readPat}+1 }
			print BEDOUT3 "$chr\t$locn\t$locInc\t${degeneracyCount}_${readPat}_$howMany\t$howIntense\t$strand\n" ;
		}	
	}
}


#sort {$a <=> $b}


my $hitsSeqToAGene = 0;
my $intergenicSeqHits = 0;
my $CodingSeqExonHit = 0;
my $UTR3pSeqHit = 0;
my $UTR5pSeqHit = 0;

my %gotThisHit ;

foreach my $readPat(reverse sort {$readHits{$a}->{howMany} <=> $readHits{$b}->{howMany}} keys %readHits) {
	# test hit
	my $dumpString = $readPat ;
	#chrX   and  17728189
	my $chr =  $readHits{$readPat}->{chr} ;#'chrX' ;
	my $locn = $readHits{$readPat}->{hitBeg} ;#72262650;
	my $locInc = $locn + length($readPat);
	my $strand = $readHits{$readPat}->{strand} ;
	my $howMany = $readHits{$readPat}->{howMany} ;
	#print BEDOUT "$chr\t$locn\t$locInc\t$howMany$readPat\t960\t$strand\n" ;
	#print "\nchromosome is $chr:  " ;
	#print FREQOUT "$chr $locn $strand $howMany $readPat\n";
	
	for (my $i = 0; $i < $#{$geneMap{$chr}}; $i++){
		
		next if ((${$geneMap{$chr}}[$i]) > $locn) ;   # gene Begin 
		next if (($chrMapBeg{$chr}->{${$geneMap{$chr}}[$i]}) < $locn) ;  # gene End  # two hash lookups in this loop must be slow  - may need to reformat this if slow
		my $exonStarts = join(' ', @{$KgNums[$chrMap{$chr}->{${$geneMap{$chr}}[$i]}]{exonsEnd} });
		#print "target :$locn  begin  ${$geneMap{$chr}}[$i]  and end $chrMapBeg{$chr}->{${$geneMap{$chr}}[$i]} and exons starts $exonStarts\n";
		my $utr3pHit= hit3pUTR($locn, $strand, $KgNums[$chrMap{$chr}->{${$geneMap{$chr}}[$i]}] ) ;
		if ($utr3pHit) {
			$UTR3pSeqHit++ ;
		}	
			if ($dumpString) {print FOUT "\nread pattern $dumpString 3pUTR hit with $howMany reads: " ; $dumpString = '';}
			#print "   $utr3pHit";
			#my $locMinus = $locn - 100; if ($locMinus < 0 ) {$locMinus = 0}
			#my $locPlus = $locInc + 100; #if ($locPlus < 0 ) {$locMinus = 0}
			
			if (!exists($gotThisHit{"$chr$strand$locn"})) {
				$gotThisHit{"$chr$strand$locn"}++;
				my $howIntense = $howMany * 70; if ($howIntense > 900) {$howIntense = 999}
				print BEDOUT "$chr\t$locn\t$locInc\t${readPat}_$howMany\t$howIntense\t$strand\n" ;
			}
		
		my $utr5pHit = hit5pUTR($locn, $strand, $KgNums[$chrMap{$chr}->{${$geneMap{$chr}}[$i]}] ) ;
		if ($utr5pHit) {  $UTR5pSeqHit++  }
		my $hitCodingExon= hitCodingExon($locn, $strand, $KgNums[$chrMap{$chr}->{${$geneMap{$chr}}[$i]}] ) ;
		if ($hitCodingExon) {  $CodingSeqExonHit++ }
		
	}

}

sub hit3pUTR {
	my $hitLoc = shift;
	my $strand = shift;
	my $geneParms = shift;
	my $cdsEnd = $geneParms->{cdsEnd} ;
	my $cdsBeg = $geneParms->{cdsBeg} ;
	if ($cdsBeg eq $cdsEnd) {return 0}  # cds beginning is usually set equal to cds end for non-coding genes
	if ($geneParms->{strand} eq '+' and  $hitLoc > $cdsEnd and $hitLoc < $geneParms->{geneEnd}) { return $geneParms->{kgID}}
	elsif ($geneParms->{strand} eq '-' and  $hitLoc < $cdsBeg and $hitLoc > $geneParms->{geneBeg}) { return $geneParms->{kgID}}
	else { return 0;}
}

sub hit5pUTR {
	my $hitLoc = shift;
	my $strand = shift;
	my $geneParms = shift;
	my $cdsEnd = $geneParms->{cdsEnd} ;
	my $cdsBeg = $geneParms->{cdsBeg} ;
	if ($cdsBeg eq $cdsEnd) {return 0}  # cds beginning is usually set equal to cds end for non-coding genes
	if ($geneParms->{strand} eq '+' and  $hitLoc < $cdsBeg and $hitLoc > $geneParms->{geneBeg}) { return $geneParms->{kgID}  }
	elsif ($geneParms->{strand} eq '-' and  $hitLoc > $cdsEnd and $hitLoc < $geneParms->{geneEnd}) { return $geneParms->{kgID}}
	else { return 0;}
}

sub hitSplice {
	my $hitLoc = shift;
	my $strand = shift;
	my $geneParms = shift;
	#$geneParms->{exonsBeg}->[0] ;
	my @exonBegs =  @{$geneParms->{exonsBeg}};
	my @exonEnds =  @{$geneParms->{exonsEnd}};
	#print "HERE THEY ARE : exon starts $exonStarts\n" ;
	my $i = 0;
	my $numExons = $geneParms->{numExons} ;
	my $spliceFuzz = 4 ;

	while ($i < $numExons){
		if (($hitLoc > $exonBegs[$i] - $spliceFuzz) and ($hitLoc < $exonBegs[$i] + $spliceFuzz)  and ($hitLoc < $exonEnds[$i])) { # hit close to an exon start
			return $geneParms->{kgID} ;
		}
		elsif (($hitLoc > $exonBegs[$i] ) and ($hitLoc > $exonEnds[$i] - $spliceFuzz)  and ($hitLoc < $exonEnds[$i] + $spliceFuzz )) { # hit close to an exon end
			return $geneParms->{kgID} ;
		}
		$i++;
	}
		
	

	if ($i eq $numExons) {
		return 0;   # not close to splice hit registered (may not be expected if original mapping did not include match to RNA)
	}	
}

sub hitCodingExon {
	my $hitLoc = shift;
	my $strand = shift;
	my $geneParms = shift;
	my $codeBeg = $geneParms->{cdsBeg};
	my $codeEnd = $geneParms->{cdsEnd} ;	
	my @exonBegs =  @{$geneParms->{exonsBeg}};
	my @exonEnds =  @{$geneParms->{exonsEnd}};
	my $i = 0;
	my $numExons = $geneParms->{numExons} ;

	#if ($geneParms->{strand} eq '+' and  $hitLoc < $geneParms->{cdsBeg} and $hitLoc > $geneParms->{geneBeg}) { return $geneParms->{kgID}  }
	#elsif ($geneParms->{strand} eq '-' and  $hitLoc > $geneParms->{cdsEnd} and $hitLoc < $geneParms->{geneEnd}) { return $geneParms->{kgID}}
	
	while ($i < $numExons){
		if (($hitLoc > $exonBegs[$i] ) and ($hitLoc < $exonEnds[$i]) and ($hitLoc > $codeBeg ) and ($hitLoc < $codeEnd )) { # hit in codon exon
			return $geneParms->{kgID} ;
		}
		$i++;
	}
	if ($i eq $numExons) {
		return 0;   # no close to splice hit registered (may not be expected if original mapping did not include match to RNA)
	}
	
}

sub hitCodingSynonymState {    #  if not present then work out codon map and write it into record when calculated for a gene for the first time
	my $hitLoc = shift;
	my $strand = shift;
	my $geneParms = shift;
	my @exonBegs =  @{$geneParms->{exonsBeg}};
	my @exonEnds =  @{$geneParms->{exonsEnd}};
	my $i = 0;
	my $numExons = $geneParms->{numExons} ;
	my $spliceFuzz = 15 ;
	if ($geneParms->{strand} eq '+' ) {
		#foreach my $exBegLocn ($exonBegs) {}
		while ($i < $numExons){
			if (($hitLoc > $exonBegs[$i] - $spliceFuzz) and ($hitLoc < $exonBegs[$i] + $spliceFuzz)  and ($hitLoc < $exonEnds[$i])) { # hit close to an exon start
				return $geneParms->{kgID} ;
			}
			elsif (($hitLoc > $exonBegs[$i] ) and ($hitLoc > $exonEnds[$i] - $spliceFuzz)  and ($hitLoc < $exonEnds[$i] + $spliceFuzz )) { # hit close to an exon end
				return $geneParms->{kgID} ;
			}
			$i++;
		}
		
	}
	elsif ($geneParms->{strand} eq '-' ){
		while ($i < $numExons){
			if (($hitLoc > $exonBegs[$i] - $spliceFuzz) and ($hitLoc < $exonBegs[$i] + $spliceFuzz)  and ($hitLoc < $exonEnds[$i])) { # hit close to an exon start
				return $geneParms->{kgID} ;
			}
			elsif (($hitLoc > $exonBegs[$i] ) and ($hitLoc > $exonEnds[$i] - $spliceFuzz)  and ($hitLoc < $exonEnds[$i] + $spliceFuzz )) { # hit close to an exon end
				return $geneParms->{kgID} ;
			}
			$i++;
		}		
	}
	else { return 0;}
	#$geneParms->{exonsBeg}->[0] ;
	#my $exonStarts = join(' ', @{$geneParms->{exonsBeg}});
	#print "HERER THEY ARE : exon starts $exonStarts\n" ;
	
}

	
my $guff =<< 'GUFF';
	my $seqobj = $seqio->next_seq;
6.      my $src    = $seqobj->id;
7.      my $length = $seqobj->length;
8.      my @cds    = grep { $_->primary_tag eq $ptag $seqobj->get_SeqFeatures;
9.      for my $feat(@cds) {
10.         for my $gene($feat->get_tag_values('gene')) {
11.             @{$features{$src}{$ptag}{$gene}{'product'}}     = $feat->get_tag_values('product');
12.             @{$features{$src}{$ptag}{$gene}{'protein_id'}}  = $feat->get_tag_values('protein_id');
13.             @{$features{$src}{$ptag}{$gene}{'translation'}} = $feat->get_tag_values('translation');
14.               $features{$src}{$ptag}{$gene}{'strand'}       = $feat->strand;
15.               $features{$src}{$ptag}{$gene}{'start'}        = $feat->start;
16.               $features{$src}{$ptag}{$gene}{'end'}          = $feat->end;
17. ## location
18. 	if($feat->location->isa('Bio::Location::SplitLocationI')) {
19. 	    my @locn = $feat->location->sub_Location;
20. 	    for(my $i = 0; $i <= $#locn; $i++) {
21. 		    @{$ret{$src}{$ptag}{$gene}{'exon'}[$i]} = ($locn[$i]->start,$locn[$i]->end);
22.                                            }
23.                                                               }
24. 	else {
25. 	        @{$ret{$src}{$ptag}{$gene}{'exon'}[0]} = ($feat->start,$feat->end);
26.          }
27.                                                     }
28. ## length of source feature
29.     $features{$src}{'length'} = $length;
30.                        }
31. return %features;

### check the position of SNP in protein, i.e. start / stop, change of
### amino acid
sub check{
  my $pos = shift;
 
  my $ref_ar = $refDNA[$pos];
  foreach (@$ref_ar) {
    my $posGene=$_;
    my $ref_Set=$$ref_Position{$_};
    my $genePID = $$ref_Set{pid};
   
    $resStr.= $$ref_Set{strand}."$SPACER";

    ### check the start stop, and write the result directly in the
    ### global var $resStr;
    checkStartStop($pos,$ref_Set,$posGene);

    if ($old eq '.') {
      $resStr.= "Ins$SPACER$SPACER-$SPACER-$SPACER";
      $Insert++;
      push @{ $hash_Diffgene{$genePID}{Ins}},$pos;
      push @{ $hash_Diffgene{$genePID}{qualIns} },$qualityMoreau;
    }
    elsif ($new eq '.') {
      $resStr.= "Del$SPACER$SPACER-$SPACER-$SPACER";
      $Deletion++;
      push @{ $hash_Diffgene{$genePID}{Del}},$pos;
      push @{ $hash_Diffgene{$genePID}{qualDel} },$qualityMoreau;
    }
    else {
      ### get codon
      checkCodon($pos,$ref_Set,$posGene);     
    }
    $resStr.= $$ref_Set{pid}." - ";
    # write the function / name of the protein
    $resStr.= substr ($$ref_Set{name},0,150);
    $resStr.= "\n";   
  }
  # TODO  if (scalar(@$ref_ar)==0) {
  #    print "empty..\n";
  #  }
}

### Check if the SNP effects a start or stop codon
sub checkStartStop{
  my $pos = shift;
  my $ref_Set = shift;
  my $posGene = shift;

  my $genePID = $$ref_Set{pid};
 
  ### check if start or stop
  if ($$ref_Set{start} <= $pos &&
      ($$ref_Set{start} + 2) >= $pos) {
    if ($$ref_Set{strand} eq '+') {
      $resStr.= "Start$SPACER";
      $Start++;
      $hash_Diffgene{$genePID}{Start}= 1;
      $hash_Diffgene{$genePID}{qualStart} =$qualityMoreau;
    }
    else {
      $resStr.= "Start$SPACER";
      $Start++;
      $hash_Diffgene{$genePID}{Start}= 1;
      $hash_Diffgene{$genePID}{qualStart} =$qualityMoreau;
    }
  }
  elsif (($$ref_Set{end}-2) <= $pos &&
         ($$ref_Set{end} >= $pos)) {
    if ($$ref_Set{strand} eq '+') {
      $resStr.= "Stop$SPACER";
      $Stop++;     
      $hash_Diffgene{$genePID}{Stop}= 1;
      $hash_Diffgene{$genePID}{qualStop} =$qualityMoreau;
    }
    else {
      $resStr.= "Stop$SPACER";
      $Stop++;     
      $hash_Diffgene{$genePID}{Stop}= 1;
      $hash_Diffgene{$genePID}{qualStop} =$qualityMoreau;
    }
  }
}  ### END check if start or stop


sub checkCodon{
  my $pos = shift;
  my $ref_Set = shift;
  my $posGene = shift;
  my $geneName = shift;
 
  my $str;
  if ($$ref_Set{strand} ne '+') {
    $str = revCom($$ref_Protein{$posGene});
  }
  else {
    $str = $$ref_Protein{$posGene};
  }
 
  if ($$ref_Set{strand}) {
    my ($startGene) = $posGene =~ /^(\d+)\.\./;
   
    my $GenePos = $pos  - $startGene;
   
    my $triplet;
    my $tripNew;
    if ($GenePos % 3 == 0) {
      $triplet = substr($str,($GenePos),3);
      $tripNew = $new.substr($str,($GenePos+1),2);
    }
    elsif ($GenePos % 3 == 1) {
      $triplet = substr($str,($GenePos-1),3);
      $tripNew =substr($str,($GenePos-1),1).$new.substr($str,($GenePos+1),1);
    }
    else {
      $triplet = substr($str,($GenePos-2),3);
      $tripNew =substr($str,($GenePos-2),2).$new;
    }
   
    my $tripBo = getTrip($triplet,$$ref_Set{strand});
    $resStr.= "$triplet$SPACER$tripNew$SPACER";
    checkStop($triplet,$tripNew,$ref_Set);
    my $tripMo = getTrip($tripNew,$$ref_Set{strand});
    $resStr.= "$tripBo$SPACER$tripMo$SPACER";
    if ($tripBo ne $tripMo) {
      $changeProtein++;
    }
    else {
      $nonChangeProtein++;
    }
  }
}

sub checkStop{
  my $old = shift;
  my $trip= shift;
  my $ref_Set = shift;
  my  $qualityMoreau = shift;
 
 
  if (uc($trip) eq  'TAA' or
      uc($trip) eq  'TAG' or
      uc($trip) eq  'TGA') # isStop($old) and
    {
      $hash_Diffgene{$$ref_Set{pid}}{NewStop}=1;
      $hash_Diffgene{$$ref_Set{pid}}{qualNewStop} =$qualityMoreau;    
    }
}

#will print triplet, and take care of strand...
sub getTrip{
  my $trip = shift;
  my $strand = shift;

  if ($strand ne '+') {
    $trip = revCom($trip);
    return $$geneCode{$trip};
  }
  else {
    return $$geneCode{$trip};
  }
}

GUFF


sub getAACode{
my %stdAminoAcidCode = (

    'TCA' => 'S',    # Serine
    'TCC' => 'S',    # Serine
    'TCG' => 'S',    # Serine
    'TCT' => 'S',    # Serine
    'TTC' => 'F',    # Phenylalanine
    'TTT' => 'F',    # Phenylalanine
    'TTA' => 'L',    # Leucine
    'TTG' => 'L',    # Leucine
    'TAC' => 'Y',    # Tyrosine
    'TAT' => 'Y',    # Tyrosine
    'TAA' => '*',    # Stop
    'TAG' => '*',    # Stop
    'TGC' => 'C',    # Cysteine
    'TGT' => 'C',    # Cysteine
    'TGA' => '*',    # Stop
    'TGG' => 'W',    # Tryptophan
    'CTA' => 'L',    # Leucine
    'CTC' => 'L',    # Leucine
    'CTG' => 'L',    # Leucine
    'CTT' => 'L',    # Leucine
    'CCA' => 'P',    # Proline
    'CCC' => 'P',    # Proline
    'CCG' => 'P',    # Proline
    'CCT' => 'P',    # Proline
    'CAC' => 'H',    # Histidine
    'CAT' => 'H',    # Histidine
    'CAA' => 'Q',    # Glutamine
    'CAG' => 'Q',    # Glutamine
    'CGA' => 'R',    # Arginine
    'CGC' => 'R',    # Arginine
    'CGG' => 'R',    # Arginine
    'CGT' => 'R',    # Arginine
    'ATA' => 'I',    # Isoleucine
    'ATC' => 'I',    # Isoleucine
    'ATT' => 'I',    # Isoleucine
    'ATG' => 'M',    # Methionine
    'ACA' => 'T',    # Threonine
    'ACC' => 'T',    # Threonine
    'ACG' => 'T',    # Threonine
    'ACT' => 'T',    # Threonine
    'AAC' => 'N',    # Asparagine
    'AAT' => 'N',    # Asparagine
    'AAA' => 'K',    # Lysine
    'AAG' => 'K',    # Lysine
    'AGC' => 'S',    # Serine
    'AGT' => 'S',    # Serine
    'AGA' => 'R',    # Arginine
    'AGG' => 'R',    # Arginine
    'GTA' => 'V',    # Valine
    'GTC' => 'V',    # Valine
    'GTG' => 'V',    # Valine
    'GTT' => 'V',    # Valine
    'GCA' => 'A',    # Alanine
    'GCC' => 'A',    # Alanine
    'GCG' => 'A',    # Alanine
    'GCT' => 'A',    # Alanine
    'GAC' => 'D',    # Aspartic Acid
    'GAT' => 'D',    # Aspartic Acid
    'GAA' => 'E',    # Glutamic Acid
    'GAG' => 'E',    # Glutamic Acid
    'GGA' => 'G',    # Glycine
    'GGC' => 'G',    # Glycine
    'GGG' => 'G',    # Glycine
    'GGT' => 'G',    # Glycine
    );
return \%stdAminoAcidCode;
}	

