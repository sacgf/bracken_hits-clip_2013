#!/usr/bin/perl -w

use POSIX ;
use strict;
use diagnostics;
use English;
use Getopt::Long;
use Data::Dumper;
use IO::File ;




                                                                                                                                            
$Data::Dumper::Indent = 1;                                                                                                                  
$Data::Dumper::Useqq  = 1;                                                                                                                  
$Data::Dumper::Purity = 1;


my ($usage) = <<'USAGE';                                                                                                                    

USAGE:

All scripts were written using perl version 5.10.0

whatGenePosition.pl

This script reads in a refSeq or other gene description UCSC or other such file and populates a
hash structure with protein coding gene structural locations wrt chromosome positions.

Output consists of listing of all genes that have at least one target site for all of the miRNAs of interest
together with the target site types (conservation, set type and count).
	
#FILES
README - this information file

#------------------
        ./whatCodonPosition.pl -i hgTableKnownGene -b testOut.bed -r crcchr15frequencies.txt -o filteredLocationOut -s chr15.fa
	./whatCodonPosition.pl -i hgTableKnownGene -b testOut.bed -r brainNoMitoNoMiss -o filteredLocationOut -s chrAll.fa     # latter is human genome
	
	./whatGenePosition.pl -i ~/myWork/miRNAtargetScanning/mouseMiRNA/mm9kgucsc.1 -b testOut.bed -r brainNoMitoNoMiss -o filteredLocationOut         was run in ~/myWork/CRCfindingSNPs

  ./whatGenePosition.pl -i hgTableKnownGene -o filteredLocationOut


 ./whatGenePosition.pl -i /Users/ihijszu/myWork/SLDEs/rnafolding/files/hgTableKnownGene -o filteredLocationOut
 
 or for mouse :  /Users/ihijszu/myWork/miRNAtargetScanning/mouseMiRNA/mm9kgucsc.1
  
eg  ./filterKnownSNPs.pl -g MF -k 3 -n 100 -c None -o scoresOut.tx -d backAllTNT3pUTR -t h -s scoringGOdistribs/GMrgScore3 
prints out an html linked version where the files are in a web server html dir subdir and are linked on each gene name.

If option to filter by barcode is supplied then any records without a barccode found will be placed in the noBarcodeFound file

Usage: $0 <options> fasta_file
    Options
	
    	-i	: input of gene source information file  
        -o      : main output file of reads matching the hits file
	-r	: read hits input file
	-s	: nucleotide sequence multifasta file, consistent with the gene source information file - ie from same build
	-b	: bed output file
        -h      : help and exit
        --help	: help and exit 
	
	eg ./trimNfilter.pl -i /usr/local/work/mmCleave/s_1_sequence_fastq.txt -b /usr/local/work/mmCleave/barcodes.file -c 1 -o testItOut.fastq
	./trimNfilter.pl -i /usr/local/work/mmCleave/s_1_sequence_fastq.txt -d dropTheseSeqs -b /usr/local/work/mmCleave/barcodes.file -c 1 -o testItOut.fastqRead
	/trimNfilter.pl -i /usr/local/work/mmCleave/s_1_sequence_fastq.txt -d dropTheseSeqs -b /usr/local/work/mmCleave/barcodes.file -p 20  -c 1 -o testItOut.fastqRead
	
	
	cp /usr/local/work/CRCSNPruns/crcchr4frequencies.txt .     
        cp /data/genomes/hg18/chr4.fa .
	cp /usr/share/rnafolding/hgTableKnownGene
        cp /usr/share/rnafolding/kg.txt ./hgTableKnownGene
	
	./whatCodonPosition.pl -i hgTableKnownGene -b testOutchr4.bed -r /usr/local/work/CRCSNPruns/crcchr4frequencies.txt -o filteredLocationOutchr4.fa -s /data/genomes/hg18/chr4.fa
	/data/genomes/hg18/chr4.fa
	
	./whatCodonPosition.pl -i hgTableKnownGene -b testOutchr2.bed -r /usr/local/work/CRCSNPruns/crcchr2frequencies.txt -o filteredLocationOutchr2 -s /data/genomes/hg18/chr2.fa
	
	./whatCodonPosition.pl -i hgTableKnownGene -b testOut.bed -r crcchr4frequencies.txt -o filteredLocationOut -s chr4.fa   /usr/local/work/CRCSNPruns/crcchr4frequencies.txt
	foreach f(chr1 chr2 chr3 chr4 chr5 chr6 chr7 chr8 chr9 chr10)
	echo processing $f
	'./whatCodonPosition.pl -i hgTableKnownGene -b testOut'$f'.bed -r crc'$f'frequencies.txt -o filteredLocationOut'$f' -s' $f'.fa /usr/local/work/CRCSNPruns/crc'$f'frequencies.txt'
	
	
USAGE



#if ( $#ARGV < 1 ) {
    if (0) {
    print $usage;                                                                                                                           
    exit;                                                                                                                                   
}
our $trimQualities = 0;
our $convert_quals2fastq = 0;
our $phredQualityThreshold;
my (
    $geneSourceFile,
    $hitFile,
    $outFile,
    $seqsFile,
#    $forkedLeftFile,
#    $dumpSeqFile,
#    $trimFile,
    $bedFile,
    $help
);

&GetOptions(	"i=s"=>\$geneSourceFile,
                "r=s"=>\$hitFile,
		"s=s"=>\$seqsFile,
                "o=s"=>\$outFile,
#                "f=s"=>\$forkedLeftFile,
#		"d=s"=>\$dumpSeqFile,
#		"t=s"=>\$trimFile,
		"b=s"=>\$bedFile,
#		"q=s"=>\$trimQualities,
#		"c=i"=>\$convert_quals2fastq,
#		"p=i"=>\$phredQualityThreshold,
                "h"=>\$help,
                "help"=>\$help);

	if( $help ) {
    		&show_help();
    		exit(1);
	}    
my (
	$trim_linkers,
	$newstring
   );
#if (defined($trimFile)) {
#	if (-e $trimFile) {$trim_linkers = 1}
#	else{$trim_linkers = 0}
#}


#uc001bkm.1	chr1	+	25816545	25983527	25816875	25982867	12	25816545,25885517,25945745,25948332,25952561,25957669,25962951,25970734,25977182,25980017,25981662,25982740,	25817415,25885614,25945861,25948413,25952656,25957787,25963045,25970850,25977402,25980190,25981778,25983527,	Q9NR34	uc001bkm.1
#uc001bkn.1	chr1	+	25979791	25983527	25980127	25982867	3	25979791,25981662,25982740,	25980190,25981778,25983527,		uc001bkn.1
#uc001bko.1	chr1	+	25999253	26017300	25999308	26014796	13	25999253,26000120,26001093,26004219,26007657,26008103,26008760,26010531,26010768,26011764,26012958,26013154,26014625,	25999491,26000238,26001195,26004353,26007867,26008228,26008898,26010613,26010957,26011870,26013071,26013256,26017300,	NP_065184	uc001bko.1
#uc001bkp.1	chr1	+	25999253	26017300	25999308	26014796	12	25999253,26000120,26004219,26007657,26008103,26008760,26010531,26010768,26011764,26012958,26013154,26014625,	25999491,26000238,26004353,26007867,26008228,26008898,26010613,26010957,26011870,26013071,26013256,26017300,	NP_996809	uc001bkp.1
#uc001bkq.1	chr1	+	26019047	26032017	26022191	26031104	7	26019047,26022096,26022721,26025379,26025692,26028586,26030998,	26019107,26022206,26022826,26025489,26025904,26028908,26032017,	Q9NUI7	uc001bkq.1
#uc001bkr.1	chr1	+	26019047	26032017	26022191	26028862	7	26019047,26022096,26022721,26025379,26025692,26028695,26030998,	26019107,26022206,26022826,26025489,26025904,26028908,26032017,	Q63HP1	uc001bkr.1
#uc001bks.1	chr1	+	26019270	26032017	26022182	26031104	7	26019270,26022096,26022721,26025379,26025692,26028586,26030998,	26019492,26022206,26022826,26025489,26025904,26028908,26032017,	NP_062457	uc001bks.1
#uc001bkt.1	chr1	+	26019905	26032017	26022191	26031104	7	26019905,26022096,26022721,26025379,26025692,26028586,26030998,	26020081,26022206,26022826,26025489,26025904,26028908,26032017,	Q9NUI7	uc001bkt.1
#uc001bku.1	chr1	+	26021607	26032017	26022191	26028862	6	26021607,26022721,26025379,26025692,26028695,26030998,	26022206,26022826,26025489,26025904,26028908,26032017,	Q63HP1	uc001bku.1
#uc001bkv.1	chr1	+	26024709	26032017	26025744	26031104	3	26024709,26028586,26030998,	26025904,26028908,26032017,	Q7Z2S7	uc001bkv.1
#uc001bkw.1	chr1	-	26033083	26058435	26034070	26058435	3	26033083,26036490,26058357,	26034924,26036632,26058435,	Q9H7T9	uc001bkw.1
#uc001bkx.1	chr1	-	26060561	26070331	26061876	26062917	2	26060561,26069686,	26062939,26070331,	Q86WK9	uc001bkx.1
#uc001bky.1	chr1	-	26083258	26091685	26083771	26084392	2	26083258,26091591,	26084947,26091685,	NP_689710	uc001bky.1

my $info =<< 'INFO';
Description of the ucsc knownGene table and file format:
name 		uc007aet.1	varchar(255) 	values 		Name of gene
chrom 		chr1		varchar(255) 	values 		Reference sequence chromosome or scaffold
strand 		-		char(1) 	values 		+ or - for strand
txStart 	3195984		int(10) 	unsigned 	range 	Transcription start position
txEnd 		3205713		int(10) 	unsigned 	range 	Transcription end position
cdsStart 	3195984		int(10) 	unsigned 	range 	Coding region start
cdsEnd 		3195984		int(10) 	unsigned 	range 	Coding region end
exonCount 	2		int(10) 	unsigned 	range 	Number of exons
exonStarts 	3195984,3203519,		longblob 	Exon start positions
exonEnds 	3197398,3205713,		longblob 	Exon end positions
proteinID 	 		varchar(40) 	values 		UniProt display ID for Known Genes, UniProt accession or RefSeq protein ID for UCSC Genes
alignID 	uc007aet.1	varchar(255) 	values 		Unique identifier for each (known gene, alignment position) pair


we need to have a way to trawl chromosomes quickly, to find overlap of a read with gene regions of interest. hash key is good for getting chromosome data,
then an array pair with start and end of feature locations and an efficient search method would be best. Or is there a better indexing method to speed this up?

INFO




open(GENEIN,"<$geneSourceFile") or die "couldn't open known gene input file $geneSourceFile\n";
open(FOUT,">$outFile")    or die "couldn't open main output file $outFile\n";
open(HITSIN,"<$hitFile")  or die "couldn't open hit input file $hitFile\n";
open(SEQSIN,"<$seqsFile")  or die "couldn't open hit input file $seqsFile\n";
open(BEDOUT,">$bedFile ") or die "couldn't open bed format output file $bedFile \n";
print BEDOUT 'track name="mm9Cleave" description="pred mm9 cleave" visibility=2 url=mm9Cleave color="190,80,80"'."\n";
#print BEDOUT "$chr\t$locn\t$locInc\t$howMany$readPat\t960\t$strand\n" ;







my $AAcodeHash = getAACode() ;

print " amino acid for ATG is $AAcodeHash->{'ATG'} \n";

my $geneSourceFileDump = $geneSourceFile.'.Dump';

our %chrMapBeg;
our %chrMap ;
our %Kgenes ;
my @KgNums ;
our %geneMap ;
   
if (-e $geneSourceFileDump)  {      ################### read in data dumper ordered gene information  #########################
	print "found gene dump file $geneSourceFileDump so loading hashes from that\n";
	my $data = do {
	    if ( open my $fh, '<', $geneSourceFileDump ) { local $/; <$fh> }
	    else   { undef }
	};
	eval $data;
	my $totLeng = 0;
	
}
else{   # couldn't find $geneSourceFileDump file so need to prepare it manually
	
while(0) {    #  this is for dumping out to a file

	open( DUMP_FILE, '>' . $geneSourceFileDump)
	or die "couldn't open $geneSourceFileDump for write";
	print DUMP_FILE Data::Dumper->Dump( [ \%chrMapBeg, \%chrMap, \%Kgenes, \%geneMap ], [qw( *chrMapBeg *chrMap *Kgenes  *geneMap)]);       # [ qw( chrMapBeg chrMap Kgenes) ]);
	close(DUMP_FILE);
}
      
print "\nstarting to load gene data directly from known gene file\n";
#my %features;

my $geneCounter = -1;
my $header = <GENEIN>; print "header is : \n$header\n";
while(<GENEIN>) {
	next if /^\s+/;
	my @fields = split(/\t/,$_); chomp @fields;
	#print "res is $fields[0]  $fields[1] $fields[2] $fields[3] :$fields[4] :$fields[7] :$fields[8] :$fields[9] :$fields[10]\n";
	my $kgID = $fields[0];
	my $chr = $fields[1];
	my $strand = $fields[2];
	my $geneBeg = $fields[3];
	my $geneEnd = $fields[4];
	my $cdsBeg = $fields[5];
	my $cdsEnd = $fields[6];	
	my $numExons = $fields[7];
	my @exonsBeg = split (/,/,$fields[8]);
	my @exonsEnd = split (/,/,$fields[9]);
	my $protId = $fields[10];
	my $aliId = $fields[11];
	
	if (!defined($chrMapBeg{$chr}->{$geneBeg})) {$chrMapBeg{$chr}->{$geneBeg} = $geneEnd} # at gene begin in chr array, push the geneCount identifier and gene end pair
	else {$geneBeg++;$chrMapBeg{$chr}->{$geneBeg} = $geneEnd}
	
	
	if (!exists($Kgenes{$kgID})) {
		$geneCounter++;
		#$KgNums[$geneCounter] = $_;
		$KgNums[$geneCounter]{kgID} = $kgID;
		$KgNums[$geneCounter]{chr} = $chr;
		$KgNums[$geneCounter]{strand} = $strand;
		if ($strand eq '+') {$KgNums[$geneCounter]{sense} = 1}  # will not exist if anti-sense strand
		$KgNums[$geneCounter]{geneBeg} = $geneBeg;
		$KgNums[$geneCounter]{geneEnd} = $geneEnd;
		$KgNums[$geneCounter]{cdsBeg} = $cdsBeg;
		$KgNums[$geneCounter]{cdsEnd} = $cdsEnd;
		if ($cdsBeg eq $cdsEnd) {$KgNums[$geneCounter]{nonCoding} = 1}  # will not exist if anti-sense strand
		$KgNums[$geneCounter]{numExons} = $numExons;
		$KgNums[$geneCounter]{exonsBeg} = \@exonsBeg;
		$KgNums[$geneCounter]{exonsEnd} = \@exonsEnd;
		$KgNums[$geneCounter]{protId} = $protId;
		$KgNums[$geneCounter]{aliId} = $aliId;
		$KgNums[$geneCounter]{geneNames} = '';
		$Kgenes{$kgID} = $geneCounter;
		$chrMap{$chr}->{$geneBeg} = $geneCounter;
	}
	#if (exists($chrMap{$chr} )){}
	#else {
	#for (my $i = $geneBeg; $i < $geneEnd; $i++){

	#}

}


foreach my $chr (sort {$a cmp $b} keys %chrMap) {
	#print "\nchromosome is $chr:  " ;
	foreach (sort {$a <=> $b} keys %{$chrMapBeg{$chr}}) {
		push @{$geneMap{$chr}}, $_   ;
		#print "  $_";
	}
}
while(0){
	print "writing to known gene hashes to dump file\n";
	open( DUMP_FILE, '>' . $geneSourceFileDump)
	or die "couldn't open $geneSourceFileDump for write";
	print DUMP_FILE Data::Dumper->Dump( [ \%chrMapBeg, \%chrMap, \%Kgenes, \%geneMap, \@KgNums ], [qw( *chrMapBeg *chrMap *Kgenes *geneMap *KgNums)]);       # [ qw( chrMapBeg chrMap Kgenes) ]);
	close(DUMP_FILE);
}
}
# test hit

#chrX   and  17728189
my $chr = 'chr4' ;
my $locn = 1888489 ; # = 72262650;
#while(0) {
print "\nchromosome is $chr:  " ;
for (my $i = 0; $i < $#{$geneMap{$chr}}; $i++){
	
	next if ((${$geneMap{$chr}}[$i]) > $locn) ;   # gene Begin 
	next if (($chrMapBeg{$chr}->{${$geneMap{$chr}}[$i]}) < $locn) ;  # gene End
	my $exonStarts = join(' ', @{$KgNums[$chrMap{$chr}->{${$geneMap{$chr}}[$i]}]{exonsEnd} });
	print "target :$locn  begin  ${$geneMap{$chr}}[$i]  and end $chrMapBeg{$chr}->{${$geneMap{$chr}}[$i]} and exons starts $exonStarts\n";
	
#}
}

#  exit(0);
print "\nstarting to load geneome nucleotide sequences\n";
my $increment = 100000;
my $counter = 0;

my %sequences;

my $genomeSequenceDump = $seqsFile.'.Dump';

   
if (-e $genomeSequenceDump)  {      ################### read in data dumper ordered gene information  #########################
	print "found gene dump file $genomeSequenceDump so loading hashes from that\n";
	my $data = do {
	    if ( open my $fh, '<', $genomeSequenceDump ) { local $/; <$fh> }
	    else   { undef }
	};
	eval $data;
	my $totLeng = 0;
	
}
else{   # couldn't find $genomeSequenceDump file so need to prepare it manually
	
while(0) {    #  this is for dumping out to a file

	open( DUMP_FILE, '>' . $genomeSequenceDump)
	or die "couldn't open $genomeSequenceDump for write";
	print DUMP_FILE Data::Dumper->Dump( [ \%sequences ], [qw( *sequences )]);       # [ qw( chrMapBeg chrMap Kgenes) ]);
	close(DUMP_FILE);
}
  
my $inc = 1;
my $incProd = 10000;
my $keepChr ='';
my @accumulateSeq ;
while(<SEQSIN>){
	next if /\^s+/ ;
	chomp;
	if ($_ =~ />/){
		if ($keepChr ne '') {$sequences{$keepChr} = \@accumulateSeq}
		@accumulateSeq = ();
		push(@accumulateSeq,'');  # zero correct sequence array by offsetting array entries by 1
		$_ =~ s/>//;
		$keepChr = $_;
		print "generating sequence hash for chromosome $keepChr\n";
	}
	else{
		#print "generating sequence $_\n";
		push(@accumulateSeq,split('',$_));
		
	}
	$counter++;
	if ($counter > $incProd) {
		$inc++;
		$incProd = $inc * $increment ;
		#if ($_ =~ /[atcgATCG]/){print "non-N line $counter   "}
		print "$counter chr:$keepChr  ";
	}
	#my $kount = $counter/$increment;   #  my $increment = 100000;
	#if (int($kount) * $increment eq  $counter ) {print "$counter $keepChr  ";}
	
}
if ($keepChr ne '') {$sequences{$keepChr} = \@accumulateSeq}
print "\n\nfinished generating sequence hash for chromosome $keepChr\n";



#	open( DUMP_FILE, '>' . $genomeSequenceDump)
#	or die "couldn't open $genomeSequenceDump for write";
#	print "started generating sequence hash Dump file for chromosome $keepChr\n";
#	print DUMP_FILE Data::Dumper->Dump( [ \%sequences ], [qw( *sequences )]);       # [ qw( chrMapBeg chrMap Kgenes) ]);
#	close(DUMP_FILE);
#       print "finished data dumping sequence hash for chromosomes\n";

} # # couldn't find $genomeSequenceDump file so need to prepare it manually


# exit (0);
my %readHits;  # key is read sequence and val is number of hits for each sequence plus the three
#   other fields strand chr start location (don't bother with  mismatch count or quality line)
#  need to ensure that the hit for a read sequence is the same location or flag an error

#########################################
# Following was formatted for a single line per read (originally fastq data) 
#########################################

#while(<HITSIN>) 

while(0)  # this code segment was switched off
{
	next if /^\s+/;
	my @fields = split(/\t/,$_); chomp @fields;
	#print "res is $fields[0]  $fields[1] $fields[2] $fields[3] :$fields[4] :$fields[7] :$fields[8] :$fields[9] :$fields[10]\n";
	my $readID = $fields[0];
	my $chr = $fields[2];
	my $strand = $fields[1];
	my $hitBeg = $fields[3];
	my $readSeq = $fields[4];
	
	if (exists($readHits{$readSeq}) and ($readHits{$readSeq}->{chr} ne $chr or $readHits{$readSeq}->{hitBeg} ne $hitBeg)) {print "\nALERT : same read sequence maps to different unique location\n"}
	elsif (exists($readHits{$readSeq})){$readHits{$readSeq}->{howMany}++}
	else {
		$readHits{$readSeq}->{howMany}=1;
		$readHits{$readSeq}->{chr}=$chr;
		$readHits{$readSeq}->{strand}=$strand;
		$readHits{$readSeq}->{hitBeg}=$hitBeg;
		
	}
}

my $hitsSeqToAGene = 0;
my $intergenicSeqHits = 0;
my $CodingSeqExonHit = 0;
my $UTR3pSeqHit = 0;
my $UTR5pSeqHit = 0;

my %gotThisHit ;


# this will not happen if %readHits is empty
print "\nForeach readPat started \n";


while(0)  # this code segment was switched off
{

foreach my $readPat(reverse sort {$readHits{$a}->{howMany} <=> $readHits{$b}->{howMany}} keys %readHits) {
	# test hit
	my $dumpString = $readPat ;
	#chrX   and  17728189
	my $chr =  $readHits{$readPat}->{chr} ;#'chrX' ;
	my $locn = $readHits{$readPat}->{hitBeg} ;#72262650;
	my $locInc = $locn + length($readPat);
	my $strand = $readHits{$readPat}->{strand} ;
	my $howMany = $readHits{$readPat}->{howMany} ;
	#print BEDOUT "$chr\t$locn\t$locInc\t$howMany$readPat\t960\t$strand\n" ;
	#print "\nchromosome is $chr:  " ;
	for (my $i = 0; $i < $#{$geneMap{$chr}}; $i++){
		
		next if ((${$geneMap{$chr}}[$i]) > $locn) ;   # gene Begin 
		next if (($chrMapBeg{$chr}->{${$geneMap{$chr}}[$i]}) < $locn) ;  # gene End  # two hash lookups in this loop must be slow  - may need to reformat this if slow
		my $exonStarts = join(' ', @{$KgNums[$chrMap{$chr}->{${$geneMap{$chr}}[$i]}]{exonsEnd} });
		#print "target :$locn  begin  ${$geneMap{$chr}}[$i]  and end $chrMapBeg{$chr}->{${$geneMap{$chr}}[$i]} and exons starts $exonStarts\n";
		my $utr3pHit= hit3pUTR($locn, $strand, $KgNums[$chrMap{$chr}->{${$geneMap{$chr}}[$i]}] ) ;
		if ($utr3pHit) {
			$UTR3pSeqHit++ ;
			
			if ($dumpString) {print FOUT "\nread pattern $dumpString 3pUTR hit with $howMany reads: " ; $dumpString = '';}
			#print "   $utr3pHit";
			#my $locMinus = $locn - 100; if ($locMinus < 0 ) {$locMinus = 0}
			#my $locPlus = $locInc + 100; #if ($locPlus < 0 ) {$locMinus = 0}
			if (!exists($gotThisHit{"$chr$strand$locn"})) {
				$gotThisHit{"$chr$strand$locn"}++;
				print BEDOUT "$chr\t$locn\t$locInc\t${readPat}_$howMany\t960\t$strand\n" ;
			}
		}
		my $utr5pHit = hit5pUTR($locn, $strand, $KgNums[$chrMap{$chr}->{${$geneMap{$chr}}[$i]}] ) ;
		if ($utr5pHit) {  $UTR5pSeqHit++  }
		my $hitCodingExon= hitCodingExon($locn, $strand, $KgNums[$chrMap{$chr}->{${$geneMap{$chr}}[$i]}] ) ;
		if ($hitCodingExon) {
			$CodingSeqExonHit++ ;
			hitCodon($locn, $strand, $KgNums[$chrMap{$chr}->{${$geneMap{$chr}}[$i]}], $sequences{$chr});
		}
		
	}

}

}

#Pool    	Position|ReferenceAllele|Reads|SameSNP|SNPAllele|Proportion|StDev|NumStrains|Int|LowerCI|UpperCI|SNPAlleleQ|ProportionQ StDevQ NumStrainsQ IntQ LowerCIQ UpperCIQ
#crcchr15        18397053        T       10      1       A       0.100      0.095   1.000    1   -0.862  2.862   A       	0.111   0.101   1.110   1       -0.870  3.090   
#crcchr15        18748421        A       10      1       G       0.900      0.095   9.000    9    7.138  10.862  G       	0.885   0.104   8.850   9       6.812   10.888  
#crcchr15        18748495        G       12      1       A       1.000      0.000   10.000  10   10.000  10.000  A       	1.000   0.000   10.000  10      10.000  10.000  
#crcchr15        18748515        T       19      1       C       0.053      0.051   0.530    1   -0.470  1.530   C       	0.057   0.054   0.570   1       -0.488  1.628   
#crcchr15        18748521        A       19      1       G       0.053      0.051   0.530    1   -0.470  1.530   G       	0.042   0.046   0.420   0       -0.482  1.322   

$hitsSeqToAGene = 0;
$intergenicSeqHits = 0;
$CodingSeqExonHit = 0;
$UTR3pSeqHit = 0;
$UTR5pSeqHit = 0;


	
my @codChange1 = (1,-1,-2);
my @codChange2 = (2,1,-1);
$increment = 1000;
my $SNPcounter = 0 ;
my @currentChrSeq;
my $currentChr = 0;
$counter = 0;
my $remark='';
while(<HITSIN>) 
{
	
	next if /^\s+/;
	next if /^Pool/;
	$SNPcounter++;
	my @fields = split(/\s+/,$_); chomp @fields;
	#print "res is $fields[0]  $fields[1] $fields[2] $fields[3] :$fields[4] :$fields[7] :$fields[8] :$fields[9] :$fields[10]\n";
	my $readID = $SNPcounter ;
	my ($chr, $hitBeg, $refAllele, $numReads, $skip, $SNPallele, $prop, @dump ) = @fields;
	$chr =~ s/crc// ;
	
	#print "$chr:$hitBeg:$refAllele:$numReads:$SNPallele:$prop - was chr hitBeg refAllele numReads SNPallele prop" ;
	
	my $strand = '+';
	my $dumpString = "$chr $hitBeg $refAllele $numReads $SNPallele $prop" ;
	#exit(0);
	if (!defined($currentChr) || ($currentChr ne $chr)) {
		#@currentChrSeq = @{$sequences{$chr}};
		$currentChr = $chr;
	}
	$locn = $hitBeg;
	#exit (0);
	#my $adjLocn = $locn;     #$locn-1 ;  # offset correction for zero start of sequence array
	if ($refAllele ne uc($sequences{$chr}->[$locn]) ){print "mismatch to UCSC $chr allele seq at $hitBeg,  $refAllele ne $sequences{$chr}->[$locn] and SNP is $SNPallele in proportion $prop of $numReads reads\n"; next}
	#else {print "MATCH to UCSC $chr allele seq at $hitBeg,  $refAllele ne $currentChrSeq[$hitBeg]\n";}
	#print "upper limit  $#{$geneMap{$chr}} \n";
	for (my $i = 0; $i < $#{$geneMap{$chr}}; $i++){   # step through genes on chromosome to find one overlapping the SNP location
		#print "before the next entires start ${$geneMap{$chr}}[$i]  end $chrMapBeg{$chr}->{${$geneMap{$chr}}[$i]} hit beg is $hitBeg\n";
		
		next if ((${$geneMap{$chr}}[$i]) > $hitBeg) ;   # if SNP locn before gene Begin 
		next if (($chrMapBeg{$chr}->{${$geneMap{$chr}}[$i]}) < $hitBeg) ;  # if SNP locn after gene End  # two hash lookups in this loop must be slow  - may need to reformat this if slow
				
		#my $exonStarts = join(' ', @{$KgNums[$chrMap{$chr}->{${$geneMap{$chr}}[$i]}]{exonsEnd} });
		#print "target :$locn  begin  ${$geneMap{$chr}}[$i]  and end $chrMapBeg{$chr}->{${$geneMap{$chr}}[$i]} and exons starts $exonStarts\n";
		#my $utr3pHit=  ;
		#print "past the next entires so actually testing locations\n";
		#if(!defined($KgNums[$chrMap{$chr}->{${$geneMap{$chr}}[$i]}]->{cdsBeg})){print "KgNums {cdBeg} not defined for geneMap chr $chr  locn  $geneMap{$chr}[$i] \n"}
		if (hit3pUTR($locn, $strand, $KgNums[$chrMap{$chr}->{${$geneMap{$chr}}[$i]}] )) {
			#$UTR3pSeqHit++ ;
			#print "SNP 3pUTRhit " ;
			#if ($dumpString) {print FOUT "\nSNP $dumpString 3pUTR hit  " ; $dumpString = '';}
			#print "   $utr3pHit";
			#my $locMinus = $locn - 100; if ($locMinus < 0 ) {$locMinus = 0}
			#my $locPlus = $locInc + 100; #if ($locPlus < 0 ) {$locMinus = 0}
			#if (!exists($gotThisHit{"$chr$strand$locn"})) {	$gotThisHit{"$chr$strand$locn"}++;	print BEDOUT "$chr\t$locn\t$locInc\t${readPat}_$howMany\t960\t$strand\n" ; }
		}
		#my $utr5pHit = ;
		if (hit5pUTR($locn, $strand, $KgNums[$chrMap{$chr}->{${$geneMap{$chr}}[$i]}] ) ) {  $UTR5pSeqHit++ ; print "SNP 5pUTRhit " ; }
		#my $hitCodingExon= ;
		if (hitCodingExon($locn, $strand, $KgNums[$chrMap{$chr}->{${$geneMap{$chr}}[$i]}] ) ) {
			$CodingSeqExonHit++ ;
			#print "SNP coding hit " ;
			if (my $hitCodon = hitCodon($locn, $strand, $KgNums[$chrMap{$chr}->{${$geneMap{$chr}}[$i]}], $sequences{$chr})){

				#$dumpString = '';
				chomp $hitCodon;
				my ($nt, $codPos) = split(/\s+/,$hitCodon);
				my $locn1 = $locn+$codChange1[$codPos] ;  # these provide an array of offsets for the other two nts of the codon
				my $locn2 = $locn+$codChange2[$codPos] ;
				my $hitCodon1 = hitCodon($locn1, $strand, $KgNums[$chrMap{$chr}->{${$geneMap{$chr}}[$i]}], $sequences{$chr});
				my $hitCodon2 = hitCodon($locn2, $strand, $KgNums[$chrMap{$chr}->{${$geneMap{$chr}}[$i]}], $sequences{$chr});
				chomp $hitCodon1;
				chomp $hitCodon2;
				my ($nt1, $codPos1) = split(/\s+/,$hitCodon1);
				my ($nt2, $codPos2) = split(/\s+/,$hitCodon2);
				
				
				my ($refCODON, $SNPcodon);
				if ($codPos == 0){$refCODON = uc "$nt$nt1$nt2"; $SNPcodon = uc "$SNPallele$nt1$nt2";}
				if ($codPos == 1){$refCODON = uc "$nt1$nt$nt2"; $SNPcodon = uc "$nt1$SNPallele$nt2";}
				if ($codPos == 2){$refCODON = uc "$nt1$nt2$nt"; $SNPcodon = uc "$nt1$nt2$SNPallele";}
				if ($strand eq '-'){$refCODON = reverse($refCODON); $SNPcodon = reverse($SNPcodon);}
				my $SNPaa = $AAcodeHash->{$SNPcodon} ;
				my $refAA = $AAcodeHash->{$refCODON} ;
				$remark = "$refAA -> $SNPaa , $refCODON -> $SNPcodon ";
				#print "SNP CODON hit $hitCodon at $locn strand $strand nt1 nt2 are $nt1 $nt2 codPos1 and 2 are $codPos1 $codPos2\nSNPaa is $SNPaa with codon $SNPcodon and refAA is $refAA with codon $refCODON\n" ;
				if ($SNPaa eq '*' &&  $refAA ne '*' ){$remark.=' ALERT: NEW STOP CODON ';}
				if ($SNPaa ne '*' &&  $refAA eq '*' ){$remark.=' ALERT: STOP CODON REMOVED';}
				if ($SNPcodon ne 'ATG' && $refCODON eq 'ATG' ){$remark.=' ALERT: POSSIBLE START CODON CHANGE';}
				if ($refAA ne $SNPaa) {$remark.= " NON-SYNONYMOUS"}
				#if ($dumpString) {print FOUT "\nSNP $dumpString 3pUTR hit  " ; $dumpString = '';}
				print FOUT "\n$dumpString SNP $remark " ;
				print "\n$dumpString SNP $remark" ;
				last;

			}
			
			
		}
		elsif (my $spliceHit = hitSplice($locn, $strand, $KgNums[$chrMap{$chr}->{${$geneMap{$chr}}[$i]}] ) ) { print FOUT "\n$dumpString SPLICE HIT  " ;  print "SNP splice hit " ;last;}
		


		 
		
		#my $point = fetch_free();
		#my $mem = $point->{free} ;
		#print "mem use is $mem\n";
		#if ($mem < 7 ){exit (0)}
		#howMuchFreeMem() ;

		
		
	}
	my $kount = $SNPcounter/$increment;   #  my $increment = 100000;
	if (int($kount) * $increment eq  $SNPcounter ) {print "$SNPcounter  \n"; }

}




sub hit3pUTR {
	my $hitLoc = shift;
	my $strand = shift;
	my $geneParms = shift;
	my $cdsEnd = $geneParms->{cdsEnd} ;
	my $cdsBeg = $geneParms->{cdsBeg} ;
	#print "cdsBeg $cdsBeg  cdsEnd $cdsEnd \n";
	if ($cdsBeg eq $cdsEnd) {return 0}  # cds beginning is usually set equal to cds end for non-coding genes
	if ($geneParms->{strand} eq '+' and  $hitLoc > $cdsEnd and $hitLoc < $geneParms->{geneEnd}) { return $geneParms->{kgID}}
	elsif ($geneParms->{strand} eq '-' and  $hitLoc < $cdsBeg and $hitLoc > $geneParms->{geneBeg}) { return $geneParms->{kgID}}
	else { return 0;}
}

sub hit5pUTR {
	my $hitLoc = shift;
	my $strand = shift;
	my $geneParms = shift;
	my $cdsEnd = $geneParms->{cdsEnd} ;
	my $cdsBeg = $geneParms->{cdsBeg} ;
	if ($cdsBeg eq $cdsEnd) {return 0}  # cds beginning is usually set equal to cds end for non-coding genes
	if ($geneParms->{strand} eq '+' and  $hitLoc < $cdsBeg and $hitLoc > $geneParms->{geneBeg}) { return $geneParms->{kgID}  }
	elsif ($geneParms->{strand} eq '-' and  $hitLoc > $cdsEnd and $hitLoc < $geneParms->{geneEnd}) { return $geneParms->{kgID}}
	else { return 0;}
}

sub hitSplice {
	my $hitLoc = shift;
	my $strand = shift;
	my $geneParms = shift;
	#$geneParms->{exonsBeg}->[0] ;
	my @exonBegs =  @{$geneParms->{exonsBeg}};
	my @exonEnds =  @{$geneParms->{exonsEnd}};
	#print "HERE THEY ARE : exon starts $exonStarts\n" ;
	my $i = 0;
	my $numExons = $geneParms->{numExons} ;
	my $spliceFuzz = 4 ;

	while ($i < $numExons){
		if (($hitLoc > $exonBegs[$i] - $spliceFuzz) and ($hitLoc < $exonBegs[$i] + $spliceFuzz)  and ($hitLoc < $exonEnds[$i])) { # hit close to an exon start
			return $geneParms->{kgID} ;
		}
		elsif (($hitLoc > $exonBegs[$i] ) and ($hitLoc > $exonEnds[$i] - $spliceFuzz)  and ($hitLoc < $exonEnds[$i] + $spliceFuzz )) { # hit close to an exon end
			return $geneParms->{kgID} ;
		}
		$i++;
	}
		
	

	if ($i eq $numExons) {
		return 0;   # not close to splice hit registered (may not be expected if original mapping did not include match to RNA)
	}	
}

sub hitCodingExon {
	my $hitLoc = shift;
	my $strand = shift;
	my $geneParms = shift;
	my $codeBeg = $geneParms->{cdsBeg};
	my $codeEnd = $geneParms->{cdsEnd} ;	
	my @exonBegs =  @{$geneParms->{exonsBeg}};
	my @exonEnds =  @{$geneParms->{exonsEnd}};
	my $i = 0;
	my $numExons = $geneParms->{numExons} ;
	#print "in coding exon check  hitloc is $hitLoc\n";

	#if ($geneParms->{strand} eq '+' and  $hitLoc < $geneParms->{cdsBeg} and $hitLoc > $geneParms->{geneBeg}) { return $geneParms->{kgID}  }
	#elsif ($geneParms->{strand} eq '-' and  $hitLoc > $geneParms->{cdsEnd} and $hitLoc < $geneParms->{geneEnd}) { return $geneParms->{kgID}}
	
	while ($i < $numExons){
		if (($hitLoc >= $exonBegs[$i] ) and ($hitLoc <= $exonEnds[$i]) and ($hitLoc >= $codeBeg ) and ($hitLoc <= $codeEnd )) { # hit in codon exon
			return $geneParms->{kgID} ;
		}
		$i++;
	}
	if ($i eq $numExons) {
		return 0;   # no 'close to splice hit' event registered (may not be expected if original mapping did not include match to RNA)
	}
	
}


sub hitCodon {
    my $hitLoc = shift;
    my $strand = shift;
    my $geneParms = shift;
    # my $chr = shift ;  this is not needed if the sequence array is given only for the current chromosome
    # - ie split the incoming sequence file into multiple arrays, by chromosome
    my $seqRoot  = shift ;
    #print "in hit codon\n";
    #my @seq  = @{$seqRoot} ;
   
    my $codeBeg = $geneParms->{cdsBeg};
    my $codeEnd = $geneParms->{cdsEnd} ;  
    my @exonBegs =  @{$geneParms->{exonsBeg}};
    my @exonEnds =  @{$geneParms->{exonsEnd}};
    my %codonData ;
    my $i = 0;
    my $numExons = $geneParms->{numExons} ;
   
    #print "third nucleotide is $seqRoot->[3]\n";
    if (exists($geneParms->{codons})){
		
		if ($geneParms->{codons}->{$hitLoc}) {
			#print "codon found is $geneParms->{codons}->{$hitLoc}\n";
			return $geneParms->{codons}->{$hitLoc};
		}
		else {return 0;}		      
	}
    else {
	my  $i = 1;
	my $exonCounter = 0;
	my ($n1, $n2, $n3);  # three nucleotides
	until ($codeBeg <= $exonEnds[$exonCounter]){$exonCounter++; }  #print "exonCounter is  $exonCounter     ";   }   # move to exon containing the coding start
	#print "\npast exon counting to find the start\n";
	my $ntStepper = $codeBeg - 2;
	my $ntStepEnd ;
	my $exEnd ;
	while (1){
    
	   
	    $ntStepEnd = $ntStepper+2;
	    
	    $exEnd = $exonEnds[$exonCounter];
	    if ($exEnd >= $codeEnd) {$exEnd = $codeEnd}
	    if (!defined($exonEnds[$exonCounter])) {}
	    elsif (($ntStepEnd) < $exEnd ){
		    $n1 = $ntStepper; $ntStepper++;
		    $n2 = $ntStepper; $ntStepper++;
		    $n3 = $ntStepper; $ntStepper++;
		   
	   }
	   elsif (($ntStepper + 2 ) == $exEnd){
		    $n1 = $ntStepper; $ntStepper++;
		    $n2 = $ntStepper; $ntStepper++;
		    $n3 = $ntStepper;
			
		    $exonCounter++;
		    $ntStepper = $exonBegs[$exonCounter];
			$ntStepper++;
	   }
	   elsif (($ntStepper + 1 ) == $exEnd){
		    $n1 = $ntStepper; $ntStepper++;
		    $n2 = $ntStepper;
		   
		    $exonCounter++;
		    $ntStepper = $exonBegs[$exonCounter];
			$ntStepper++;
		    $n3 = $ntStepper; $ntStepper++;   
	   }
	   elsif ($ntStepper == $exEnd){
		    $n1 = $ntStepper;
	    
		    $exonCounter++;
		    $ntStepper = $exonBegs[$exonCounter];
			$ntStepper++;
		    $n2 = $ntStepper; $ntStepper++;
		    $n3 = $ntStepper; $ntStepper++;
	   
	   }
			if (!exists($seqRoot->[$n3])) {print "\n\n n3 is $n3 and is not found. codeEnd is $codeEnd, exEnd is $exEnd, exon end is $exonEnds[$exonCounter] \n"}
		    $codonData{$n1} =  "$seqRoot->[$n1] 0" ;
		    $codonData{$n2} =  "$seqRoot->[$n2] 1" ;  # check does this increment before lookup? and does that happen before RHS or after - what order
		    $codonData{$n3} =  "$seqRoot->[$n3] 2" ;
		    my $codonThngy = uc "$seqRoot->[$n1]$seqRoot->[$n2]$seqRoot->[$n3]" ;
		    #print  "codon $codonThngy AA $AAcodeHash->{$codonThngy} starts as $n1      ntStepper is $ntStepper and codeEnd is $codeEnd\n";
		    #print "ntStepper $ntStepper codeEnd $codeEnd  codonData{n1} $codonData{$n1} $n1 codonData{n2} $codonData{$n2} $n2  codonData{n3} $codonData{$n3} $n3\n";
		    
	   
	   # need to calculate the amino acid and that depends on the strand.  Also the AA count depends on the strand
		if ($ntStepper >= $codeEnd) { last;}
		if (!defined($exonEnds[$exonCounter])) {last}
	}
	$geneParms->{codons} = \%codonData;
	if ($codonData{$hitLoc}) {print "codon is  $codonData{$hitLoc}\n";return $codonData{$hitLoc}}
	else {return 0;}
   }
    
    #if ($geneParms->{strand} eq '+' and  $hitLoc < $geneParms->{cdsBeg} and $hitLoc > $geneParms->{geneBeg}) { return $geneParms->{kgID}  }
    #elsif ($geneParms->{strand} eq '-' and  $hitLoc > $geneParms->{cdsEnd} and $hitLoc < $geneParms->{geneEnd}) { return $geneParms->{kgID}}
     	
}

sub hitCodingSynonymState {    #  if not present then work out codon map and write it into record when calculated for a gene for the first time
	my $hitLoc = shift;
	my $strand = shift;
	my $geneParms = shift;
	my @exonBegs =  @{$geneParms->{exonsBeg}};
	my @exonEnds =  @{$geneParms->{exonsEnd}};
	my $i = 0;
	my $numExons = $geneParms->{numExons} ;
	my $spliceFuzz = 15 ;
	if ($geneParms->{strand} eq '+' ) {
		#foreach my $exBegLocn ($exonBegs) {}
		while ($i < $numExons){
			if (($hitLoc > $exonBegs[$i] - $spliceFuzz) and ($hitLoc < $exonBegs[$i] + $spliceFuzz)  and ($hitLoc < $exonEnds[$i])) { # hit close to an exon start
				return $geneParms->{kgID} ;
			}
			elsif (($hitLoc > $exonBegs[$i] ) and ($hitLoc > $exonEnds[$i] - $spliceFuzz)  and ($hitLoc < $exonEnds[$i] + $spliceFuzz )) { # hit close to an exon end
				return $geneParms->{kgID} ;
			}
			$i++;
		}
		
	}
	elsif ($geneParms->{strand} eq '-' ){
		while ($i < $numExons){
			if (($hitLoc > $exonBegs[$i] - $spliceFuzz) and ($hitLoc < $exonBegs[$i] + $spliceFuzz)  and ($hitLoc < $exonEnds[$i])) { # hit close to an exon start
				return $geneParms->{kgID} ;
			}
			elsif (($hitLoc > $exonBegs[$i] ) and ($hitLoc > $exonEnds[$i] - $spliceFuzz)  and ($hitLoc < $exonEnds[$i] + $spliceFuzz )) { # hit close to an exon end
				return $geneParms->{kgID} ;
			}
			$i++;
		}		
	}
	else { return 0;}
	#$geneParms->{exonsBeg}->[0] ;
	#my $exonStarts = join(' ', @{$geneParms->{exonsBeg}});
	#print "HERER THEY ARE : exon starts $exonStarts\n" ;
	
}

	
my $guff =<< 'GUFF';
	my $seqobj = $seqio->next_seq;
6.      my $src    = $seqobj->id;
7.      my $length = $seqobj->length;
8.      my @cds    = grep { $_->primary_tag eq $ptag $seqobj->get_SeqFeatures;
9.      for my $feat(@cds) {
10.         for my $gene($feat->get_tag_values('gene')) {
11.             @{$features{$src}{$ptag}{$gene}{'product'}}     = $feat->get_tag_values('product');
12.             @{$features{$src}{$ptag}{$gene}{'protein_id'}}  = $feat->get_tag_values('protein_id');
13.             @{$features{$src}{$ptag}{$gene}{'translation'}} = $feat->get_tag_values('translation');
14.               $features{$src}{$ptag}{$gene}{'strand'}       = $feat->strand;
15.               $features{$src}{$ptag}{$gene}{'start'}        = $feat->start;
16.               $features{$src}{$ptag}{$gene}{'end'}          = $feat->end;
17. ## location
18. 	if($feat->location->isa('Bio::Location::SplitLocationI')) {
19. 	    my @locn = $feat->location->sub_Location;
20. 	    for(my $i = 0; $i <= $#locn; $i++) {
21. 		    @{$ret{$src}{$ptag}{$gene}{'exon'}[$i]} = ($locn[$i]->start,$locn[$i]->end);
22.                                            }
23.                                                               }
24. 	else {
25. 	        @{$ret{$src}{$ptag}{$gene}{'exon'}[0]} = ($feat->start,$feat->end);
26.          }
27.                                                     }
28. ## length of source feature
29.     $features{$src}{'length'} = $length;
30.                        }
31. return %features;

### check the position of SNP in protein, i.e. start / stop, change of
### amino acid
sub check{
  my $pos = shift;
 
  my $ref_ar = $refDNA[$pos];
  foreach (@$ref_ar) {
    my $posGene=$_;
    my $ref_Set=$$ref_Position{$_};
    my $genePID = $$ref_Set{pid};
   
    $resStr.= $$ref_Set{strand}."$SPACER";

    ### check the start stop, and write the result directly in the
    ### global var $resStr;
    checkStartStop($pos,$ref_Set,$posGene);

    if ($old eq '.') {
      $resStr.= "Ins$SPACER$SPACER-$SPACER-$SPACER";
      $Insert++;
      push @{ $hash_Diffgene{$genePID}{Ins}},$pos;
      push @{ $hash_Diffgene{$genePID}{qualIns} },$qualityMoreau;
    }
    elsif ($new eq '.') {
      $resStr.= "Del$SPACER$SPACER-$SPACER-$SPACER";
      $Deletion++;
      push @{ $hash_Diffgene{$genePID}{Del}},$pos;
      push @{ $hash_Diffgene{$genePID}{qualDel} },$qualityMoreau;
    }
    else {
      ### get codon
      checkCodon($pos,$ref_Set,$posGene);     
    }
    $resStr.= $$ref_Set{pid}." - ";
    # write the function / name of the protein
    $resStr.= substr ($$ref_Set{name},0,150);
    $resStr.= "\n";   
  }
  # TODO  if (scalar(@$ref_ar)==0) {
  #    print "empty..\n";
  #  }
}

### Check if the SNP effects a start or stop codon
sub checkStartStop{
  my $pos = shift;
  my $ref_Set = shift;
  my $posGene = shift;

  my $genePID = $$ref_Set{pid};
 
  ### check if start or stop
  if ($$ref_Set{start} <= $pos &&
      ($$ref_Set{start} + 2) >= $pos) {
    if ($$ref_Set{strand} eq '+') {
      $resStr.= "Start$SPACER";
      $Start++;
      $hash_Diffgene{$genePID}{Start}= 1;
      $hash_Diffgene{$genePID}{qualStart} =$qualityMoreau;
    }
    else {
      $resStr.= "Start$SPACER";
      $Start++;
      $hash_Diffgene{$genePID}{Start}= 1;
      $hash_Diffgene{$genePID}{qualStart} =$qualityMoreau;
    }
  }
  elsif (($$ref_Set{end}-2) <= $pos &&
         ($$ref_Set{end} >= $pos)) {
    if ($$ref_Set{strand} eq '+') {
      $resStr.= "Stop$SPACER";
      $Stop++;     
      $hash_Diffgene{$genePID}{Stop}= 1;
      $hash_Diffgene{$genePID}{qualStop} =$qualityMoreau;
    }
    else {
      $resStr.= "Stop$SPACER";
      $Stop++;     
      $hash_Diffgene{$genePID}{Stop}= 1;
      $hash_Diffgene{$genePID}{qualStop} =$qualityMoreau;
    }
  }
}  ### END check if start or stop


sub checkCodon{
  my $pos = shift;
  my $ref_Set = shift;
  my $posGene = shift;
  my $geneName = shift;
 
  my $str;
  if ($$ref_Set{strand} ne '+') {
    $str = revCom($$ref_Protein{$posGene});
  }
  else {
    $str = $$ref_Protein{$posGene};
  }
 
  if ($$ref_Set{strand}) {
    my ($startGene) = $posGene =~ /^(\d+)\.\./;
   
    my $GenePos = $pos  - $startGene;
   
    my $triplet;
    my $tripNew;
    if ($GenePos % 3 == 0) {
      $triplet = substr($str,($GenePos),3);
      $tripNew = $new.substr($str,($GenePos+1),2);
    }
    elsif ($GenePos % 3 == 1) {
      $triplet = substr($str,($GenePos-1),3);
      $tripNew =substr($str,($GenePos-1),1).$new.substr($str,($GenePos+1),1);
    }
    else {
      $triplet = substr($str,($GenePos-2),3);
      $tripNew =substr($str,($GenePos-2),2).$new;
    }
   
    my $tripBo = getTrip($triplet,$$ref_Set{strand});
    $resStr.= "$triplet$SPACER$tripNew$SPACER";
    checkStop($triplet,$tripNew,$ref_Set);
    my $tripMo = getTrip($tripNew,$$ref_Set{strand});
    $resStr.= "$tripBo$SPACER$tripMo$SPACER";
    if ($tripBo ne $tripMo) {
      $changeProtein++;
    }
    else {
      $nonChangeProtein++;
    }
  }
}

sub checkStop{
  my $old = shift;
  my $trip= shift;
  my $ref_Set = shift;
  my  $qualityMoreau = shift;
 
 
  if (uc($trip) eq  'TAA' or
      uc($trip) eq  'TAG' or
      uc($trip) eq  'TGA') # isStop($old) and
    {
      $hash_Diffgene{$$ref_Set{pid}}{NewStop}=1;
      $hash_Diffgene{$$ref_Set{pid}}{qualNewStop} =$qualityMoreau;    
    }
}

#will print triplet, and take care of strand...
sub getTrip{
  my $trip = shift;
  my $strand = shift;

  if ($strand ne '+') {
    $trip = revCom($trip);
    return $$geneCode{$trip};
  }
  else {
    return $$geneCode{$trip};
  }
}


Exchange Group	6	EvolutionSubstitution	{HRK}{DENQ}{C}{STPAG}{MILV}{FYW}
Charge/Polarity	4	Charge and Polarity	{HRK} {DE} {CTSGNQY} {PMLIVFW}
Hydrophobicity	3	Hydrophobicity	{DENQRK} {CSTPGHY} {AMILVFW}
Structural	3	Surface Exposure	{DENQHRK} {CSTPAGWY} {MILVF}
2D Propensity	3	Secondary Structure 	{AEQHKMLR} {CTIVFYW} {SGPDN}


GUFF


sub getAACode{
my %stdAminoAcidCode = (

    'TCA' => 'S',    # Serine
    'TCC' => 'S',    # Serine
    'TCG' => 'S',    # Serine
    'TCT' => 'S',    # Serine
    'TTC' => 'F',    # Phenylalanine
    'TTT' => 'F',    # Phenylalanine
    'TTA' => 'L',    # Leucine
    'TTG' => 'L',    # Leucine
    'TAC' => 'Y',    # Tyrosine
    'TAT' => 'Y',    # Tyrosine
    'TAA' => '*',    # Stop
    'TAG' => '*',    # Stop
    'TGC' => 'C',    # Cysteine
    'TGT' => 'C',    # Cysteine
    'TGA' => '*',    # Stop
    'TGG' => 'W',    # Tryptophan
    'CTA' => 'L',    # Leucine
    'CTC' => 'L',    # Leucine
    'CTG' => 'L',    # Leucine
    'CTT' => 'L',    # Leucine
    'CCA' => 'P',    # Proline
    'CCC' => 'P',    # Proline
    'CCG' => 'P',    # Proline
    'CCT' => 'P',    # Proline
    'CAC' => 'H',    # Histidine
    'CAT' => 'H',    # Histidine
    'CAA' => 'Q',    # Glutamine
    'CAG' => 'Q',    # Glutamine
    'CGA' => 'R',    # Arginine
    'CGC' => 'R',    # Arginine
    'CGG' => 'R',    # Arginine
    'CGT' => 'R',    # Arginine
    'ATA' => 'I',    # Isoleucine
    'ATC' => 'I',    # Isoleucine
    'ATT' => 'I',    # Isoleucine
    'ATG' => 'M',    # Methionine
    'ACA' => 'T',    # Threonine
    'ACC' => 'T',    # Threonine
    'ACG' => 'T',    # Threonine
    'ACT' => 'T',    # Threonine
    'AAC' => 'N',    # Asparagine
    'AAT' => 'N',    # Asparagine
    'AAA' => 'K',    # Lysine
    'AAG' => 'K',    # Lysine
    'AGC' => 'S',    # Serine
    'AGT' => 'S',    # Serine
    'AGA' => 'R',    # Arginine
    'AGG' => 'R',    # Arginine
    'GTA' => 'V',    # Valine
    'GTC' => 'V',    # Valine
    'GTG' => 'V',    # Valine
    'GTT' => 'V',    # Valine
    'GCA' => 'A',    # Alanine
    'GCC' => 'A',    # Alanine
    'GCG' => 'A',    # Alanine
    'GCT' => 'A',    # Alanine
    'GAC' => 'D',    # Aspartic Acid
    'GAT' => 'D',    # Aspartic Acid
    'GAA' => 'E',    # Glutamic Acid
    'GAG' => 'E',    # Glutamic Acid
    'GGA' => 'G',    # Glycine
    'GGC' => 'G',    # Glycine
    'GGG' => 'G',    # Glycine
    'GGT' => 'G',    # Glycine
    );
return \%stdAminoAcidCode;
}	




sub howMuchMemLeft{    ## needs /proc/user_beancounters to be populated - by some virtualisation product not on RH   -  maybe free -h  will produce something useful??
our @Wfields = ( "kmemsize", "privvmpages" ) ; # watch these feilds

our $THRESHOLD=(50/100); # fraction of memory used
our $SAVE_DIR='/home/jan'; # Where to save the output

	my $beans = fetch_ubc();   ##### this should monitor memory usage and exit when exceeds 50%
	for my $Wfield ( @Wfields ) {
		if ($beans->{$Wfield}{"held"}>($THRESHOLD*$beans->{$Wfield}{"limit"})){ panic_time(); };
	} 

}

sub howMuchFreeMem{
	
	my $point = fetch_free();
	my $mem = $point->{free} ;
	
	#if ($mem < 10) {print "mem use is $mem\n";}
	if ($mem < 5 ){exit (0)}
}

sub fetch_free {
        my @memReport = `free -g`;  # gives free memory in gigs,  -m in megs
#free -g
#             total       used       free     shared    buffers     cached
#Mem:            31          1         29          0          0          1
#-/+ buffers/cache:          0         30
#Swap:           33          0         33
my %_MEMES;
        my $freeGigs=32;
	shift(@memReport); # dump first line
	pop(@memReport); pop(@memReport); # dump last two lines
        foreach  (@memReport) {
	
                m/(Mem:)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)/;
                %_MEMES = ( 'total' => $2,
                                'used' => $3,
                                'free' => $4,
                                'shared' => $5,
                                'buffers' => $6,
				'cached' => $7
                             );
        }

        return \%_MEMES;
}

sub fetch_ubc {
        open( UBC, '<', '/proc/user_beancounters' );
        my %_BEANS;
        while (<UBC>) {
                m/([a-z]+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)/;
                $_BEANS{$1} = { 'held' => $2,
                                'maxheld' => $3,
                                'barrier' => $4,
                                'limit' => $5,
                                'failcnt' => $6
                             };
        }
        close UBC;
        return \%_BEANS;
}

sub panic_time {
        exit (0) ;
}



