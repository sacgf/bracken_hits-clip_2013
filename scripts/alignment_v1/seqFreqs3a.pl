#!/usr/bin/perl -w

use strict;

use String::Approx 'amatch';

my ($usage) = <<'USAGE';

USAGE:

seqFreq3.pl <seqFile> <optional truncate N chars off the end> <dump or nodump> <optional start sequence> <use n cores>

takes :
0. how many highest frequecy sequences to be printed
1. full file path for seq file
2. number of chars to remove from trailing end of file( default 0 )
3. dump all sequences with frequencies rather than a summary report (nodump)
4. only process seqs starting with this char string (ie use for particular barcode set)  NOT IMPLEMENTED YET
5. split input file n ways and spawn n parallel processes to minimal execution time  NOT IMPLEMENTED YET

provides a distribution of sequence frequencies, in order of most frequent



example  Aseq-splitsRepeatTrim-06-02-11]$ perl ../../../scripts/seqFreqs3.pl 4000 cntl.fastq 0

cd ~/myWork/CamsCLIPseq1st/CamsCLIPseq2ndsmRNA

perl ../clipSeq1smRNA/scripts/seqFreqs3a.pl 400000 cntl.fastq 0 > allCLIP1cntlseqFreqsFuzzyMiRNAmapped

USAGE

#
#

#print help if no argument given
unless ( defined $ARGV[2] ) {
    print $usage;
    exit;
}

#TCCCTGAGACCCTAACTTGTGACAGACGAC
#123456789012345678901234567890
#ACTTGTTAGGCTCCTCTTTTTCAGACGACG


#@HWI-EAS382_0118:7:1:8468:1049#0/1
#GAAATGCAGCTTTTTCTCAGACGACGAGCG
#+
#ahfhhh_fhhhggghgh`chffhfff]caf
my $runMac = 1;

my $progOffset = './'; # '../../../' ;

chomp @ARGV;



if ($runMac) {
    
    $progOffset = './' ;
    #$bowTieIndices =  ../ ;
}
#$progOffset = "../../CamsCLIPseq1st";
my $fileIn = $ARGV[1] ;
my $bowFileIn = $ARGV[2] ;
my $numPeaks = $ARGV[0] ; # 25 ;

my $fastQfileIn = "$progOffset".$fileIn  ;  #'cntl.fastq';

my $bowtieHitsIn = "$progOffset".$bowFileIn ;  # 'cntl50.NonUnique.Bowtiehits';

my $miRNAfile = "$progOffset"."matureMiRNAHsa.fa";  # "matureMiRNAHsa.fa"; #

open (MIRNAS,"<$miRNAfile") or die "could not open file $miRNAfile for reading\n";
open (GRABFILE, "<$fileIn") or die "could not open file $fileIn for reading \n";
open (BOWFILE, "<$bowtieHitsIn") or die "could not open file $bowtieHitsIn for reading \n";


my $polyFlag = 1;
#open (FASTQIN,"<$fastQfileIn") or die "could not open file $fastQfileIn for reading\n";  # open fastq file to read in seqs

#if (not -e "$fileIn.Seq")
#if (){}


my $fullLinker = 'CAG ACG ACG AGC GGG A';
#print "linker is $fullLinker  and test seq is $testSeq $testSeqNums\n";
$fullLinker =~ s/\s+//g ;
print "fullLinker\n$fullLinker\n";
$fullLinker =~ tr/ATCG/1234/;
#print "$fullLinker\n";

my $startPos = 30 - length($fullLinker) ;



my $testSeq = 'TAGCAGCACGAAATATTGGGCGAAACAGCAG';
my $numSeq =  '123456789012345678901234567890123456';
#                             CAGACGACGAGCGGGA





my $truncNum = 0 ;
my $dumpSeqs = 0;



if (defined ($ARGV[3]) && ($ARGV[3] > 0 )) {$truncNum = $ARGV[2]; if ($ARGV[2] < 0){$truncNum = 0} }
if (defined ($ARGV[4]) && ($ARGV[4] > 0 )) {$dumpSeqs = 1}

my $truncNumOne = $truncNum ; 
my %seqFreqs;
my %polyFreqs ;

my $maxReadLeng = 25;
my $minReadLeng = 16;

print "selected minReadLeng : $minReadLeng, maxReadLeng : $maxReadLeng\n";
my $inLine ='';
# my $truncMinus = -1 * $truncNum;
while (<GRABFILE>){
    next if /^$/ ;
    chomp;
    next unless /^[ATCGNatcgn]{$minReadLeng,$maxReadLeng}/;
    my $inStrLen = length($_) ;
    if(0){  # truncation and strip remaining linkers
        if ($truncNum){    # fixed truncation of 3 ' end of reads
            
            if ($inStrLen < $truncNum) {print "\n" ; print "problem line : \n$_\n"}
            else {
                $inLine = substr($_,0,$inStrLen - $truncNumOne);
                $seqFreqs{$inLine}++ ;
            }
        }
        else {
            $startPos = 31 - length($fullLinker) ;
            my @linkerArr =  split('',$fullLinker);
            #print "   startPos $startPos    ";
            $inLine = uc $_ ;
            my $procString = substr($inLine,($startPos - 1));
            my $SeqNums = $procString;
            $SeqNums =~ tr/ATCG/1234/;
           
            my @procStrArr =  split('',$SeqNums);
            
            #print "$procStringText seq\n";
            #print "$procString seq\n" ;
            #print "$fullLinker linker\n" ;
            
            if (length(@procStrArr) < length(@linkerArr)) {
                until ((length(@procStrArr) == length(@linkerArr))) {
                    my $dumpIt = pop @linkerArr;
                    #print "popping linkerArr char $dumpIt\n";
                }
            }
            elsif (length(@procStrArr) > length(@linkerArr)) {
                until ((length(@procStrArr) == length(@linkerArr))) {
                    my $dumpIt = shift @procStrArr;
                    print "start was $startPos shifting procStrArr char $dumpIt\n";
                    $startPos++;
                }
            }
            my $remainingProcSeq = join('',@procStrArr) ;
            my $remainingLinker = join('',@linkerArr);
            until ($remainingProcSeq eq  $remainingLinker){
                my $dumpIt = shift @procStrArr;
                #print "start was $startPos shifting procStrArr char $dumpIt\n ";
                $startPos++;
                $remainingProcSeq = join('',@procStrArr) ;
                $dumpIt = pop @linkerArr;
                #print "popping linkerArr char $dumpIt\n";
                $remainingProcSeq = join('',@procStrArr) ;
                $remainingLinker = join('',@linkerArr);
                #print "rem $remainingProcSeq\nrem $remainingLinker remStop\n"
            }
            
        }
    }
        $inLine = uc $_ ;
        my $procString =  $inLine ; #  substr($inLine,($startPos - 1));
        my $SeqNums = $procString;
        $SeqNums =~ tr/ATCG/1234/;
        my $rightTrimmedSeq = $inLine ;# substr($inLine,0,($startPos-1)) ;
        my $polyTest = $rightTrimmedSeq ;
        $seqFreqs{$rightTrimmedSeq}++ ;  # write seq into hash or increment existing keys
        #$polyTest =~ /.{3}$/ ;
        $polyTest = substr($polyTest,length($polyTest) -3 , 3 );
        #print "  polyTest  $polyTest     ";
        if ( $polyTest eq 'AAA'){
            $polyFreqs{$rightTrimmedSeq}++ ;
        }
    
}

print  "\ngrabbed $fileIn\n";

#>hsa-let-7f-1* MIMAT0004486 Homo sapiens let-7f-1*
#CUAUACAAUCUAUUGCCUUCCC
#>hsa-let-7f-2* MIMAT0004487 Homo sapiens let-7f-2*
#CUAUACAGUCUACUGUCUUUCC
#>hsa-miR-15a MIMAT0000068 Homo sapiens miR-15a
#UAGCAGCACAUAAUGGUUUGUG
#>hsa-miR-15a* MIMAT0004488 Homo sapiens miR-15a*
#CAGGCCAUAUUGUGCUGCCUCA
my %miRNAs ;
my @miRNAarray ;
while (<MIRNAS>){
    #print ;
    if( />hsa/ ){
        chomp;
        my @fields = split(/\s+/,$_) ;
        my $miRNA =  <MIRNAS> ;
        chomp $miRNA ;
        $miRNA =~ s/U/T/g ;
        push (@miRNAarray, $miRNA) ;
        $miRNAs{$miRNA} = $fields[-1];  # grab last name of miRNA
        #print "$miRNAs{$miRNA}     $miRNA     ";
    }
    #my $drop = <MIRNAS> ;   
}


print  "\ngrabbed $miRNAfile\n";

#HWI-EAS382_0118:7:1:13199:1036#0/1	-	chr3	121858948	AATAACACTGTCTGGTAGAGAT	BD<<:B2=2;643-B:B?B2:D	0	5:C>A
#HWI-EAS382_0118:7:1:13189:1036#0/1	-	chr2	121858958	AATAACTCTGTCTGGTAAAGAT	BD<<:B2=2;643-B:B?B2:D	0	5:C>A
#HWI-EAS382_0118:7:1:13179:1036#0/1	-	chr2	121858968	AATAACGCTGTCTGGTAAAGAT	BD<<:B2=2;643-B:B?B2:D	0	5:C>A
#HWI-EAS382_0118:7:1:13169:1036#0/1	-	chr2	121858988	AATAACCCTGTCTGGTAAAGAT	BD<<:B2=2;643-B:B?B2:D	0	5:C>A
#HWI-EAS382_0118:7:1:13259:1036#0/1	-	chr2	121858978	AATAACACTTTCTGGTAAAGAT	BD<<:B2=2;643-B:B?B2:D	0	5:C>A
my %bowMap ;   # hash of mapping to genome count
my %bowLoc ;  # hash of array of map locations
while (<BOWFILE>){
    chomp;
    my @fields =  split;
    my $readSeq = $fields[4];
    my $readLeng = length ($readSeq) ;
    my $readStart = $fields[3];
    my $readEnd = $fields[3]+$readLeng ;
    if ($fields[1] eq '-'){
        $readSeq = revcomLine($readSeq);
        #$readStart = $fields[3] - $readLeng;
        #$readEnd = $fields[3] ;
        
    }
    $fields[3]++ ;
    $bowMap{$readSeq}++;
    $bowLoc{$readSeq} = "  $fields[2] $fields[3] $fields[1] :  $fields[2]:$readStart-$readEnd";
}

print  "\ngrabbed $bowtieHitsIn\n";

my $numReadsTot = 0 ;
my $nuCount = 0 ;
my $numSingletons = 0;
my $numDoubletons = 0;

my @peakFreqs  ;
my @peakSeqs  ;
my @peakInitVals ;
my $notPastPeaks = 1 ;
#while ($numPeaks > -1){
#    push @peakInitVals , (-20 -$numPeaks ) ;
##   $numPeaks--;
#}

my $peakSeq = '';
foreach (reverse sort {$seqFreqs{$a} <=> $seqFreqs{$b}}keys %seqFreqs){
    my $freq = $seqFreqs{$_} ;
    $nuCount++;
    if ($notPastPeaks){
        {push @peakFreqs, $freq  ; push @peakSeqs, $_}
        if ($nuCount > $numPeaks){$notPastPeaks = 0}
    }
    if ($dumpSeqs ) {print "$_ $freq\n";}
    $numReadsTot +=  $freq ;
    if ($freq eq 1){$numSingletons++}
    if ($freq eq 2){$numDoubletons++}
}
my $CountOut = 0 ;
print "\nINPUTS : file $ARGV[1] 3' truncations $ARGV[2]" ;
print "\ntot seqs $numReadsTot\nsingles $numSingletons\ndoubles $numDoubletons\n" ;
print "peak seqs and freqs :  \n";
my $mapped ;
while (@peakFreqs){
    $CountOut++ ;
    my $errors = 1 ;
    my $peakFreq = shift @peakFreqs ;
    my $peakSeq = shift @peakSeqs ;
    my $mapped = "Not mapped to genome <<<<<<<<<<<<<<<<<<<< " ;
    if (exists($bowMap{$peakSeq})){$mapped = "mapped to $bowLoc{$peakSeq}"}
    my $miRNAnameIs ="-" ;
    foreach (keys %miRNAs){
        if ($peakSeq =~ $_ ){
            $miRNAnameIs = $miRNAs{$_};
            print "$CountOut  $peakSeq  $peakFreq  $miRNAnameIs $mapped\n";
            last ;  # drop out of the foreach keys %miRNAs looop
            #next ;  # next read seq
        }
    }
    unless ($miRNAnameIs eq "-" ) {
        print "$CountOut  $peakSeq  $peakFreq  $miRNAnameIs $mapped\n";
        next;
    }
    
    while ($errors < 11) {
        my @catches = amatch($peakSeq, [''."$errors".''], @miRNAarray);
        if ($#catches > 0) {
            foreach my $line(@catches) {
                my $miRNAseq = $line ;
                $miRNAnameIs = $miRNAs{$miRNAseq} ;
                $mapped =  $mapped." $line with num errors:$errors" ;
                #print "$fields[0],$fields[1],$fields[2],$miRNAs{$miRNAseq},$line,errors,$errors\n" ;
                #print $peakSeq ."  Hit String with \n$line & miRNA : $miRNAs{$miRNAseq} errors $errors\n";
            }
            last ; next ;
        }
        $errors++ ;
    }
    
    print "$CountOut  $peakSeq  $peakFreq  $miRNAnameIs $mapped\n";
                   
}


print "\n";

if ($polyFlag){  # dump poly flag information
    print "\n";
    
    my $numReadsTot = 0 ;
    my $nuCount = 0 ;
    my $numSingletons = 0;
    my $numDoubletons = 0;
    
    my @peakFreqs  ;
    my @peakSeqs  ;
    my @peakInitVals ;
    my $notPastPeaks = 1 ;
    #while ($numPeaks > -1){
    #    push @peakInitVals , (-20 -$numPeaks ) ;
    ##   $numPeaks--;
    #}
    
    my $peakSeq = '';
    foreach (reverse sort {$polyFreqs{$a} <=> $polyFreqs{$b}}keys %polyFreqs){
        my $freq = $polyFreqs{$_} ;
        $nuCount++;
        if ($notPastPeaks){
            {push @peakFreqs, $freq  ; push @peakSeqs, $_}
            if ($nuCount > $numPeaks){$notPastPeaks = 0}
        }
        if ($dumpSeqs ) {print "$_ $freq\n";}
        $numReadsTot +=  $freq ;
        if ($freq eq 1){$numSingletons++}
        if ($freq eq 2){$numDoubletons++}
    }
    my $CountOut = 0 ;
    print "\nINPUTS : file $ARGV[1] 3' truncations $ARGV[2]" ;
    print "\ntot seqs $numReadsTot\nsingles $numSingletons\ndoubles $numDoubletons\n" ;
    print "peak seqs and freqs :  \n";
    while (@peakFreqs){
        $CountOut++ ;
        my $mapped = "Not mapped to genome <<<<<<<<<<<<<<<<<<<< " ;
        my $peakFreq = shift @peakFreqs ;
        my $peakSeq = shift @peakSeqs ;
        if (exists($bowMap{$peakSeq})){$mapped = "mapped to $bowLoc{$peakSeq}"}
        my $miRNAnameIs ="-" ;
        foreach (keys %miRNAs){
            if ($peakSeq =~ $_ ){$miRNAnameIs = $miRNAs{$_}}
        }
        print "$CountOut  $peakSeq  $peakFreq  $miRNAnameIs  $mapped\n";
                       
    }
    print "\n";


}

exit 0;

sub keep
{
my ($minASCII, $maxASCII) = (256, -20);
my @ASCIIvals ;
my @chrStr   ;
foreach (@chrStr){
    my @strs = split;
    chomp @strs;
    my $val = ord($_);
    push @ASCIIvals, $val;
    if ($val < $minASCII){$minASCII = $val}
    if ($val > $maxASCII){$maxASCII = $val}
}
}

sub revcomLine {
    my $inSeq = shift;
    chomp $inSeq;
    $inSeq =~ tr  [ATCGatcg] [TAGCtagc]
      ; #  or tr/a-z/A-Z/;   # shift all to upper case   [ATCGatcg] [TAGCtagc] ;    [ACGTRYMKSWBDHVNacgtrymkswbdhvn]  [TGCAYRKMWSVHDBNtgcayrkmwsvhdbn];  #
    $inSeq = reverse $inSeq;
    return $inSeq;
}
