import logging
import re

def generate_mis_matches(sequence, num_mis_matches):
    logger = logging.getLogger(__name__)
    logger.debug("generate_mis_matches(%s, %d)", sequence, num_mis_matches)
    mismatches = set()

    if num_mis_matches > 0:
        for i in range(0, len(sequence)):
            mm_list = list(sequence)
            mm_list[i] = '.'
            new_str = ''.join(mm_list)
            logger.debug("i=%d, sub_seed_seq = %s, new_str=%s", i, sequence, new_str)
            mismatches.update(generate_mis_matches(new_str, num_mis_matches-1))
    else:
        mismatches.add(sequence)
    
    return mismatches

def match_patterns_in_file(source, patterns, mandatory):
    data = {}
    i = 0
    for line in source:
        i += 1
        for (name, pattern) in patterns.iteritems():
            match_obj = re.search(pattern, line)
            if match_obj:
                data[name] = match_obj.group(1)

    if mandatory:
        num_lines = i
        for (name, pattern) in patterns.iteritems():
            if not data.get(name):
                error_str = "Pattern: %s = %s had no matches (searched %d lines)" % (name, pattern, num_lines)
                raise ValueError(error_str)

    return data

def match_strings(pattern, iterable):
    matches = []
    regex = re.compile(pattern)
    for s in iterable:
        if regex.search(s):
            matches.append(s)
    return matches
