'''
Helpers for SciPy and numpy

Created on Jan 2, 2013

@author: dave
'''

from scipy import stats
import numpy as np

PEARSON = "PEARSON"
SPEARMAN = "SPEARMAN"
R_VALUE_KEY = "r_value"
REGRESSION_COLUMNS = ['slope', 'intercept', R_VALUE_KEY, 'p_value', 'std_err']

def regression_as_dict(x, y):
    data = {}
    for (k, v) in zip(REGRESSION_COLUMNS, stats.linregress(x, y)):
        data[k] = v

    data["r_squared"] = data[R_VALUE_KEY] ** 2
    return data

def pack_into_boxes(np_array, values_per_box):
    size = int(len(np_array) / values_per_box)
    packed_array = np.zeros(size, dtype='i')
    for i in range(0, len(np_array)):
        j = i / values_per_box
        packed_array[j] += np_array[i]
    
    return packed_array

def mean_arrays(np_arrays):
    ''' @param np_arrays: array of arrays '''
    return sum(np_arrays) / len(np_arrays)

def correlation_pvalues(x, y, **kwargs):
    correlation_type = kwargs.get('correlation', PEARSON)
    if correlation_type == SPEARMAN:
        (correlation, p_value) = stats.spearmanr(x, y)
    elif correlation_type == PEARSON:
        (correlation, p_value) = stats.pearsonr(x, y)
    else:
        raise ValueError("Unknown correlation type '%s'" % correlation_type)
    return (correlation, p_value)
