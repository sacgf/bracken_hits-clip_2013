import gzip
import logging
from logging.handlers import RotatingFileHandler
import os
import sys

import numpy as np

class Struct:
    def __init__(self, **entries): self.__dict__.update(entries)

def get_open_func(file_name):
    if file_name.endswith(".gz"):
        return gzip.open
    else:
        return open

def remove_gz_if_exists(filename):
    if filename.endswith(".gz"):
        filename = os.path.splitext(filename)[0] # remove .gz
    return filename
    
def require_args(a, *args):
    if len(sys.argv) != len(args) + 2:
        args_str = " ".join([a] + list(args))
        sys.stderr.write("Usage %s %s \n" % (os.path.basename(sys.argv[0]), args_str)) 
        sys.exit(1)

def setup_basic_rotating_logging(log_file, log_level):
    logger = logging.getLogger()
    logger.setLevel(log_level)
    handler = RotatingFileHandler(log_file, backupCount=5)
    handler.doRollover()
    handler.setFormatter(logging.Formatter(logging.BASIC_FORMAT))
    logger.addHandler(handler)
    return logger    
