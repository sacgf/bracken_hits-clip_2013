import glob
import os
import random
import string
import subprocess

def file_or_file_name(f, mode='r'):
    if isinstance(f, basestring): # Works on unicode
        if 'w' in mode: # Create path if writing
            mk_path_for_file(f)
            
        return open(f, mode)
    elif isinstance(f, file):
        return f # Already a File object
    else:
        raise ValueError("'%s' (%s) not a file or string" % (f, type(f)))

def mk_path(path):
    if path and not os.path.exists(path):
        os.makedirs(path)

def mk_path_for_file(f):
    mk_path(os.path.dirname(f))

def name_from_file_name(file_name):
    '''Gets file name with removing extension and directory'''
    return os.path.splitext(os.path.basename(file_name))[0]

def file_to_hash(f, sep=None):
    data = {}
    for line in file_or_file_name(f):
        line = line.rstrip()
        key = line
        value = None
        if sep:
            items = line.split(sep)
            if len(items) >= 2:
                (key, value) = items[0:2]
        data[key] = value
    return data

def single_file_from_wildcard(pathname):
    files = glob.glob(pathname)
    num_files = len(files)
    if num_files == 1:
        return files[0]

    problem = "No matches"
    if num_files > 1:
        problem = "More than 1 (%d) matches" % num_files
    raise ValueError("%s for filename pattern '%s'" % (problem, pathname))

def line_count_wc(filename):
    '''
    Use wc, the unix tool, to count and return the number of lines of a file.
    From https://gist.github.com/zed/0ac760859e614cd03652
    '''
    out = subprocess.Popen(['wc', '-l', os.path.expanduser(filename)],
                         stdout=subprocess.PIPE,
                         stderr=subprocess.STDOUT
                         ).communicate()[0]
    return int(out.partition(b' ')[0])

def line_count(file_name):
    lines = 0
    with open(file_name, "r") as f:
        for _ in f:
            lines += 1
    return lines

def random_file_name():
    """
    returns a random string of 8 characters which can be used as a 'unique' file name
    """
    return ''.join(random.choice(string.ascii_uppercase) for _ in range(8))

def random_file_at_same_dir(file_path, prefix='', extension=''):
    """
    returns a file path with random file name and in the same dir as file_path. 
    prefix and extension will be added to the random file name, if available.
    """
    fname = ''
    if prefix:
        fname = prefix + '.'
    fname += random_file_name()
    if extension:
        fname += '.' + extension
    return os.path.join(os.path.dirname(file_path), prefix + '.' + random_file_name() + '.' + extension)

def remove_a_file(path):
    """remove a file if that file is existing and removable"""
    try:
        os.remove(path)
        return True
    except:
        return False

def remove_gz_if_exists(filename):
    if filename.endswith(".gz"):
        filename = os.path.splitext(filename)[0] # remove .gz
    return filename

def absolute_file_path(file_path):
    """return the absolute file path, with converting '~' to user's home directory.
    if file_path is not a string, just return file_path per ser """
    if type(file_path) is not str:
        return file_path
    else:
        return os.path.abspath(os.path.expanduser(file_path))


def array_to_file(file_name, array):
    mk_path_for_file(file_name)
    with open(file_name, "w") as f:
        for line in array:
            f.write(line + "\n")

def file_to_array(file_name, comment=None):
    array = []
    with open(file_name) as f:
        for line in f:
            if comment and line.startswith(comment):
                continue
            array.append(line.rstrip())
    return array

def base_project_dirname():
    ''' Returns the full path name of the project '''
    return up_dir(os.path.dirname(__file__), 2) # Relative to current file (change if you ever move!)

def up_dir(path, n):
    ''' Basically like os.path.join(path, "../" * n) but without the dots '''
    assert n >= 0
    path_components = path.split(os.path.sep)
    return os.path.sep.join(path_components[:-n])

def file_name_insert(insert, file_name):
    ''' Return "insert" just before the file extension '''
    (file_path, extension) = os.path.splitext(file_name)
    out_filename = "%s.%s%s" % (file_path, insert, extension)
    return out_filename
