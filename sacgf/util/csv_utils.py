import csv
import logging
from sacgf.util import file_utils

def write_csv_dict(csv_file, headers, rows, extrasaction=None, dialect=None):
    '''
    default dialect = 'excel', other dialect option: 'excel-tab'
    These are the same optional arguments as csv.DictWriter
    headers=keys for dicts
    rows=list of dicts
    '''
    logger = logging.getLogger(__name__)
    
    if extrasaction is None:
        extrasaction = "raise"
    if dialect is None:
        dialect = 'excel'
    
    logger.info("Writing %s", csv_file)
    f = file_utils.file_or_file_name(csv_file, "wb")

    writer = csv.DictWriter(f, headers, extrasaction=extrasaction, dialect=dialect)
#        writer.writeheader()
#        e_r_s_a uses python 2.6
    writer.writerow(dict(zip(headers, headers)))
    writer.writerows(rows)

def filter_csv(peaks, **kwargs):
    filter_func = kwargs.get("filter_func")
    sort_key_func = kwargs.get("sort_key")
    top = kwargs.get("top")

    my_peaks = peaks
    if filter_func:
        my_peaks = filter(filter_func, my_peaks)
        
    if sort_key_func:
        my_peaks = sorted(my_peaks, key=sort_key_func, reverse=True)
    if top:
        my_peaks = my_peaks[:top]
    return my_peaks

