'''
Created on Jan 15, 2013

@author: dave
'''

from matplotlib import pyplot

def write_scatter_colored_gene_set_plot(graph_image, exp, x_col, y_col, colored_gene_sets):
    '''
    colored_gene_sets = {"8mer" : ("#ff0000", set(["MSN", "CFL2"])), # Draw these 8mer's red
                       "7mer" : ("#0000ff", set([MSN", "ROCK2"])} # Draw these ones blue

                    key = label in plot legend
                    value = tuple of (color, set of genes that will be coloured)
                    
    exp = data frame with x and y columns
    x_col and y_col = names of columns to plot    
    '''
    pyplot.figure()
    sp = pyplot.subplot(1, 1, 1)
    scatter_colored_gene_set_plot(sp, exp, x_col, y_col, colored_gene_sets)
    pyplot.savefig(graph_image)
    pyplot.close()    

def scatter_colored_gene_set_plot(subplot, df, x_col, y_col, colored_gene_sets):
    # Draw all the genes in black
    subplot.scatter(df[x_col], df[y_col], color="#000000", s=0.2, marker='o', alpha=0.2)
    
    for (name, (color, gene_set)) in colored_gene_sets.iteritems():
        sub_set = df.ix[ df.index.isin(gene_set) ]
        im = subplot.scatter(sub_set[x_col], sub_set[y_col], c=color, s=3, edgecolors='none', label=name)

    subplot.set_xlabel(x_col)
    subplot.set_ylabel(y_col)
    subplot.legend(loc=2)
    return im
