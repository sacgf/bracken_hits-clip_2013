'''
Created on Mar 19, 2013

@author: dlawrence
'''

from matplotlib.backends.backend_agg import FigureCanvasAgg
from matplotlib.cm import ScalarMappable
from matplotlib.figure import Figure
from matplotlib.patches import Rectangle
from sacgf.util import file_utils
import abc
import numpy as np

class GraphBase(object):
    __metaclass__ = abc.ABCMeta

    def __init__(self, **kwargs):
        '''
            legend: [('label', 'color'), ('label2', 'color2')]
        '''
        self.title = kwargs.get("title")
        self.x_label = kwargs.get("x_label")
        self.y_label = kwargs.get("y_label")
        self.legend = kwargs.get('legend')
        self.plot_methods = [self.plot]

    def decorations(self, ax):
        if self.title:
            ax.set_title(self.title)
        if self.x_label:
            ax.set_xlabel(self.x_label)
        if self.y_label:
            ax.set_ylabel(self.y_label)

    @abc.abstractmethod
    def plot(self, ax):
        return

    def post_plot(self, ax):
        if self.legend:
            patches = []
            labels = []
            for (name, color) in self.legend:
                labels.append(name)
                patches.append(Rectangle((0, 0), 1, 1, fc=color))

            ax.legend(patches, labels, loc='upper left')

    def figure(self, figure):
        ''' a hook method if you want to do something about the figure '''
        pass

    def save(self, filename_or_obj, dpi=None):
        figure = Figure(dpi=dpi)
        figure.patch.set_facecolor('white')
        ax = figure.add_subplot(1, 1, 1)
        
        self.decorations(ax)
        for plot in self.plot_methods:
            plot(ax) # Implementation
        self.post_plot(ax)
        self.figure(figure)

        if isinstance(filename_or_obj, basestring):
            file_utils.mk_path_for_file(filename_or_obj)
        
        canvas = FigureCanvasAgg(figure)
        canvas.print_png(filename_or_obj)
        
    def draw_origin(self, ax):
        ax.axvline(c='black', lw=0.5) 
        ax.axhline(c='black', lw=0.5)

    def colorbar_from_cmap_array(self, figure, cmap, array):        
        mappable = ScalarMappable(cmap=cmap)
        mappable.set_array(array)
        figure.colorbar(mappable)
        


class BivariateGraph(GraphBase):
    def __init__(self, x, y, **kwargs):
        ''' x, y : numpy arrays to compare
            log (boolean) : Whether to log x/y (alters values and x/y labels)
        '''
        super(BivariateGraph, self).__init__(**kwargs)
        if kwargs.pop("log", False):
            # Adjust axis labels
            self.x_label = "log(1+%s)" % self.x_label
            self.y_label = "log(1+%s)" % self.y_label
            x = np.log(1 + x)
            y = np.log(1 + y)

        self.x = x
        self.y = y
        