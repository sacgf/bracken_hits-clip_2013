'''
Created on Apr 11, 2013

@author: dave
'''

from sacgf.graphing.graph_base import GraphBase, BivariateGraph
from sacgf.util import stats_utils
import numpy as np
import os
import pandas as pd

class ScatterMatrixGraph(GraphBase):
    ''' Wrapper around pandas method (so we can save it) '''   
    def __init__(self, dataframe):
        super(ScatterMatrixGraph, self).__init__()       
        self.dataframe = dataframe
    
    def plot(self, ax):
        pd.scatter_matrix(self.dataframe, ax=ax)


class ScatterPlot(BivariateGraph):
    def __init__(self, x, y, **kwargs):
        '''
            x, y : arrays to plot
            
            optional:
                (GraphBase init args)
                point_size (8)
                color (black)
                alpha (default = 0.2 for >1000 points, no transparency for <=1000 points)
                marker ('o')
                correlation: PEARSON or SPEARMAN
                p_value (False) (requires correlation to be set)
                show_points (False)
                line_of_best_fit: (False)
                edgecolor: (color)
        '''
        super(ScatterPlot, self).__init__(x, y, **kwargs)
        self.point_size = kwargs.get("point_size", 8)
        self.color = kwargs.get("color", "black")
        self.marker = kwargs.get("marker", 'o')
        self.correlation = kwargs.get("correlation")
        self.show_points = kwargs.get("show_points")
        self.line_of_best_fit = kwargs.get("line_of_best_fit")
        self.edgecolor = kwargs.get("edgecolor")
        self.p_value = kwargs.get('p_value')
        self.label = None

        # If we have more than 1000 points, use transparency by default
        default_alpha = 1
        if len(x) > 1000:
            default_alpha = 0.2
        self.alpha = kwargs.get("alpha", default_alpha)


    @staticmethod
    def from_dataframe(df, column_x, column_y, **kwargs):
        ''' convenience method to make sure columns always match up with labels '''
        
        x = df[column_x]
        y = df[column_y]
        return ScatterPlot(x, y, x_label=column_x, y_label=column_y, **kwargs)

    def center_axis(self, ax):
        #get and set axes to be symmetrical for x and y
        xmin, xmax = ax.get_xlim()
        ymin, ymax = ax.get_ylim()
    
        if min(self.x) >= 0: xmin = 0
        if min(self.y) >= 0: ymin = 0
        if max(self.x) <= 0: xmax = 0
        if max(self.y) <= 0: ymax = 0
        
        if xmin < 0 and xmax > 0: #only adjust if they span the y axis
            if -xmin < xmax:
                xmin = -xmax
            else:
                xmax = -xmin
        
        if ymin < 0 and ymax > 0: #only adjust if they span the x axis
            if -ymin < ymax:
                ymin = -ymax
            else:
                ymax = -ymin
        
        ax.set_xlim(xmin, xmax)    
        ax.set_ylim(ymin, ymax)
        
    def plot(self, ax):
        x = self.x
        y = self.y
        
        ax.scatter(x, y, c=self.color, s=self.point_size, marker=self.marker, alpha=self.alpha, edgecolor=self.edgecolor, label=self.label)

        self.center_axis(ax)
        self.draw_origin(ax)

        y_pos = 0.95
        if self.correlation:
            (correlation_coefficient, p_value) = stats_utils.correlation_pvalues(x, y, correlation=self.correlation)
            text = "%s r^2: %.3f" % (self.correlation, correlation_coefficient**2)
            if self.p_value:
                text += ' (p-value: %f)' % p_value
            ax.text(0.05, y_pos, text, va='center', transform=ax.transAxes)
            y_pos -= 0.05
        
        if self.show_points:
            num_points = len(x)
            text = "points = %d" % num_points
            ax.text(0.1, y_pos, text, ha='center', va='center', transform=ax.transAxes)

        if self.line_of_best_fit:
            coefs = np.lib.polyfit(x, y, 1)
            fit_y = np.lib.polyval(coefs, x)
            ax.plot(x, fit_y, 'k--', lw = 1)
        


class ScatterPlotWithErrorbars(ScatterPlot):
    ''' ScatterPlot with error bars drawn at the end '''
    def __init__(self, x, y, error_bars, **kwargs):
        super(ScatterPlotWithErrorbars, self).__init__(x, y, **kwargs) 
        self.error_bars = error_bars
        self.plot_methods.append(self.plot_errorbars)
    
    def plot_errorbars(self, ax):
        ax.errorbar(self.x, self.y, yerr=self.error_bars, fmt='o', capsize=3)

class ScatterPlotWithColoredPoints(ScatterPlot):
    '''Scatterplot with colored gene sets drawn at the end'''
    def __init__(self, x, y, colored_lists, **kwargs):
        '''
        labelled (boolean): whether to print colored set names on plot legend, default=False.
        colored_lists: list of either:
            if labelled=True: 4-part tuples of (x_data, y_data, color, label)
            if labelled=False: 3-part tuples of (x_data, y_data, color)
        where x_data and y_data are lists of x and y coordinates the colored points
        and color is in a matplotlib format
        '''
        kwargs['alpha'] = kwargs.get('alpha', 1.0)
        self.colored_lists = []
        super(ScatterPlotWithColoredPoints, self).__init__(x, y, **kwargs)
        self.edgecolor = kwargs.get('edgecolor', self.color)
        self.colored_lists = colored_lists
        labelled = kwargs.get('labelled', False)
        if labelled == True:
            self.plot_methods.append(self.plot_labelled_colored_lists)
        else:
            self.plot_methods.append(self.plot_colored_lists)
        
    def plot_labelled_colored_lists(self, ax):
        for x, y, color, label in self.colored_lists:
            ax.scatter(x, y, c=color, edgecolor=color, s=self.point_size, marker=self.marker, alpha=self.alpha, label=label)

        ax.legend(loc=0, scatterpoints=1)
        
    def plot_colored_lists(self, ax):
        for x, y, color in self.colored_lists:
            ax.scatter(x, y, c=color, edgecolor=color, s=self.point_size, marker=self.marker, alpha=self.alpha)


def write_scatterplot(graph_image, x, y, **kwargs):
    graph = ScatterPlot(x, y, **kwargs)
    graph.save(graph_image)

def write_scatterplot_from_dataframe(graph_image, dataframe, column_x, column_y, **kwargs):
    ''' Convenience method to do common comparison in a single function  '''
    
    defaults = {'correlation' : stats_utils.PEARSON,
                'show_points' : True,
                'line_of_best_fit' : True,
    }

    for (k, v) in defaults.iteritems():
        kwargs[k] = kwargs.get(k, v)
    
    graph = ScatterPlot.from_dataframe(dataframe, column_x, column_y, **kwargs)
    graph.save(graph_image)
    

def write_scatterplot_from_dataframe_auto_filename(out_dir, title, dataframe, column_x, column_y, **kwargs):
    ''' Filename comes from title/columns '''
    name = "_".join([ title, column_x, column_y])
    graph_image = os.path.join(out_dir, name + ".png")
    write_scatterplot_from_dataframe(graph_image, dataframe, column_x, column_y, title=title, **kwargs)


