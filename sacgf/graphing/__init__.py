"""Graphing functions to simplify creation of Matplotlib graphs
    @see: http://matplotlib.org
"""

import logging
from matplotlib import pyplot
import matplotlib
from matplotlib.axes import Axes
import os

import matplotlib.cm as cm
import numpy as np
from sacgf import util
from sacgf.graphing import scatterplot
from sacgf.graphing.graph_base import GraphBase
from sacgf.graphing.scatterplot import ScatterPlot
from sacgf.util import stats_utils, file_utils


matplotlib.use('agg', warn=False)

logger = logging.getLogger(__name__)

def write_graph(graph_image, largs, profile, **kwargs):
    logger.info("write_graph: %s", graph_image)
    
    title = kwargs.get("title")
    x_label = kwargs.get("x_label")
    y_label = kwargs.get("y_label")
    extra_func = kwargs.get("extra_func")
    legend_props = kwargs.get("legend_props", {})

    fig = pyplot.figure()
    lines = pyplot.plot(largs, profile)

    if title:
        pyplot.title(title)
    if x_label:
        pyplot.xlabel(x_label)
    if y_label:
        pyplot.ylabel(y_label)
    if extra_func:
        extra_func(pyplot, fig, lines)
        
    p = pyplot.subplot(1, 1, 1)
    box = p.get_position()
    p.set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9]) # Shrink height by 20% for legend to go outside
    p.legend(prop=legend_props)

    file_utils.mk_path_for_file(graph_image)
    pyplot.savefig(graph_image)
    pyplot.close()


def write_multi_graph(graph_image, largs, profiles, **kwargs):
    '''
    Draw graph with multiple profiles
    profiles (dict): key=profile name; value={"array":numpy array of data, "fmt": color} 
    '''
    logger.info("write_multi_graph: %s", graph_image)

    title = kwargs.get("title")
    x_label = kwargs.get("x_label")
    y_label = kwargs.get("y_label")
    ylim = kwargs.get("ylim")
    extra_func = kwargs.get("extra_func")
    legend_props = kwargs.get("legend_props", {})

    fig = pyplot.figure()
    if title:
        pyplot.title(title)
    if x_label:
        pyplot.xlabel(x_label)
    if y_label:
        pyplot.ylabel(y_label)
    if ylim:
        pyplot.ylim(ylim)
    p = pyplot.subplot(1, 1, 1)
    box = p.get_position()
    p.set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9]) # Shrink height by 20% for legend to go outside

#    p = pyplot.subplot()
    for (profile_name, profile_data) in sorted(profiles.iteritems()):
        lines = p.plot(largs, profile_data["array"], profile_data["fmt"], label=profile_name)
    
        if extra_func:
            extra_func(pyplot, fig, lines)

    p.legend(prop=legend_props)
#    p.legend(loc='upper center', bbox_to_anchor=(0.5, -0.1),
#              fancybox=True, shadow=True, ncol=3)
            
    file_utils.mk_path_for_file(graph_image)
    pyplot.savefig(graph_image)
    pyplot.close()


def compare_samples(out_dir, title, x, y, x_label, y_label):
    ''' Write basic scatterplot of 2 data sets, including r^2 and # points '''
    
    name = "_".join([ title, x_label, y_label])
    graph_image = os.path.join(out_dir, name + ".png")
    
    scatterplot.write_scatterplot(graph_image, x, y, title=title, x_label=x_label, y_label=y_label, correlation=stats_utils.PEARSON, show_points=True, line_of_best_fit=True)


def stacked_bar_graph(largs, arrays, labels, colors):
    bottom = np.zeros(len(largs), dtype='i')
    for array, label, color in zip(arrays, labels, colors):
        lines = pyplot.bar(largs, array, label=label, color=color, bottom=bottom, linewidth=0)
        bottom += array
    
    return lines

def write_stacked_bar_graph(graph_image, largs, arrays, labels, colors, **kwargs):
    '''largs = x-values
    arrays = list of lists y-values
    labels = list of labels, same length as arrays
    colors = list of colors, same length as arrays
    '''
    logger.info("write_stacked_bar_graph: %s", graph_image)

    title = kwargs.get("title")
    extra_func = kwargs.get("extra_func")
    subtitle = kwargs.get("subtitle")
    axvspan = kwargs.get("axvspan")
    loc = kwargs.get("loc")
    x_label = kwargs.get("x_label")
    y_label = kwargs.get("y_label")
    legend_title = kwargs.get("legend_title")
    legend_props = kwargs.get("legend_props", {})

    fig = pyplot.figure(figsize=(8, 6))
    lines = stacked_bar_graph(largs, arrays, labels, colors)

    if title:
        pyplot.suptitle(title, fontsize=18)
    if subtitle:
        pyplot.title(subtitle, fontsize=12)
    if extra_func:
        extra_func(pyplot, fig, lines)
    if axvspan:
        pyplot.axvspan(axvspan.xmin, axvspan.xmax, facecolor=axvspan.color, alpha=axvspan.alpha)
    if x_label:
        pyplot.xlabel(x_label)
    if y_label:
        pyplot.ylabel(y_label)

    pyplot.xlim(xmin=largs[0], xmax=largs[-1]+1)
    pyplot.legend(loc=loc, title=legend_title, prop=legend_props)
    
    file_utils.mk_path_for_file(graph_image)
    pyplot.savefig(graph_image, dpi=300)
    pyplot.close()

def write_individual_bar_graphs(graph_image, largs, arrays, labels, colors, **kwargs):
    ''' same as write_stacked_bar_graph but writes out 1 plot per entry in array '''

    (name_base, extension) = os.path.splitext(graph_image)
    
    title = kwargs.get("title", "")
    y_label = kwargs.get("y_label", None)
    for array, label, color in zip(arrays, labels, colors):
        kwargs["title"] = "%s (%s)" % (title, label)
        kwargs["color"] = color

        bargraph = BarGraph(largs, array)
        bargraph.y_label = y_label
        individual_graph_image = "%s.%s%s" % (name_base, label, extension) # extension still has dot
        bargraph.save(individual_graph_image)
    
    
class BarGraph(GraphBase):
    def __init__(self, x, y, **kwargs):
        super(BarGraph, self).__init__(**kwargs)
        self.x = x
        self.y = y
        self.labels = kwargs.get("labels")
        self.color = kwargs.get("color")
        
    def plot(self, ax):
        width = 0.5

        ax.set_xlim(xmin=self.x[0], xmax=self.x[-1]+1)
        ax.bar(self.x, self.y, width=width, color=self.color)

        if self.labels:
            ax.set_xticks(self.x + width/2)
            ax.set_xticklabels(self.labels)


def save_png_from_lambda(filename, drawing_operation):
    ''' lambda takes axis as argument ''' 

    class MyGraph(GraphBase):
        def plot(self, ax):
            drawing_operation(ax)

    graph = MyGraph()
    graph.save(filename)
