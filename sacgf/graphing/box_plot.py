'''
A BoxPlot aka a Box and Whisker plot - Useful for showing the average as well as the distribution of a series

Created on Dec 5, 2012

@author: dlawrence
'''

from matplotlib import pyplot

from sacgf.graphing.graph_base import GraphBase
from sacgf.util import file_utils


def write_box_plot(graph_image, data, **kwargs):
    '''
    data= e.g. list of lists of datapoints 
    '''
    title = kwargs.get("title")
    labels = kwargs.get("labels")
    x_label = kwargs.get("x_label")
    y_label = kwargs.get("y_label")
    extra_func = kwargs.get("extra_func")
    
    pyplot.figure()
    pyplot.boxplot(data)

    if title:
        pyplot.title(title)
    if labels:
        pyplot.setp(pyplot.gca(), 'xticklabels', labels) 
    if x_label:
        pyplot.xlabel(x_label)
    if y_label:
        pyplot.ylabel(y_label)
    if extra_func:
        extra_func()
    
    file_utils.mk_path_for_file(graph_image)
    pyplot.savefig(graph_image)
    pyplot.close()
    
def write_boxplot_from_df(df, graph_image, **kwargs):
    '''Writes boxplot from all columns in dataframe
    Uses column names as labels if labels not provided'''
    data_lists = []
    for column in df:
        data_lists.append(df[column])
    kwargs['labels'] = kwargs.get("labels", [c for c in df.columns])
    write_box_plot(graph_image, data_lists, **kwargs)
    
    
class BoxPlot(GraphBase):
    ''' Wrapper around pandas method (so we can save it) '''   
    def __init__(self, dataframe, **kwargs):
        super(BoxPlot, self).__init__() # Don't pass kwargs on (won't work anyway)
        self.dataframe = dataframe
        self.kwargs = kwargs
    
    def plot(self, ax):
        self.dataframe.boxplot(ax=ax, **self.kwargs)
