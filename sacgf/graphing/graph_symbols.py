'''
Hierarchy of symbols drawn on Matplotlib Graphs

Created on Nov 7, 2012

@author: dlawrence
'''
from matplotlib.collections import LineCollection
import numpy

ARROW_HEIGHT = 2

class Arrows(object):
    def __init__(self, color, positions):
        self.color = color
        self.positions = positions

    def draw(self, pyplot, max_value):        
        shape = len(self.positions)
        x = numpy.array(self.positions, dtype='i')
        y = numpy.empty(shape, dtype='i')
        y.fill(max_value+ARROW_HEIGHT)
        U = numpy.zeros(shape, dtype='i')
        V = numpy.empty(shape, dtype='i')
        V.fill(-ARROW_HEIGHT)

        pyplot.quiver(x, y, U, V, minshaft=1.3, angles='uv', scale_units='xy', scale=1, color=self.color)#, pivot='tip')


class Lines(object):
    def __init__(self, thickness, color, positions):
        self.thickness = thickness
        self.color = color
        self.positions = positions
    
    def draw(self, pyplot, max_value):
        segs = []
        for x in self.positions:
            segs.append(((x, max_value), (x, max_value+1)))

        num_lines = len(segs)
        colors = (self.color) * num_lines
        line_segments = LineCollection(numpy.array(segs, dtype='d'), linewidths=self.thickness, colors=colors, linestyle='solid')
        pyplot.gca().add_collection(line_segments)


class Symbols(object):
    def __init__(self, symbol, color, positions):
        self.symbol = symbol
        self.color = color
        self.positions = positions
    
    def draw(self, pyplot, max_value):
        for x in self.positions:
            pyplot.text(x, max_value, self.symbol, fontsize=24, color=self.color, horizontalalignment='center', verticalalignment='bottom')


def graph_symbols_factory(symbol, color, positions, thickness):
    if symbol is None:
        graph_symbols = Arrows(color, positions)
    elif symbol == '|': # Lines
        graph_symbols = Lines(thickness, color, positions)
    else:
        graph_symbols = Symbols(symbol, color, positions)
    return graph_symbols