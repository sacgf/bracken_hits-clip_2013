'''
Created on 27/06/2013

@author: katherine
'''

from matplotlib.ticker import Formatter

class MultiplyingLabelFormatter(Formatter):
    '''This is passed to matplotlib plots and is called on every labelling of a tick label'''
    def __init__(self, multiplier):
        self.multiplier = multiplier
        
    def __call__(self, x, pos=None):
        x = int(x)
        return "%d" % (x * self.multiplier)

class ToKiloBaseLabelFormatter(Formatter):
    '''This is passed to matplotlib plots and is called on every labelling of a tick label
    Formats values in bp in kb. E.g. 100 becomes 0.1kb
    kwargs:     show_plus (boolean, default = False): Print positive values with +, e.g. +1kb
                tss (boolean, default=False): Print 'TSS' instead of 0kb'''
    def __init__(self, **kwargs):
        self.use_tss_as_middle = kwargs.get('tss', False)
        self.show_plus = kwargs.get('show_plus', False)
        
    def __call__(self, label_in_bp, pos=None):
        label_in_kb = label_in_bp / 1000.0
        if self.use_tss_as_middle:
            if label_in_kb == 0:
                return "TSS"
        if self.show_plus:
            if label_in_kb > 0:
                return "+%gkb" % label_in_kb
        return "%gkb" % label_in_kb
