'''
Annotate HITS-CLIP peaks - reference annotations and Seed matches

Created on Sep 9, 2012

@author: dlawrence
'''

from sacgf import genomics, util
from sacgf.genomics.reference.transcript import NotOnTranscriptException
from sacgf.hitsclip.peaks.peak import peak_info_to_iv
from sacgf.mirna.seed_match import get_standard_seed_match_types
import numpy as np
import sys

def annotate_hc_peaks(config, reference, treatment_name, peaks):
    mir_name = {"200a" : "hsa-miR-200a-3p",
                "200b" : "hsa-miR-200b-3p"}
    mir = reference.get_mirna(mir_name[treatment_name])

    seed_match_types = get_standard_seed_match_types(mir)
    
    count_header_name_func              = lambda x : "num %s" % x
    count_transcript_header_name_func   = lambda x : "%s transcript" % x
    closest_match_header_name_func      = lambda x : "closest %s" % x
    closest_dist_header_name_func       = lambda x : "closest %s distance" % x
    closest_transcript_header_name_func = lambda x : "closest %s transcript" % x

    extra_headers = ['gene', 'region', 'transcript_id']
    for header_name_func in [count_header_name_func, count_transcript_header_name_func, closest_match_header_name_func, closest_dist_header_name_func, closest_transcript_header_name_func]:
#        extra_headers += [header_name_func(i["name"]) for i in seed_match_types] #delete me if line below works
        extra_headers += [header_name_func(i) for i in seed_match_types.keys()]
    
    for peak_info in peaks:
        iv = peak_info_to_iv(peak_info)
        transcript_ids = reference.get_transcript_ids(iv)
        peak_info["transcript_id"]   = " ".join(transcript_ids)
        peak_info["gene"]            = reference.get_gene_names(iv)
        peak_info['region']          = reference.get_region_names(iv)

    for seed_match_name, seed_match in seed_match_types.iteritems():
        for peak_info in peaks:
            iv = peak_info_to_iv(peak_info)
            transcript_ids = reference.get_transcript_ids(iv)
            closest = sys.maxint
            closest_match = ""
            closest_transcript = ""
            highest_count = 0
            highest_count_transcript = ""                 

            for transcript_id in transcript_ids:
                try:
                    transcript = reference.transcripts[transcript_id]
                    (start_mpos, end_mpos) = transcript.get_transcript_positions(iv)
                    peak_transcript_iv = genomics.transcript_iv(transcript_id, start_mpos, end_mpos)
                    mrna_sequence = str(transcript.get_transcript_sequence())
                    count = 0
                    for m in seed_match.find(mrna_sequence):
                        count += 1
                        seed_iv = util.Struct(start=m.start(0), end=m.end(0) + 1)
                        distance = genomics.distance(peak_transcript_iv, seed_iv)
                        if abs(distance) < abs(closest):
                            closest_match = m.group(0)
                            closest = distance
                            closest_transcript = transcript_id
                            
                    if count > highest_count:
                        highest_count = count
                        highest_count_transcript = transcript_id
                except NotOnTranscriptException:
                    pass # Skip this transcript

            peak_info[count_header_name_func(seed_match_name)] = str(highest_count)
            peak_info[count_transcript_header_name_func(seed_match_name)] = highest_count_transcript
            if closest != sys.maxint:
                peak_info[closest_match_header_name_func(seed_match_name)] = closest_match
                peak_info[closest_dist_header_name_func(seed_match_name)] = int(closest)
                peak_info[closest_transcript_header_name_func(seed_match_name)] = closest_transcript
            else:
                na = np.NaN
                peak_info[closest_match_header_name_func(seed_match_name)] = na
                peak_info[closest_dist_header_name_func(seed_match_name)] = na
                peak_info[closest_transcript_header_name_func(seed_match_name)] = na

    return extra_headers
