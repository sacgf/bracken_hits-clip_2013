from sacgf.genomics import bed_file
from sacgf.hitsclip.peaks.peak import peak_info_to_iv
import HTSeq
from sacgf.hitsclip.peaks import annotate, best_target, expression, sublists
import os
import pandas as pd

def write_peaks_bed_file(output_dir, treatment_name, peaks):
    bed_file_name = os.path.join(output_dir, "bedfiles", "%s_peaks.bed" % treatment_name)
    features = []
    for peak in peaks:
        iv = peak_info_to_iv(peak)
        feature = HTSeq.GenomicFeature("peak", "peak", iv)
        feature.score = peak['peak_ratio_max']
        features.append(feature)
    bed_file.write_features_to_bed_file(bed_file_name, features)

def peaks_to_annotated_data_frame(config, reference, treatment_name, peaks):
    extra_headers = annotate.annotate_hc_peaks(config, reference, treatment_name, peaks)
    peaks.sort(key=lambda p: p['peak_ratio_max'], reverse=True)

    # Load into Pandas table
    peaks_df = pd.DataFrame.from_dict(peaks)
    best_target.add_best_target_column(peaks_df)

    # Insert "best target" after "region"
    i = extra_headers.index("region") + 1
    extra_headers = extra_headers[:i] + ["best target"] + extra_headers[i:]
    fieldnames = ['chrom', 'start', 'end', 'strand', 'length', 'treatment_sum', 'treatment_max', 'control_sum', 'control_max', 'peak_ratio_max', 'treatment_average', 'control_average']

    return peaks_df.reindex(columns=fieldnames + extra_headers)


def post_peak_processing(config, reference, microarray_expression, samples, treatment_name, peaks):
    peaks_df = peaks_to_annotated_data_frame(config, reference, treatment_name, peaks)

    # Write CSV
    peaks_df.to_csv(os.path.join(config.output, "%s.csv" % treatment_name)) # Write csv

    # Differential Expression
    fold_change = expression.load_differential_expression(config, treatment_name)
    expression_dir = os.path.join(config.output, "expression")
    expression.compare_peaks_vs_fold_change(expression_dir, treatment_name, fold_change, peaks_df)

    expression.compare_peaks_vs_expression(expression_dir, treatment_name, microarray_expression, peaks_df)
    sublists.peak_sublists(config, reference, samples, treatment_name, peaks)
    
    return peaks_df

def load_peaks_df(config, treatment_name):
    '''load csv produced by post_peak_processing into dataframe'''
    return pd.read_csv(os.path.join(config.output, "%s.csv" % treatment_name))
        