'''
Created on Jan 15, 2013

@author: dave
'''
from sacgf.hitsclip.peaks.peak import Peak

def find_peaks(peak_min_treatment_depth, peak_min_ratio, peak_uninteresting_cutoff, control, treatment):
    ''' peak_min_treatment_depth : int
        peak_min_ratio : int
        peak_uninteresting_cutoff : int, number of bases where previous 2 thresholds are not reached for a peak to end
        control, treatment : HTSeq.GenomicArray of depths
        returns: list of dicts of stats (Peak.get_peak_info()) on peaks'''
    peaks = []

    for (_, strand) in treatment.chrom_vectors.items():
        for (_, vec) in strand.items():
            peak = None
            for (iv, value) in vec.steps():
                un_interesting_bases = 0
                for (ctr_interval, ctr_value) in control[iv].steps():
                    if is_interesting(peak_min_treatment_depth, peak_min_ratio, ctr_value, value):
                        un_interesting_bases = 0
                        p = Peak(iv, value, ctr_value)
                        if peak == None:
                            peak = p
                        else:
                            peak += p
                    else:
                        if peak:
                            un_interesting_bases += ctr_interval.length
                            if un_interesting_bases >= peak_uninteresting_cutoff:
                                peaks.append(peak)
                                peak = None
                            
    return [p.get_peak_info() for p in peaks]



def is_interesting(peak_min_treatment_depth, peak_min_ratio, control, treatment):
    interesting = False
    if treatment >= peak_min_treatment_depth:
        if control == 0:
            interesting = True
        else:
            ratio = treatment / control
            if ratio >= peak_min_ratio:
                interesting = True
    return interesting