'''

SA Cancer Genomics Facility

Jan 14, 2013 - Initial Version

@author: dlawrence
'''

import logging
import os
from scipy import stats

import pandas as pd
from sacgf.graphing.box_plot import write_box_plot
from sacgf.graphing.scatter_colored_gene_set_graph import \
    write_scatter_colored_gene_set_plot
from sacgf.hitsclip.peaks import best_target
from sacgf.util import file_utils, stats_utils, csv_utils
from sacgf.util.regex_utils import match_strings


headers = ["subset name", "num genes", "mean", "p_value vs all"]
logger = logging.getLogger(__name__)

def load_differential_expression(config, treatment_name):
    microarray_diff_expr_csv = config.micro_array_fold_change_csv_format % treatment_name
    logger.info("Loading differential expression: '%s'", microarray_diff_expr_csv)
    expr_df = pd.DataFrame.from_csv(microarray_diff_expr_csv, index_col="Gene Symbol")

    microarray_diff_expr_column = config.micro_array_fold_change_column_format % treatment_name
    return expr_df[microarray_diff_expr_column]

def load_expression(micro_array_expression_csv, sample_names):
    logger.info("Loading expression: '%s'", micro_array_expression_csv)
    raw_expr_df = pd.DataFrame.from_csv(micro_array_expression_csv, index_col="Gene Symbol")        
    microarray_expression = pd.DataFrame(index=raw_expr_df.index)

    # Average columns?
    for sample in sample_names:
        match = sample
        if sample == 'control':
            match = 'neg'

        pattern = ".*%s.*" % match
        columns = match_strings(pattern, raw_expr_df.columns)
        assert len(columns) > 0, "found a match for %s in %s" % (pattern, raw_expr_df.columns)
        np_arrays = []
        for c in columns:
            np_arrays.append(raw_expr_df[c])

        microarray_expression[sample] = stats_utils.mean_arrays(np_arrays)

    return microarray_expression


class SubGroupComparer(object):
    def __init__(self, out_dir, treatment_name, fold_change, write_csv=None):
        self.out_dir = os.path.join(out_dir, "%s_subgroup_gene_lists" % treatment_name)
        self.treatment_name = treatment_name
        self.fold_change = fold_change
        self.write_csv = write_csv

    def compare(self, name, sub_group_df):
        gene_set = pd.unique(sub_group_df["gene"])
        logger.info("geneset %s size = %d", name, len(gene_set))
        return self.compare_gene_set(name, gene_set)
        
    def compare_gene_set(self, name, gene_set):
        sub_group_fold_change = self.fold_change.ix[ self.fold_change.index.isin(gene_set) ]
        gene_set_comparison_dict = {}
        if len(sub_group_fold_change) > 0:
            if self.write_csv:
                csv_file_name = os.path.join(self.out_dir, "%s_%s.csv" % (self.treatment_name, name))
                file_utils.mk_path_for_file(csv_file_name)
                sub_group_fold_change.to_csv(csv_file_name)
            
            (_, p_value) = stats.ttest_ind(self.fold_change, sub_group_fold_change)
            gene_set_comparison_dict = { k : v for (k, v) in zip(headers, [name, len(sub_group_fold_change), sub_group_fold_change.mean(), p_value])}
        return gene_set_comparison_dict

def compare_peaks_vs_fold_change(out_dir, treatment_name, fold_change, peaks_df):
    graph_image = os.path.join(out_dir, "%s_expression_boxplot.png" % treatment_name)
    title = "Fold change - %s vs control" % treatment_name
    create_sub_groups_fold_change_box_plot(graph_image, title, fold_change, peaks_df)
    
    rows = []
    rows.append({ k : v for (k, v) in zip(headers, ["all", len(fold_change), fold_change.mean(), ""])})

    sub_group_comparer = SubGroupComparer(out_dir, treatment_name, fold_change, True)

    # By Target    
    for t in best_target.TARGET_RANKING_ORDER:
        target_group = peaks_df[ peaks_df["best target"] == t ]
        rows.append(sub_group_comparer.compare(t, target_group))

    # By Peak score
    peak_sets = {"all peaks": peaks_df, "top 10" : peaks_df.ix[:10], "top 100" : peaks_df.ix[:100]}
    for (name, peak_sub_group) in peak_sets.iteritems():
        rows.append(sub_group_comparer.compare(name, peak_sub_group))

    for i in range(4, 10):
        t_max = peaks_df[ peaks_df["treatment_max"] >= i ]
        rows.append(sub_group_comparer.compare("treatment_max >= %d" % i, t_max))

    zero_control_max = peaks_df[ peaks_df["control_max"] == 0 ]
    rows.append(sub_group_comparer.compare("control max = 0", zero_control_max))

    non_zero_control_max = peaks_df[ peaks_df["control_max"] > 0 ]
    rows.append(sub_group_comparer.compare("control max > 0", non_zero_control_max))
        
    long_peaks = peaks_df[ peaks_df["length"] > 20 ]
    rows.append(sub_group_comparer.compare("length > 20", long_peaks))

    short_peaks = peaks_df[ peaks_df["length"] <= 20 ]
    rows.append(sub_group_comparer.compare("length <= 20", short_peaks))

#    # By Peak region
    for region in pd.unique(peaks_df["region"]):
        if len(region) == 0:
            continue
        region_sub_group = peaks_df[ peaks_df["region"] == region ]
        gene_set_comparison_dict = sub_group_comparer.compare(region, region_sub_group)
        if gene_set_comparison_dict:
            rows.append(gene_set_comparison_dict)

    peaks_per_gene = peaks_df["gene"].value_counts()
    for n in [2, 3, 5, 10]:
        n_peaks_per_gene = pd.unique(peaks_per_gene[peaks_per_gene >= n].index)
        rows.append(sub_group_comparer.compare_gene_set("peaks per gene >= %d" % n, n_peaks_per_gene))

    sorted_rows = sorted(rows, key=lambda k : k["mean"])
    
    csv_file = os.path.join(out_dir, "%s_subgroup_expression.csv" % treatment_name)
    csv_utils.write_csv_dict(csv_file, headers, sorted_rows)



def create_sub_groups_fold_change_box_plot(graph_image, title, fold_change, peaks_df):
    logger.info("create_sub_groups_fold_change_box_plot: %s", graph_image)

    data = []
    labels = []

    # ALL
    data.append(fold_change)    
    labels.append("all")

    targets = ["8mer", "7mer"]
    for t in targets:
        target_group = peaks_df[ peaks_df["best target"] == t ]
        gene_set = pd.unique(target_group["gene"])
        data.append(fold_change.ix[ fold_change.index.isin(gene_set) ])
        labels.append(t)

    write_box_plot(graph_image, data, title=title, labels=labels)

def gene_set_from_mask(df, mask):
    sub_set = df[mask]
    return pd.unique(sub_set["gene"])

def compare_peaks_vs_expression(out_dir, treatment_name, microarray_expression, peaks_df):
    colored_gene_sets = {"8mer" : ("#ff0000", gene_set_from_mask(peaks_df, peaks_df["best target"] == "8mer")),
                       "7mer" : ("#ff6d0c", gene_set_from_mask(peaks_df, peaks_df["best target"] == "7mer")),}

    graph_image = os.path.join(out_dir, "%s_targets.png" % treatment_name)
    write_scatter_colored_gene_set_plot(graph_image, microarray_expression, treatment_name, "control", colored_gene_sets)

    peaks_per_gene = peaks_df["gene"].value_counts()
    colored_gene_sets = {">= 10 targs" : ("#ff0000", pd.unique(peaks_per_gene[peaks_per_gene >= 10].index)),}
                        #">= 5 targs" : ("#ff6d0c", pd.unique(peaks_per_gene[peaks_per_gene >= 5].index))}

    graph_image = os.path.join(out_dir, "%s_multiple_targets.png" % treatment_name)
    write_scatter_colored_gene_set_plot(graph_image, microarray_expression, treatment_name, "control", colored_gene_sets)

    

