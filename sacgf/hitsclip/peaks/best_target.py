'''
Created on Jan 11, 2013

@author: dlawrence
'''

import numpy as np

# Worst -> Best (overwrites)
TARGET_RANKING_ORDER = ['7mer-1mm', '8mer-1mm', '6mer', 'cen_pair 2mm', '7mer', '8mer']
BEST_TARGET_COLUMN = "best target"
PEAK_DISTANCE = 150

def add_best_target_column(dataframe):
    dataframe[BEST_TARGET_COLUMN] = "None"
    for t in TARGET_RANKING_ORDER:
        column = dataframe["closest %s distance" % t]
        has_target = np.abs(column) < PEAK_DISTANCE
        dataframe[BEST_TARGET_COLUMN][has_target] = t

    

        
        