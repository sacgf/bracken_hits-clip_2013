'''
Get a sublist of a list of HITS-CLIP peaks, according to various criteria

Created on Nov 7, 2012

@author: dlawrence
'''
import glob
import logging
import os

from sacgf.hitsclip.graphs.individual_gene_graph import do_individual_gene_graph
from sacgf.util import file_utils
from sacgf.util.csv_utils import filter_csv


logger = logging.getLogger(__name__)

def top_transcripts(peaks, column_id, num_top):
    uniq_transcripts = set()
    top_peaks = []
    
    tp = 0
    for p in peaks:
        transcripts = p[column_id].split(" ")
        for tss in transcripts:
            if tp < num_top and tss not in uniq_transcripts:
                uniq_transcripts.add(tss)
                top_peaks.append(tss)
                tp += 1
    return top_peaks  


def create_close_func(column_name, distance):
    def close_to_target_func(p):
        dist_str = p[column_name]
        try:
            return abs(int(dist_str)) < distance
        except ValueError:
            pass
        return False
    return close_to_target_func


def create_graphs(config, reference, samples, transcripts_list, dir1_name, dir2_name):
    out_dir = os.path.join(config.output, "gene_graphs", dir1_name, dir2_name)
    for transcript_id in transcripts_list:
        transcript = reference.transcripts[transcript_id]
        if transcript.is_coding: # Needs a 3'UTR
            min_value = config.peak_min_treatment_depth # Change to 0 to show all

            gene_graph_image = os.path.join(out_dir, "%s_%s.png" % (transcript.get_gene_name(), transcript_id))
            if not os.path.exists(gene_graph_image):
                do_individual_gene_graph(gene_graph_image, reference, min_value, transcript, samples)


def peak_sublists(config, reference, samples, treatment_name, peaks):
    logger.debug("peak_sublists for treatment %s", treatment_name)
    # Calculated sublists
    num_top = 40
    is_3p_utr = lambda p : p["region"].find("3PUTR") != -1
    close_central_paired = create_close_func("closest cen_pair 2mm distance", 50)
#    close_seed_mis_match = create_close_func("closest cen_pair 2mm distance", 50)

    logger.debug("There are %d peaks", len(peaks))

    # Now print out the following:
    peaks_3p_utr = filter_csv(peaks, filter_func=is_3p_utr)

    logger.debug("There are %d 3p_utr peaks", len(peaks_3p_utr))

    peaks_cp = filter_csv(peaks_3p_utr, filter_func=close_central_paired)
    top_cp = top_transcripts(peaks_cp, "cen_pair 2mm transcript", num_top)
    logger.debug("There are %d top central paired peaks", len(top_cp))

    
    name = "top_%d_%s" % (num_top, "central_paired")
    create_graphs(config, reference, samples, top_cp, treatment_name, name)

    close7mer = create_close_func("closest 7mer distance", 50)
    close7mer1mm = create_close_func("closest 7mer-1mm distance", 50)

    def mismatch_func(p):
        return not close7mer(p) and close7mer1mm(p)
    peaks_m_m = filter_csv(peaks_3p_utr, filter_func=mismatch_func)
    top_m_m = top_transcripts(peaks_m_m, "7mer-1mm transcript", num_top)
    name = "top_%d_%s" % (num_top, "mismatches")
    create_graphs(config, reference, samples, top_m_m, treatment_name, name)

    close6mer = create_close_func("closest 6mer distance", 50)

    def orphan_func(p):
        return not (close6mer(p) or close7mer1mm(p)) 
    peaks_orphan = filter_csv(peaks_3p_utr, filter_func=mismatch_func)
    top_orphan = top_transcripts(peaks_orphan, "transcript_id", num_top)
    name = "top_%d_%s" % (num_top, "orphans")
    create_graphs(config, reference, samples, top_orphan, treatment_name, name)


def do_gene_list_graphs(config, reference, samples):
    ''' Generate individual gene graphs for the gene lists in config.gene_graph_lists_dir '''
    logger.info("do_gene_list_graphs")
    
    for gene_list_file in glob.glob(os.path.join(config.gene_graph_lists_dir, "*.txt")):
        name = file_utils.name_from_file_name(gene_list_file)
        logger.debug("do_gene_list_graphs - %s", name)
        gene_list = file_utils.file_to_hash(open(gene_list_file)).keys()
        transcripts_list = []
        for gene_name in gene_list:
            try:
                gene = reference.genes[gene_name]
                # Only use standard chromosomes
                transcripts = filter(lambda t : t.iv.chrom in reference.hs_chromosomes(), gene.transcripts)
                transcript_ids = [t.name for t in transcripts]
                transcripts_list.extend(transcript_ids)
            except KeyError:
                logger.error("Uknown gene '%s'", gene_name)
        create_graphs(config, reference, samples, transcripts_list, "gene_lists", name)

