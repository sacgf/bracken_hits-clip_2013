'''
HITS-CLIP peak (ie enrichment of treatment vs control)

Created on Jun 17, 2012

@author: dlawrence
'''
import HTSeq

class Peak:
    def __init__(self, iv, treatment_value, control_value):
        self.iv = iv
        self.treatment_sum = treatment_value
        self.treatment_max = treatment_value
        self.control_sum = control_value
        self.control_max = control_value
        
        denominator = 0.01 # peaks with no control are multiplied by 10
        if control_value != 0:
            denominator = control_value
        self.peak_ratio_max = treatment_value / denominator
    
    def __iadd__(self, other):
        self.iv.start = min(self.iv.start, other.iv.start)
        self.iv.end = max(self.iv.end, other.iv.end)
        self.treatment_sum += other.treatment_sum
        self.treatment_max = max(self.treatment_max, other.treatment_max)
        self.control_sum += other.control_sum
        self.control_max = max(self.control_max, other.control_max)
        self.peak_ratio_max = max(self.peak_ratio_max, other.peak_ratio_max)
        return self

    def get_peak_info(self):
        peak_info = {
            'chrom'             :   self.iv.chrom,
            'start'             :   self.iv.start,
            'end'               :   self.iv.end,
            'strand'            :   self.iv.strand,
            'length'            :   self.iv.length,
            'treatment_sum'      :   self.treatment_sum,
            'treatment_max'      :   self.treatment_max,
            'treatment_average'  :   self.treatment_sum / self.iv.length,
            'control_sum'        :   self.control_sum,
            'control_max'        :   self.control_max,
            'peak_ratio_max'      :   self.peak_ratio_max,
            'control_average'    :   self.control_sum / self.iv.length,
        }

        return peak_info


def peak_info_to_iv(peak_info):
    return HTSeq.GenomicInterval(peak_info["chrom"], int(peak_info["start"]), int(peak_info["end"]), peak_info["strand"])