"""Core library for HIgh-Throughput Sequencing of RNA isolated by CrossLinking ImmunopreciPitation experiments

    @see http://en.wikipedia.org/wiki/HITS-CLIP
"""
import ConfigParser
import HTSeq
from collections import defaultdict
import csv
import glob
import os

import pandas as pd
from sacgf import genomics, qc, util
from sacgf.genomics import iv_iterators
from sacgf.genomics.reference import Reference
from sacgf.hitsclip.graphs import verification_assay_graphs
from sacgf.hitsclip.graphs.stop_codon_graph import stop_codon_graph_image
from sacgf.hitsclip.peaks import best_target, expression
from sacgf.util import file_utils


class HITSCLIPRegionStats(qc.RegionStats):
    def __init__(self, reference):
        columns = ["name", "total", "5PUTR", "coding", "intron", "3PUTR", "intergenic", "non coding"]
        super(HITSCLIPRegionStats, self).__init__(reference.regions, columns, "intergenic")
    
    def process_tally(self, name, tally):
        tally["total"] = sum(tally.values())
        tally["name"] = name
        

def do_qc(config, reference, samples):
    # Luciferase verification plots
    
    graph_image = os.path.join(config.output, "verification_boxplot.png")
    verification_assay_graphs.write_verification_asssay_box_plots(graph_image, config.validation_assay_data_file)

    reads_qc_dir = os.path.join(config.qc_dir, "reads")

    file_utils.mk_path(reads_qc_dir)
    stop_codon_graph_image(reads_qc_dir, reference, samples)

    out_dir = os.path.join(config.output, "regions")    
    region_stats_csv = os.path.join(out_dir, "region_stats.csv")

    if not os.path.exists(region_stats_csv):
        region_stats = HITSCLIPRegionStats(reference)
    
        def midpoint_iterator(iv_iterator):
            for iv in iv_iterator:
                yield genomics.mid_point(iv)
    
        for (sample_name, sample_data) in samples.iteritems():
            iv_iterator = iv_iterators.load_iv_iterator(sample_data.file_name)
            region_stats.add_sample_pos_array(sample_name, midpoint_iterator(iv_iterator))
    
        region_stats.write_csv(region_stats_csv)
    

def get_full_file_name(config, name):
    return os.path.join(config.aligned_reads_dir, config.aligned_reads_pattern % name)


def load_sample(chrom_dao, config, name, before_distance, after_distance):
    file_name = get_full_file_name(config, name)
    if not os.path.exists(file_name):
        raise ValueError("Could not open '%s'" % file_name)
    else:
        print "load_sample: %s" % file_name
    
    iterator = chrom_dao.load_iv_iterator(file_name)
    fixed_length_iterator = iv_iterators.fixed_length_iv_iterator(iterator, before_distance, after_distance)
    array = genomics.create_coverage_array(fixed_length_iterator)
    return util.Struct(file_name=file_name, array=array)


def load_samples(config, chrom_dao):
    samples = {}
    samples["control"] = load_sample(chrom_dao, config, config.control, config.control_read_before, config.control_read_after)
    for treatment_name in config.treatments:
        samples[treatment_name] = load_sample(chrom_dao, config, treatment_name, config.treatment_read_before, config.treatment_read_after)
    return samples