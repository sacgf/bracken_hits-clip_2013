'''
Plot graphs of verification assays (Luciferase/QPCR) for HITS-CLIP experiment

Created on Dec 12, 2012

@author: dlawrence
'''

from matplotlib import pyplot
from scipy import stats
import logging
import numpy as np
import pandas

logger = logging.getLogger(__name__)

def write_verification_asssay_scatterplots(graph_image, data_csv):
    scores = {"8mer" : 1.0, "7mer" : 0.73, "CP" : 0.45, "6mer" : 0.27, "8MM" : 0.18, "7MM" : 0.12, "0" : 0, np.NaN : 0}
    validation_data = pandas.read_csv(data_csv)
    pyplot.figure()

    sub_plot_id = 0
    for mir in ["miR-200a", "miR-200b"]:
        target_scores = [scores[s] for s in validation_data["Best target %s" % mir]]

        y = validation_data["Peak score %s" % mir]
        for experiment in ["Luciferase", "QPCR"]:
            sub_plot_id += 1
            
            name = "%s %s" % (experiment, mir)
            logger.debug("Subplot %d = %s", sub_plot_id, name)
            x = validation_data["%s scr" % experiment] - validation_data[name]

            pyplot.subplot(2, 2, sub_plot_id)
            pyplot.title(name)
            pyplot.scatter(x, y, c=target_scores, cmap=pyplot.cm.jet) #@UndefinedVariable

    pyplot.savefig(graph_image)
    pyplot.close()


def filter_no_match(x):
    '''Where "0", replace with "None"'''
    no_match_name = "None"
    return x.replace("0", no_match_name).fillna(no_match_name)

def get_named_target_data(experiment, mir, validation_data, target):
    '''Returns: pd.Series(index=gene_ids, values=validation experiment values)'''
    target_rows_mask = filter_no_match(validation_data["Best target %s" % mir]) == target #Get boolean mask for rows to select rows which are the right seed match type for that mir???
    experiment_data = validation_data[target_rows_mask]["%s %s" % (experiment, mir)] #Select data for that miR/seed match type by applying mask and selecting correct col for that expt
    
    return experiment_data

def get_experiment_data_grouped_by_target(experiment, mirs, targets, validation_data):
    '''
    Formats and returns the data for the specified experiment, pooling together data from mirs.
        experiment: A validation experiment name (str, e.g. "Luciferase")
        mirs: list of miR names (str, e.g. "miR-200a")
        targets: list of target types (str, e.g. "8mer")
        validation_data: csv of data from the validation experiment
    Returns: dict of {target_type: list of values(float) from all mirs for that expt and target}
    '''
    data = {}
    for target in targets:
        target_arrays = []
        for mir in mirs:
            experiment_data = get_named_target_data(experiment, mir, validation_data, target)
            target_arrays.append(experiment_data.values)
        
        data[target] = np.concatenate(target_arrays)
    return data

EXPERIMENTS = ["Luciferase", "Luciferase P-value"]#, "QPCR"]
MIRNAS = ["miR-200a", "miR-200b"]
TARGETS = ["None", "7MM", "8MM", "6mer", "7mer", "8mer", "CP"]

def write_verification_asssay_box_plots(graph_image, data_csv):
    '''
    get targets 
    luc data and qpcr, 
    go through peaks getting target types
    group target types together
    data_csv = csv of validation data (e.g. Luciferase) 
    '''
    validation_data = pandas.read_csv(data_csv)
    pyplot.figure()
    sub_plot_id = 0
    for experiment in EXPERIMENTS:
        all_targets_data = get_experiment_data_grouped_by_target(experiment, MIRNAS, TARGETS, validation_data)

        control = all_targets_data["None"]
        data = []
        for target in TARGETS:
            target_data = all_targets_data[target]
            logger.debug("%s t-test vs normal = %s", target, stats.ttest_ind(control, target_data))
            data.append(target_data)

        sub_plot_id += 1
        pyplot.subplot(len(EXPERIMENTS), 1, sub_plot_id)
        pyplot.title(experiment)
        
        pyplot.boxplot(data)
        pyplot.setp(pyplot.gca(), 'xticklabels', TARGETS) 

    pyplot.savefig(graph_image)
    pyplot.close()

