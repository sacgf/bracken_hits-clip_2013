'''
Plot a graph of HITS-CLIP reads vs stop codon and poly-A tail of known genes

Created on Nov 7, 2012

@author: dlawrence
'''

import abc
import logging
from matplotlib import pyplot
import numpy
import os

from sacgf import genomics, graphing
from sacgf.genomics import iv_iterators
from sacgf.graphing import stacked_bar_graph
from sacgf.graphing.matplotlib_utils import MultiplyingLabelFormatter
from sacgf.util import stats_utils, file_utils


logger = logging.getLogger(__name__)

class RegionsHistogramCounter(object):
    def __init__(self, reference, lower_bounds):
        self.reference = reference
        self.lower_bounds = lower_bounds
        self.regions = []
        self.extra_rescued = 0

    def add_region(self, region):
        self.regions.append(region)

    def count(self, g_pos):
        transcript = self.reference.get_longest_coding_transcript(g_pos)
        if transcript:
            m_pos = transcript.get_transcript_position(g_pos)
            # Get stop codon pos in outer loop (even though not all calls need it)
            # So that a failure will affect all regions evenly 
            stop_codon_pos = transcript.get_stop_codon_position()
            transcript_length = transcript.get_transcript_length()
            
            for region in self.regions:
                region.handle(m_pos, stop_codon_pos, transcript_length)


class Region(object):
    __metaclass__ = abc.ABCMeta
    
    def __init__(self, lower_bounds, start, stop):
        self.lower_bounds = lower_bounds
        self.start = start
        self.stop = stop
        self.graph_size = stop - start
        assert self.graph_size > 0

        self.arrays = [numpy.zeros(self.graph_size, dtype='i') for _ in self.lower_bounds]

    def get_region_length_index(self, region_length):
        for (i, l) in enumerate(self.lower_bounds):
            if region_length >= l:
                return i
        return -1

    @abc.abstractmethod
    def get_region_length(self, stop_codon_pos, transcript_length):
        return

    @abc.abstractmethod
    def get_graph_position(self, m_pos, stop_codon_pos, transcript_length):
        return
    
    def handle(self, m_pos, stop_codon_pos, transcript_length):
        p = self.get_graph_position(m_pos, stop_codon_pos, transcript_length)
        if p >= 0 and p < self.graph_size:
            region_length = self.get_region_length(stop_codon_pos, transcript_length)
            i = self.get_region_length_index(region_length)
            array = self.arrays[i]
            array[p] += 1

    def get_l_args(self):
        return numpy.arange(self.start, self.stop)

class BeforeStopCodon(Region):
    def get_region_length(self, stop_codon_pos, transcript_length):
        return stop_codon_pos
        
    def get_graph_position(self, m_pos, stop_codon_pos, transcript_length):
        dist_from_stop = stop_codon_pos - m_pos
        return self.graph_size - dist_from_stop

class ThreePStart(Region):
    def get_region_length(self, stop_codon_pos, transcript_length):
        return transcript_length - stop_codon_pos
        
    def get_graph_position(self, m_pos, stop_codon_pos, transcript_length):
        return m_pos - stop_codon_pos

class ThreePEnd(Region):
    def get_region_length(self, stop_codon_pos, transcript_length):
        return transcript_length - stop_codon_pos
        
    def get_graph_position(self, m_pos, stop_codon_pos, transcript_length):
        if m_pos < stop_codon_pos: # no longer in 3p_utr
            return -1 # Will not end up on graph
        distance_from_end = transcript_length - m_pos
        return self.graph_size - distance_from_end

def get_labels_from_lower_bounds(lower_bounds):
    labels = []
    upper_bound = None
    for lower_bound in lower_bounds:
        if upper_bound is None:
            labels.append(">%d" % lower_bound)
        else:
            labels.append("%d-%d" % (lower_bound, upper_bound))
        upper_bound = lower_bound
    return labels


def stop_codon_graph_image(out_dir, reference, samples):
    lower_bounds = [1500, 1000, 500, 200, 0]
    before_stop_codon_region = BeforeStopCodon(lower_bounds, -2000, 0)
    three_p_start_region = ThreePStart(lower_bounds, 0, 2000)
    three_p_end_region = ThreePEnd(lower_bounds, -2000, 0)
    
    counter = RegionsHistogramCounter(reference, lower_bounds)
    counter.add_region(before_stop_codon_region)
    counter.add_region(three_p_start_region)
    counter.add_region(three_p_end_region)
    
    for sample_data in samples.itervalues():
        file_name = sample_data.file_name
        iterator = iv_iterators.load_iv_iterator(file_name)
        for iv in iterator:
            g_pos = genomics.read_start(iv)
            counter.count(g_pos)

    labels = get_labels_from_lower_bounds(lower_bounds)

    before_stop_codon = (before_stop_codon_region.get_l_args(), before_stop_codon_region.arrays)
    three_p_start = (three_p_start_region.get_l_args(), three_p_start_region.arrays)
    three_p_end = (three_p_end_region.get_l_args(), three_p_end_region.arrays)

    write_graphs(out_dir, labels, before_stop_codon, three_p_start, three_p_end)


def write_component_graphs(out_dir, graph_type, labels, colors, graph_data, **kwargs):
    extra_func = kwargs.get("extra_func")
    file_name = os.path.join(out_dir, "%s.png" % graph_type) 
    graphing.write_stacked_bar_graph(file_name, graph_data[0], graph_data[1], labels, colors, extra_func=extra_func)
    largs = graph_data[0]
    for (label, profile) in zip(labels, graph_data[1]):
        sub_group_image_name = os.path.join(out_dir, "%s_%s.png" % (graph_type, label)) 
        graphing.write_graph(sub_group_image_name, largs, profile, extra_func=extra_func)

def write_graphs(out_dir, labels, before_stop_codon, three_p_start, three_p_end):
    
    # Normal
    file_name = os.path.join(out_dir, "three_panel_graph.png")
    colors = ["red", "blue", "green", "yellow", "pink"]

    write_component_graphs(out_dir, "before_stop_codon", labels, colors, before_stop_codon)
    write_component_graphs(out_dir, "three_p_stat", labels, colors, three_p_start)
    write_component_graphs(out_dir, "three_p_end", labels, colors, three_p_end)
    
    write_three_panel_graph(file_name, labels, colors, before_stop_codon, three_p_start, three_p_end)

    for nt_per_pixel in [20, 50]:
        packed_str = "%d_nt_per_box" % nt_per_pixel
        file_name = os.path.join(out_dir, "three_panel_graph_%s.png"  % packed_str)
        
        packed_before_stop_codon = pack_l_args_and_arrays(before_stop_codon[0], before_stop_codon[1], nt_per_pixel)
        packed_three_p_start = pack_l_args_and_arrays(three_p_start[0], three_p_start[1], nt_per_pixel)
        packed_three_p_end = pack_l_args_and_arrays(three_p_end[0], three_p_end[1], nt_per_pixel)

        def change_x_labels(pyplot, fig, lines):
            ax = pyplot.gca()
            ax.xaxis.set_major_formatter(MultiplyingLabelFormatter(nt_per_pixel))

        write_component_graphs(out_dir, "before_stop_codon_%s" % packed_str, labels, colors, packed_before_stop_codon, extra_func=change_x_labels)
        write_component_graphs(out_dir, "three_p_stat_%s" % packed_str, labels, colors, packed_three_p_start, extra_func=change_x_labels)
        write_component_graphs(out_dir, "three_p_end_%s" % packed_str, labels, colors, packed_three_p_end, extra_func=change_x_labels)

        write_three_panel_graph(file_name, labels, colors, packed_before_stop_codon, packed_three_p_start, packed_three_p_end, extra_func=change_x_labels)
    

def pack_l_args_and_arrays(largs, arrays, nt_per_pixel):
    packed_array = [stats_utils.pack_into_boxes(np_array, nt_per_pixel) for np_array in arrays]
    (range_start, range_end) = (largs[0], largs[-1] + 1)
    packed_l_args = numpy.arange(range_start / nt_per_pixel, range_end / nt_per_pixel)
    return (packed_l_args, packed_array)



def write_three_panel_graph(graph_image, labels, colors, before_stop_codon, three_p_start, three_p_end, **kwargs):
    extra_func = kwargs.get("extra_func")
    logger.info("write_three_panel_graph: %s", graph_image)
    
    plot_data = [
        { "largs" : before_stop_codon[0], "arrays" : before_stop_codon[1], "loc" : "upper left", "legend_title" : "mrna length"},
        { "largs" : three_p_start[0],     "arrays" : three_p_start[1], "loc" : "upper right", "legend_title" : "3' UTR length"},
        { "largs" : three_p_end[0],       "arrays" : three_p_end[1], "loc" : "upper left", "legend_title" : "3' UTR length"},
    ]

    ymax = 0
    for pd in plot_data:
        total = sum(pd["arrays"])
        ymax = max(ymax, numpy.amax(total))
    ymax *= 1.1 # 10% padding
    
    legend_props = {"size": 6}

    fig = pyplot.figure(figsize=(8, 6))
    shared_y_axis = None
    for (subplot_id, pd) in enumerate(plot_data):
        if shared_y_axis is None:
            shared_y_axis = pyplot.subplot(1, 3, subplot_id+1)
        else:
            ax = pyplot.subplot(1, 3, subplot_id+1, sharey=shared_y_axis)
            pyplot.setp( ax.get_yticklabels(), visible=False)
        largs = pd["largs"]
        lines = stacked_bar_graph(largs, pd["arrays"], labels, colors)
        pyplot.xlim(xmin=largs[0], xmax=largs[-1])
        pyplot.ylim(ymin=0, ymax=ymax)
        pyplot.legend(loc=pd["loc"], title=pd["legend_title"], prop=legend_props)

        if extra_func:
            extra_func(pyplot, fig, lines)

    pyplot.tight_layout(w_pad=0)

    file_utils.mk_path_for_file(graph_image)
    pyplot.savefig(graph_image, dpi=300)
    pyplot.close()
