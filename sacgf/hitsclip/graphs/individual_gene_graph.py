'''
Plots the 3'UTR of a mrna transcript along with HITS-CLIP peaks and mirna seed matches

The x axis is in mrna coordinates (ie exons pasted together)

Created on Nov 7, 2012

@author: dlawrence
'''

import HTSeq
import logging
from matplotlib import pyplot
import matplotlib
from matplotlib.colors import ColorConverter
import numpy

from sacgf import genomics
from sacgf.graphing.graph_symbols import graph_symbols_factory
from sacgf.mirna.seed_match import SeedMatch
from sacgf.util import file_utils


matplotlib.use('agg', warn=False)

# TODO: Pass in config or do this somehow other than hardcoded
def get_extra3p_utr_length(transcript_id):
    hardcoded_extra_transcript_length = {"NM_004850" : 1000, "NM_005228" :  5000}
    return hardcoded_extra_transcript_length.get(transcript_id, 0)

def in_any_other_sample(samples, iv, peak_min_value):
    for sample_data in samples.itervalues():
        for (_, value) in sample_data.array[iv].steps():
            if value >= peak_min_value:
                return True
    return False


def do_individual_gene_graph(gene_graph_image, reference, peak_min_value, transcript, samples):
    '''
    peak_min_value: (Use 0 to show all)
    '''
    
    logger = logging.getLogger(__name__)

    gene_name = transcript.get_gene_name()
    transcript_id = transcript.get_id()

    get_transcript_pos = lambda iv : transcript.get_transcript_positions(iv)
    three_p_utr_features = transcript.get_features("3PUTR")
    graph_features = []
    for feature in three_p_utr_features:
        graph_features.append((feature, get_transcript_pos))
        
    three_p_utr_length = sum([f.iv.length for f in three_p_utr_features])
    extra_length = get_extra3p_utr_length(transcript_id)
    if extra_length:
        logger.info("Adding extra %d bases to %s (original end = %d)", extra_length, transcript_id, three_p_utr_length)
        iv = genomics.GenomicInterval_from_directional(transcript.iv.chrom, transcript.iv.end_d, extra_length, transcript.iv.strand)
        tiv = transcript.iv

        
        def get_extended_3putr_pos(iv):
            transcript_length = transcript.get_transcript_length()
            start_mpos   = transcript_length + abs(iv.start_d - tiv.end_d)
            end_mpos     = transcript_length + abs(iv.end_d - tiv.end_d)
            return (start_mpos, end_mpos)
        
        extended_feature = (HTSeq.GenomicFeature("extended_3putr", "extended_3putr", iv), get_extended_3putr_pos)
        graph_features.append(extended_feature)
    
    sequence = str(reference.get_sequence_from_features([f[0] for f in graph_features]))
    graph_length = len(sequence)

    color = { "200a" : "r", "200b" : "g", "control" : "b" }
    seeds = {"200a" : "hsa-miR-200a-3p", "200b" : "hsa-miR-200b-3p" }

    plots = {}    
    max_value = 0
    
    for (sample_name, sample_data) in samples.iteritems():
        depth_array = numpy.zeros( graph_length, dtype='i' )

        # Offset is the END of the stop codon, so move up 2 bases
        offset = transcript.get_stop_codon_position() + 2
        for (feature, get_graph_pos) in graph_features:
            for (iv, value) in sample_data.array[feature.iv].steps():
                (s_pos, e_pos) = get_graph_pos(iv)
                if value >= peak_min_value or in_any_other_sample(samples, iv, peak_min_value):
                    depth_array[s_pos - offset:e_pos - offset + 1] = value

                max_value = max(max_value, value)
                
        plots[sample_name] = { "array" : depth_array, "fmt" : color[sample_name] }
    
    fig = pyplot.figure()
    title = "%s (%s %s) 3p_utr length %d" % (gene_name, transcript_id, str(transcript.iv), three_p_utr_length)
    fig.suptitle(title, y=0.995)

    largs = numpy.arange( 0, graph_length )
    subplot = 1

    for (sample_name, profile_data) in sorted(plots.iteritems()):
        pyplot.subplot(3, 1, subplot)
        pyplot.title(sample_name)
        pyplot.ylabel("Depth")

        subplot += 1
        array = profile_data["array"]
        lines = pyplot.plot(largs, array, profile_data["fmt"], label=sample_name)
        if extra_length > 0:
            pyplot.axvline(three_p_utr_length)

        x = numpy.array(range(0, len(array)))
        x_axis = numpy.zeros(len(array))
        pyplot.fill_between(x, array, x_axis, facecolor=profile_data["fmt"])
        pyplot.ylim(0, max(max_value * 1.2, max_value + 2))
        pyplot.setp(lines, linewidth=0.4)

        # Also plot the lines...
        mir_name = seeds.get(sample_name)
        if mir_name:
            mirna = reference.get_mirna(seeds[sample_name])
            match_symbol_list = create_match_symbol_list(sequence, mirna)
            for match_symbol in match_symbol_list:
                match_symbol.draw(pyplot, max_value)

    pyplot.xlabel("Distance from stop codon")
    pyplot.tight_layout()
    
    #    pyplot.legend(loc='upper center', bbox_to_anchor=(0.5, -0.1),
    #              fancybox=True, shadow=True, ncol=3)
            
    file_utils.mk_path_for_file(gene_graph_image)
    pyplot.savefig(gene_graph_image)
    pyplot.close()


def get_match_symbols(sequence, seed_match_types):
    match_symbol_list = []
    cc = ColorConverter()

    for sm in seed_match_types:
        seed_match = sm["seed_match"]
        matches = seed_match.find(sequence)
        positions = [int(m.start(0) + seed_match.sub_seed_length / 2) for m in matches]
        if len(positions) > 0:
            symbol = sm.get("symbol")
            color = cc.to_rgba(sm["color"], sm.get("alpha", 1.0))
            thickness = sm.get("thickness", .1)
            match_symbol = graph_symbols_factory(symbol, color, positions, thickness)
            match_symbol_list.append(match_symbol)
    
    return match_symbol_list

def create_match_symbol_list(sequence, mir):
    seed = mir.get8mer_target()

    # In reverse order of importance, so will just use painter's algorithm to make important ones on top
    seed_match_types = [
        {"seed_match" : SeedMatch(seed, sub_seed_length=6, mis_matches=1),              "color" : 'y', "symbol" : '|', "thickness" : .1},
        {"seed_match" : SeedMatch(seed, sub_seed_length=6),                            "color" : 'y'},
        {"seed_match" : SeedMatch(seed, sub_seed_length=7, mis_matches=1),              "color" : 'm', "symbol" : '|', "thickness" : .2},
        {"seed_match" : SeedMatch(seed, sub_seed_length=7),                            "color" : 'm'},
        {"seed_match" : SeedMatch(seed, mis_matches=1),                               "color" : 'k', "symbol" : '|', "thickness" : .3},
        {"seed_match" : SeedMatch(seed),                                             "color" : 'k'},
    ]
    match_symbols = get_match_symbols(sequence, seed_match_types)

    central_paired_matches = [
        {"seed_match" : SeedMatch(mir.get_central_paired_target(), mis_matches=2),   "alpha" : 0.7, "color" : 'b', "symbol" : '*'},
    ]
    match_symbols.extend(get_match_symbols(sequence, central_paired_matches))

    return match_symbols


