'''

HITS-CLIP specific configuration

@see sacgf.util.Config

Created on Nov 4, 2012

@author: dlawrence
'''

import os

from sacgf.util import config, file_utils
from sacgf.util.file_utils import name_from_file_name


class HITSCLIPConfig(config.Config):
    def __init__(self, data_dir, igenomes_dir, cfg_file_name):
        output_base_dir = os.path.join(file_utils.base_project_dirname(), "output")
        defaults = {"output.base.dir" : output_base_dir}

        super(HITSCLIPConfig, self).__init__(data_dir, igenomes_dir, cfg_file_name, defaults=defaults)
        self.config_dir = os.path.join(file_utils.base_project_dirname(), "config")
        cfg = self.cfg

        self.output = os.path.join(output_base_dir, name_from_file_name(cfg_file_name))
        self.graph_dir = os.path.join(self.output, "graph")
        self.qc_dir = os.path.join(self.output, "qc")
        self.gene_graph_lists_dir = os.path.join(self.config_dir, "gene_graph_lists")

        self.log_level = cfg.get("general", "log.level")
        self.log_file = os.path.join(self.output, "log.txt")

        # BAM also works...
        self.input_file_type = cfg.get("data", "input.file.type")
        self.aligned_reads_dir = cfg.get("data", "aligned.reads.dir")
        self.aligned_reads_pattern = cfg.get("data", "aligned.reads.pattern", raw=True)
        
        self.treatments = cfg.get("data", "treatments").split(",")
        self.control = cfg.get("data", "control")
        self.validation_assay_data_file = os.path.join(self.config_dir, cfg.get("data", "validation.assay.data.file"))

        micro_array_expression_dir = cfg.get("data", "microarray.expression.dir")
        micro_array_fold_change_csv_format = cfg.get("data", "microarray.fold_change.csv.format", raw=True)
        self.micro_array_fold_change_csv_format = os.path.join(micro_array_expression_dir, micro_array_fold_change_csv_format)
        self.micro_array_fold_change_column_format = cfg.get("data", "microarray.fold_change.column.format", raw=True)
        self.micro_array_expression_csv = cfg.get("data", "microarray.expression.csv")


        self.control_read_before = cfg.getint("peaks", "control.read.before")
        self.control_read_after = cfg.getint("peaks", "control.read.after")
        self.treatment_read_before = cfg.getint("peaks", "treatment.read.before")
        self.treatment_read_after = cfg.getint("peaks", "treatment.read.after")    
        self.peak_min_treatment_depth = cfg.getint("peaks", "min.treatment.depth")
        self.peak_min_ratio = cfg.getfloat("peaks", "min.ratio")
        self.peak_uninteresting_cutoff = cfg.getint("peaks", "uninteresting.cutoff")

        create_dirs = [self.output]
        for d in create_dirs:
            file_utils.mk_path(d)
