'''
MiRNA seed matching using regular expressions

Created on Jun 21, 2012

@author: dlawrence
'''
from collections import OrderedDict
import re

from sacgf import genomics
from sacgf.util.regex_utils import generate_mis_matches


def generate_insertions(sub_seed_seq):
    insertions_set = set()
    for i in range(1, len(sub_seed_seq)-1): # Only do in between (not start/end)
        ins_list = list(sub_seed_seq)
        ins_list.insert(i, '.')
        new_str = ''.join(ins_list)
        insertions_set.add(new_str)
    return insertions_set

def generate_deletions(sub_seed_seq):
    deletions_set = set()
    orig_list = list(sub_seed_seq)
    for i in range(1, len(sub_seed_seq)-1): # Only do in between (not start/end)
        new_str = ''.join(orig_list[:i] + orig_list[i+1:])
        deletions_set.add(new_str)
    return deletions_set

def generate_search_string(seed_sequence, sub_seed_length, generate_sub_sequences):
    seed_seqs = set()

    for subsequence in genomics.get_subsequences_set(seed_sequence, sub_seed_length):
        seed_seqs.update(generate_sub_sequences(subsequence))

    return '|'.join(seed_seqs)

class Matcher(object):
    def __init__(self, search_string, **kwargs):
        self.search_string = search_string
        self.name = kwargs.get("name")
        self.pattern = search_string
        self.regex = re.compile(search_string)
        
    def find(self, sequence):
        return self.regex.finditer(sequence)

    def get_pattern(self):
        return self.pattern

class SeedMatch(Matcher):
    def __init__(self, seed_sequence, **kwargs):
        sub_seed_length   = kwargs.get("sub_seed_length", len(seed_sequence))
        num_mis_matches      = kwargs.get("mis_matches", 0)
        generate_sub_sequences = lambda s : generate_mis_matches(s, num_mis_matches)
        self.sub_seed_length = sub_seed_length
        search_string = generate_search_string(seed_sequence, sub_seed_length, generate_sub_sequences)
        super(SeedMatch, self).__init__(search_string, **kwargs)

class SeedNonExactMatch(Matcher):
    def __init__(self, seed_sequence, num_mis_matches, **kwargs):
        sub_seed_length   = kwargs.get("sub_seed_length", len(seed_sequence))
        generate_sub_sequences = lambda s : genomics.generate_non_exact_mis_matches(s, num_mis_matches)
        self.sub_seed_length = sub_seed_length
        search_string = generate_search_string(seed_sequence, sub_seed_length, generate_sub_sequences)
        super(SeedNonExactMatch, self).__init__(search_string, **kwargs)

class SeedInsertionMatch(Matcher):
    def __init__(self, seed_sequence, **kwargs):
        sub_seed_length = kwargs.get("sub_seed_length", len(seed_sequence))
        self.sub_seed_length = sub_seed_length
        search_string = generate_search_string(seed_sequence, sub_seed_length, generate_insertions)
        super(SeedInsertionMatch, self).__init__(search_string, **kwargs)
        
class SeedDeletionMatch(Matcher):
    def __init__(self, seed_sequence, **kwargs):
        sub_seed_length = kwargs.get("sub_seed_length", len(seed_sequence))
        self.sub_seed_length = sub_seed_length
        search_string = generate_search_string(seed_sequence, sub_seed_length, generate_deletions)
        super(SeedDeletionMatch, self).__init__(search_string, **kwargs)


def get_standard_seed_match_types(mir):
    '''mir: MiRNA instance
    Returns Ordered dict of {seed_match_type: SeedMatch-er}'''
    targ8mer = mir.get8mer_target()
    targ_c_p = mir.get_central_paired_target()
    seed_match_types = OrderedDict()
    
    seed_match_types["6mer"] = SeedMatch(targ8mer, sub_seed_length=6, name="6mer")
    seed_match_types["7mer-1mm"] = SeedNonExactMatch(targ8mer, 1, sub_seed_length=7, name="7mer-1mm")
    seed_match_types["7mer"] = SeedMatch(targ8mer, sub_seed_length=7, name="7mer")
    seed_match_types["8mer-1mm"] = SeedNonExactMatch(targ8mer, 1, name="8mer-1mm")
    seed_match_types["8mer"] = SeedMatch(targ8mer, name="8mer")
    seed_match_types["cen_pair 1mm"] = SeedNonExactMatch(targ_c_p, 1, name="cen_pair 1mm")
    seed_match_types["cen_pair 2mm"] = SeedNonExactMatch(targ_c_p, 2, name="cen_pair 2mm")
    seed_match_types["insertion"] = SeedInsertionMatch(targ8mer, name="insertion")
    seed_match_types["deletion"] = SeedDeletionMatch(targ8mer, name="deletion")
    seed_match_types["cen_pair_ins"] = SeedInsertionMatch(targ_c_p, name="cen_pair_ins")
    seed_match_types["cen_pair_del"] = SeedDeletionMatch(targ_c_p, name="cen_pair_del")
    
    return seed_match_types
    
        
