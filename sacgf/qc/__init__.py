"""Quality Control"""

import HTSeq
from collections import defaultdict
import csv
import logging
import numpy
import os
import sys

import pandas as pd
from sacgf.util import file_utils


class RegionStats(object):
    '''Holds data, can produce stats on the distribution of the data over the regions
    regions_array = genomic array'''
    def __init__(self, region_array, columns, empty_key=None):
        '''columns = regions_array.keys + empty key (if supplied)
        empty key = Label for data which do not fall within a named region in regions_array (str)'''
        self.region_array = region_array
        self.empty_key = empty_key
        self.columns = columns
        self.samples = []

    def count_regions(self, region_array, genomic_array, empty_key=None):
        return count_regions(region_array, genomic_array, empty_key) #uses general count_regions fn

    def count_region_positions(self, region_array, array, empty_key=None):
        return count_region_positions(region_array, array, empty_key)

    def process_tally(self, name, tally):
        pass

    def add_sample(self, name, genomic_array):
        '''
        Calculate stats and append stats (as dict) to list of self.samples
        '''
        tally = self.count_regions(self.region_array, genomic_array, self.empty_key)
        self.process_tally(name, tally)
        self.samples.append(tally)
        
    def add_sample_pos_array(self, name, array):
        tally = self.count_region_positions(self.region_array, array, self.empty_key)
        self.process_tally(name, tally)
        self.samples.append(tally)        

    def write_csv(self, csv_file_name):
        file_utils.mk_path_for_file(csv_file_name)
        writer = csv.DictWriter(open(csv_file_name, "w"), self.columns)
        writer.writerow(dict(zip(self.columns, self.columns)))
        writer.writerows(self.samples)

    def get_dataframe(self):
        '''get data frame of tallies for all samples'''
        df = pd.DataFrame(self.samples, columns=self.columns)
        df = df.set_index(self.columns[0])
        return df
        

# TODO: rewrite to use Counter
def count_regions(region_array, genomic_array, empty_key=None):
    '''Counts the number of features in each region
    Returns dict: keys=regions, values=counts per region'''
    tally = defaultdict(lambda : 0)

    for (iv, value) in genomic_array.steps():
        region = empty_key
        if value:
            for (_, ref_value) in region_array[iv].steps():
                if ref_value:
                    region = ref_value
                    if region == "exon":
                        break; # otherwise keep searching...
            tally[region] += 1
    return tally

def count_region_positions(region_array, array, empty_key=None):
    tally = defaultdict(lambda : 0)

    for g_pos in array:
        region = empty_key
        ref_value = region_array[g_pos]
        if ref_value:
            region = ref_value
            if region == "exon":
                break; # otherwise keep searching...
        tally[region] += 1
    return tally
