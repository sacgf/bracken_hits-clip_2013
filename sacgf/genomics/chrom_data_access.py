#!/usr/bin/env python
'''
ACRF Cancer Genomics Facility

Created on 24/09/2013

@author: dlawrence
'''
from sacgf import genomics
from sacgf.genomics import iv_iterators, BAM_Reader_format_chrom, \
    SAM_Reader_format_chrom
from sacgf.genomics.bed_file import BedFileReader


class ChromDAO(object):
    def __init__(self, want_chr):
        self.want_chr = want_chr
    
    def BAM_Reader(self, bam_file_name):
        return BAM_Reader_format_chrom(bam_file_name, self.want_chr)

    def SAM_Reader(self, sam_file_name):
        return SAM_Reader_format_chrom(sam_file_name, self.want_chr)

    def BED_Reader(self, bed_file_name, delimiter=None):
        kwargs = {'delimiter' : delimiter}
        for feature in BedFileReader(bed_file_name, **kwargs):
            feature.iv.chrom = genomics.format_chrom(feature.iv.chrom, self.want_chr)
            yield feature
        
    def load_iv_iterator(self, file_name):
        iterator = iv_iterators.load_iv_iterator(file_name)
        return iv_iterators.format_chrom_iterator(iterator, want_chr=self.want_chr)
