'''
Read/Write BED files

Bed files are a tab delimited text file used for storing genomic ranges.

chrom    start    end    [name    score    strand    etc]

@see: http://genome.ucsc.edu/faq/faqformat.html#format1
'''
import HTSeq
from itertools import izip
import logging
import os
import sys

import pandas as pd
from sacgf import genomics
from sacgf.util import file_utils


REQUIRED_FIELDS = ['chrom', 'chrom_start', 'chrom_end']
OPTIONAL_FIELDS = ['name', 'score', 'strand']
THICK_ATTRIBUTES = ['thick_start', 'thick_end']
RGB_ATTRIBUTE = ['item_rgb']
BLOCK_ATTRIBUTES = ['block_count', 'block_sizes', 'block_starts']
OPTIONAL_ATTRIBUTES = THICK_ATTRIBUTES + RGB_ATTRIBUTE + BLOCK_ATTRIBUTES
ALL_OPTIONAL = OPTIONAL_FIELDS + OPTIONAL_ATTRIBUTES
BED_COLUMNS = REQUIRED_FIELDS + ALL_OPTIONAL

TYPE_CONVERSIONS = {'chrom_start' : int,
                    'chrom_end': int,
                    'score' : float,
                    'thick_start' : int,
                    'thick_end' : int,
                    'block_count' : int,
                    'block_sizes' : str,
                    'block_starts' : str}

class BedFileReader(object):
    '''
    Reads bed file
    Returns - iterator of HTSeq Genomic Features
    Attribute fields (column 7-12: 'thick_start', 'thick_end', 'block_count', 'block_sizes', 'block_starts')
        are saved as dict in feature attribute 'attr'. e.g. feature.attr['thick_start']
    '''
    def __init__(self, file_name, **kwargs):
        ''' @param want_chr: set to True/False to FORCE add/removal of "chr" (default = leave as is) '''
        self.source = file_utils.file_or_file_name(file_name, "r")
        self.delimiter = kwargs.get("delimiter", '\t')
        self.want_chr = kwargs.get("want_chr")

    def __iter__(self):
        return self

    def next(self): #@ReservedAssignment
        line = self.source.next()
        while (line.startswith('#') or line.startswith('track') or line.startswith('browser')):
            line = self.source.next()

        columns = line.rstrip().split(self.delimiter)
        data = dict(zip(REQUIRED_FIELDS, columns[:len(REQUIRED_FIELDS)]))
        # Only put in optional fields when provided
        data.update(izip(ALL_OPTIONAL, columns[len(REQUIRED_FIELDS):]))
        # Convert to appropriate types
        for f in REQUIRED_FIELDS + ALL_OPTIONAL:
            if f in TYPE_CONVERSIONS and data.get(f):
                convert = TYPE_CONVERSIONS[f]
                data[f] = convert(data[f])

        # Make sure they aren't negative (some dodgy .BED files around)
        start = max(0, data["chrom_start"])
        end = max(0, data["chrom_end"])
        chrom = data["chrom"]
        if self.want_chr is not None: #format chrom
            chrom = genomics.format_chrom(chrom, self.want_chr)
        iv = HTSeq.GenomicInterval(chrom, start, end)
        name = data.get("name", "")
        score = data.get("score", 0.0) # Using "." here causes intron to not be displayed correctly
        iv.strand = data.get("strand", '.')
            
        feature = HTSeq.GenomicFeature(name, "from_bed", iv )
        feature.score = score
        feature.attr = {"score" : score} # So attr and .score are always the same (may be 0.0 if not found)

        # Copy set optional fields to GenomicFeature.attr
        for field in OPTIONAL_ATTRIBUTES:
            value = data.get(field)
            if value:
                feature.attr[field] = value
        return feature


def write_features_to_bed_file(file_name, features):
    ''' Specify columns 7-12: 'thick_start', 'thick_end', 'block_count', 'block_sizes', 'block_starts' by 
        in dict at feature attribute 'attr'. e.g. feature.attr['thick_start'] = 200
    '''
    # Copied from util to remove circular imports which were causing ipython reload to fail
    def mk_path(path):
        if path and not os.path.exists(path):
            os.makedirs(path)
    
    def mk_path_for_file(f):
        mk_path(os.path.dirname(f))
    
    def name_from_file_name(file_name):
        return os.path.splitext(os.path.basename(file_name))[0]
    
    logger = logging.getLogger(__name__)
    logger.info("Writing IVs to %s", file_name)
    mk_path_for_file(file_name)
    with open(file_name, "w") as f:
        f.write("track name=\"%s\"" % name_from_file_name(file_name) + '\n')
        for feature in features:
            logger.debug("feature is %r", feature)
            f.write(feature_to_bed_line(feature))

def feature_to_bed_line(feature):
    name = feature.name or ''
    score = getattr(feature, "score", 0) #Using "." here causes intron to not be displayed correctly

    iv = feature.iv
    line = [iv.chrom, iv.start, iv.end, name, score, iv.strand]
    
    attr = getattr(feature, 'attr', None)
    if attr:
        block_attribute_count = sum([a in attr for a in BLOCK_ATTRIBUTES])
        if 0 < block_attribute_count < len(BLOCK_ATTRIBUTES):
            raise ValueError("Writing attributes '%s'. You must specify none or all fields of %s" % (attr, BLOCK_ATTRIBUTES))

        defaults = {"thick_start" : iv.start, "thick_end" : iv.end, "item_rgb" : '0'}
        highest_optional_field_provided = -1
        optional_fields = []
        for (i, f) in enumerate(OPTIONAL_ATTRIBUTES):
            value = attr.get(f)
            if value is not None:
                highest_optional_field_provided = i
            else:
                value = defaults.get(f)
            optional_fields.append(value)

        line += optional_fields[:highest_optional_field_provided+1]

    return '\t'.join([str(s) for s in line]) + '\n'

def ga_to_features(genomic_array, test_valid_func=None):
    if test_valid_func is None:
        test_valid_func = lambda x : x

    for chr_vec in sorted(genomic_array.chrom_vectors.values()):
        for strand_vec in chr_vec.itervalues():
            for (_, feature) in strand_vec.steps():
                if test_valid_func(feature):
                    yield feature

def write_genomic_array_of_features_to_bed_file(file_name, genomic_array, test_valid_func=None):
    features = ga_to_features(genomic_array, test_valid_func)
    write_features_to_bed_file(file_name, features)
    
def bed_file_to_score_array(bed_file_name, **kwargs):
    ''' Genomic array with Bed feature score as values, None if region has no overlapping feature
    kwargs:
    score_func : function which takes feature as input and returns score to use for genomic array
        default = lambda feature : float(feature.score)        
    '''
    score_func = kwargs.get("score_func", lambda feature : float(feature.score))

    reader = BedFileReader(bed_file_name, **kwargs)
    genomic_array = HTSeq.GenomicArray( "auto", stranded=False, typecode='O' ) #use typecode=Object so that gaps have a score of None, not 0.
    for feature in reader:
        genomic_array[feature.iv] = score_func(feature)
    return genomic_array

def bed_iv_iterator(bed_file, **kwargs):
    for feature in BedFileReader(bed_file, **kwargs):
            yield feature.iv


# Sorts bed file by chromosome orders in specified reference file
# by default, ref_index uses hg19 Broad19.fasta 
# specifying "iGenome" will use the iGenome human reference genome
# - can only use samtools faidx generated index (*.fai)
def sort_bed_file_by_chrom_order(input_bed, output_bed, ref_index=None):
    f_order = ""
    # indexed_ref by default uses Broad19.fasta
    if ref_index == None or ref_index == "Broad19":
        f_order = "/data/sacgf/reference/hg19/Broad19.fasta.fai"
    elif ref_index == "iGenome" or ref_index == "UCSC":
        f_order = "/data/sacgf/reference/iGenomes/Homo_sapiens/UCSC/hg19/Sequence/WholeGenomeFasta/genome.fa.fai" 
    else:
        f_order = ref_index

    # parse fasta index file to get chromosome order
    chr_ = {}
    order = 1
    fin = open(f_order, "r")
    for line in fin:
        chr_[line.strip().split()[0]] = order
        order += 1
    
    # load bed file to dataframe, and do sorting
    df = pd.read_table(input_bed, low_memory=False)
    df.columns = BED_COLUMNS[:len(df.columns)]
    df["chrom_order"] = 0
    for chrom in chr_:
        df.ix[df.chrom == chrom, "chrom_order"] = chr_[chrom]
    df.sort(["chrom_order", "chrom_start", "chrom_end"], inplace=True)
    df.drop("chrom_order", inplace=True, axis=1)

    df["chrom"] = df["chrom"].fillna("missing")
    df = df[df.chrom != "missing"]
    df["chrom_start"] = df["chrom_start"].astype(int)
    df["chrom_end"] = df["chrom_end"].astype(int)
    if output_bed != None:
        df.to_csv(output_bed, sep="\t", index=False, header=False)
    else:
        df.to_csv(sys.stdout, sep="\t", index=False, header=False)         



    

    
















        