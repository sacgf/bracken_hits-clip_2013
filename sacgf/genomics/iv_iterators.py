'''
Read genomic data as HTSeq.GenomicIntervals

Created on Sep 9, 2012

@author: dlawrence
'''

from sacgf import genomics
from sacgf.genomics import bed_file, iv_opposite_strand
import HTSeq
import os


def load_iv_iterator(file_name):
    suffix = os.path.splitext(file_name)[1].lower()
    iterator = None
    if suffix == ".bam":
        iterator = bam_iv_iterator(file_name)
    elif suffix == ".bed":
        iterator = bed_unique_iv_iterator(file_name)
    elif suffix == '.sam':
        iterator = sam_iv_iterator(file_name)
    elif suffix in ['.gff', '.gtf']:
        iterator = gff_iv_iterator(file_name)
    else:
        raise ValueError("Unknown input_file_type of " + suffix)
    return iterator

def chromosome_filter_iterator(chromosomes, iterator):
    for iv in iterator:
        if iv.chrom in chromosomes:
            yield iv

def bam_iv_iterator(bam_file):
    for aln in HTSeq.BAM_Reader(bam_file):
        if aln.aligned:
            yield aln.iv


def sam_iv_iterator(sam_file):
    for aln in HTSeq.SAM_Reader(sam_file):
        if aln.aligned:
            yield aln.iv

def gff_iv_iterator(gtf_file):
    for feature in HTSeq.GFF_Reader(gtf_file):
        yield feature.iv


def bed_unique_iv_iterator(bed_file_name):
    unique_starts = set()
    # TODO: This is a very wasteful way of finding out uniques
    # We could sort -'ve strand by end, then use that... 
    for iv in bed_file.bed_iv_iterator(bed_file_name):
        if not iv.start_d_as_pos in unique_starts:
            unique_starts.add(iv.start_d_as_pos)
            yield iv

def fixed_length_interval(iv, before=0, after=0):
    # TODO: Use start_d end_d??
    if iv.strand == "+":
        return HTSeq.GenomicInterval(iv.chrom, iv.start - before, iv.start + after, iv.strand)
    elif iv.strand == "-":
        return HTSeq.GenomicInterval(iv.chrom, iv.end - after, iv.end + before, iv.strand)

def fixed_length_iv_iterator(iterator, before, after):
    for iv in iterator:
        yield fixed_length_interval(iv, before, after)

def format_chrom_iterator(iterator, want_chr):
    for iv in iterator:
        iv.chrom = genomics.format_chrom(iv.chrom, want_chr)
        yield iv

def strand_swap_iterator(iterator):
    for iv in iterator:
        yield iv_opposite_strand(iv)
        
def midpoint_iterator(iterator):
    for iv in iterator:
        yield genomics.mid_point(iv)
        
def load_midpoint_iterator(file_name):
    iterator = load_iv_iterator(file_name)
    return midpoint_iterator(iterator)