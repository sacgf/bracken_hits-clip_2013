INTRO
-----

This is for the paper: "Genome-wide identification of miR-200 targets reveals a regulatory network controlling cell invasion" by Bracken et al. EMBOJ-2014-88641. http://emboj.embopress.org/content/early/2014/07/28/embj.201488641

Data can be found here: http://www.ncbi.nlm.nih.gov/bioproject/257235

For more information on the bioinformatics methods, please see the [wiki](https://bitbucket.org/sacgf/bracken_hits-clip_2013/wiki/Home).

INSTALL
-------

The following are the commands used on a Fresh Ubuntu 14.04 installation:

	sudo apt-get update
	sudo apt-get install git python-dev python-pip gfortran libopenblas-dev liblapack-dev zlib1g-dev libblas-dev libatlas-base-dev libpng-dev libfreetype6-dev libpq-dev libxft-dev
	git clone https://bitbucket.org/sacgf/bracken_hits-clip_2013.git
	cd bracken_hits-clip_2013

	# Make sure numpy is installed properly before other components
	sudo pip install numpy
	sudo pip install -r requirements.txt

INSTALL REFERENCES
------------------

You need to install the Illumina iGenomes package (which you may already have), and set an environment variable pointing to this location.

	export IGENOMES_DIR=`pwd` # Or wherever you want to put it
	mkdir -p ${IGENOMES_DIR}
	cd ${IGENOMES_DIR}
	wget ftp://igenome:G3nom3s4u@ussd-ftp.illumina.com/Homo_sapiens/UCSC/hg18/Homo_sapiens_UCSC_hg18.tar.gz
	tar xvfz Homo_sapiens_UCSC_hg18.tar.gz

	# If you have write permission to this dir, the script will automatically work and
	# you can skip the following steps. If you don't, you can generate this file once:
	sudo bash
	export IGENOMES_DIR=`pwd`
	cat ./scripts/create_pyfasta_flat_file.py # Make sure ok to run script as root
	./scripts/create_pyfasta_flat_file.py

RUNNING
-------

	cd bracken_hits-clip_2013
	./scripts/analysis/bracken_hitsclip_analysis.sh

	This will produce files in the "output" directory.